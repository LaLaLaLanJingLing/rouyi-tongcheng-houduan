package com.ruoyi.web.common.Vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 提交优惠券订单的类型
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public class SubmitOrderIVo
{
    private static final long serialVersionUID = 1L;

    /** 商品id */
    private String phone;

    /** 商户id */
    private String userName;

    /** 数量 */
    private Long totalCount;

    /** 数量 */
    private Long orderId;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("phone", getPhone())
            .append("userName", getUserName())
            .append("totalCount",getTotalCount())
            .append("orderId",getOrderId())
            .toString();
    }
}
