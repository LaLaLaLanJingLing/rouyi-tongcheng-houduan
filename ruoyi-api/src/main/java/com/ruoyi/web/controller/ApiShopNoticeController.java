package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账号管理Controller
 */
@Api(description = "公告管理")
@Controller
@RequestMapping("/api/notice")
public class ApiShopNoticeController {

    @Autowired
    IShopNoticeService shopNoticeService;

    //获取公告列表
    @GetMapping("/list")
    @ResponseBody
    public ApiResult noticeList()
    {
        ShopNotice shopNotice = new ShopNotice();
        shopNotice.setStatus(new String("1"));
        List<ShopNotice> shopNoticeList = shopNoticeService.selectShopNoticeList(shopNotice);
        return ApiResult.success(shopNoticeList);
    }

    //获取公告详情
    @GetMapping("/detail/{id}")
    @ResponseBody
    public ApiResult detail(@PathVariable("id") Long id)
    {
        if (id == null){
            return ApiResult.error("参数传递有问题");
        }
        ShopNotice shopNotice = shopNoticeService.selectShopNoticeById(id);
        return ApiResult.success(shopNotice);
    }
}
