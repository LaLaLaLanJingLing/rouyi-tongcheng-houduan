package com.ruoyi.web.controller.business;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.reflect.ReflectUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商户首页管理
 */
@Api(description = "商户店铺管理")
@Controller
@RequestMapping("/api/bus/shop")
public class ApiBusShopController {


    private static Logger logger = LoggerFactory.getLogger(ApiBusShopController.class);

    @Autowired
    IShopUserService shopUserService;

    @Autowired
    IBusinessShopService businessShopService;

    @Autowired
    IBusinessShopInfoService businessShopInfoService;

    @Autowired
    IShopOrderService shopOrderService;

    @Autowired
    IBusinessShopCardService businessShopCardService;

    @Autowired
    IBusinessShopApplyService businessShopApplyService;

    //登录
    @GetMapping("/detail")
    @ResponseBody
    public ApiResult detail(@RequestParam("shopId") Long shopId)
    {
        BusinessShop businessShop = businessShopService.selectBusinessShopById(shopId);

        if (businessShop == null){
            return ApiResult.error("店铺不存在,请联系管理员.或者重新登录");
        }

//        if (businessShop.getStates()){
//            return  ApiResult.error("店铺已经被封闭,请联系客服");
//        }

        //本月收入  本月销量  店铺的余额
        double amount = shopOrderService.selectShopSaleTotalAmountByMonth(shopId, 11L);
        //本月销量
        int saleNumber = shopOrderService.selectShopSaleTotalNumberByMonth(shopId, 11L);

        JSONObject resultJson = new JSONObject();
        resultJson.put("amount", amount);
        resultJson.put("saleNumber", saleNumber);
        resultJson.put("shop", businessShop);

        return ApiResult.success(resultJson);
    }


    @GetMapping("/user")
    @ResponseBody
    public ApiResult user(@RequestParam("shopId") Long shopId)
    {
        BusinessShop businessShop = businessShopService.selectBusinessShopById(shopId);

        if (businessShop == null){
            return ApiResult.error("店铺不存在,请联系管理员.或者重新登录");
        }

        ShopUser selectShopUser = new ShopUser();
        selectShopUser.setShopId(shopId);
        List<ShopUser> shopUserList = shopUserService.selectShopUserList(selectShopUser);

        return ApiResult.success(shopUserList);
    }


    @GetMapping("/userDetail")
    @ResponseBody
    public ApiResult userDetail(@RequestParam("userId") Long userId)
    {
        ShopUser shopUser = shopUserService.selectShopUserById(userId);

        if (shopUser == null){
            return ApiResult.error("此用户不存在,请联系管理员");
        }

        return ApiResult.success(shopUser);
    }


    @GetMapping("/balance")
    @ResponseBody
    public ApiResult balance(@RequestParam("shopId") Long shopId)
    {
        BusinessShopInfo selectBusinessShopInfo = new BusinessShopInfo();
        selectBusinessShopInfo.setShopId(shopId);
        List<BusinessShopInfo> businessShopInfoList = businessShopInfoService.selectBusinessShopInfoList(selectBusinessShopInfo);
        if (businessShopInfoList.size() == 0 ){
            return ApiResult.success(0);
        }
        if (businessShopInfoList.size() > 1){
            return ApiResult.error("系统发生问题,请联系管理员");
        }

        BusinessShopInfo businessShopInfo = businessShopInfoList.get(0);

        return ApiResult.success(businessShopInfo.getBalance());
    }


    @PostMapping("/saveCard")
    @ResponseBody
    public ApiResult saveCard(@RequestBody BusinessShopCard businessShopCard)
    {
        if (StringUtils.isEmpty(businessShopCard.getBankName())){
            return ApiResult.error("请输入银行名称");
        }
        if (StringUtils.isEmpty(businessShopCard.getCardNumber())){
            return ApiResult.error("请输入银行卡号码");
        }
        if (StringUtils.isEmpty(businessShopCard.getUsername())){
            return ApiResult.error("请输入持卡人姓名");
        }
        businessShopCard.setCreateDate(new Date());
        businessShopCard.setStatus(0L);
        businessShopCard.setUpdateTime(new Date());
        //TODO: aa
        businessShopCard.setUpdateBy("老板");

        businessShopCardService.insertBusinessShopCard(businessShopCard);
        return ApiResult.success();
    }


    @GetMapping("/cardList")
    @ResponseBody
    public ApiResult cardList(@RequestParam("shopId") Long shopId)
    {
        if (shopId == null){
            return ApiResult.error("参数有问题,请联系管理员");
        }
        BusinessShopCard selectBusinessShopCard = new BusinessShopCard();
        selectBusinessShopCard.setShopId(shopId);
        List<BusinessShopCard> businessShopCardList = businessShopCardService.selectBusinessShopCardList(selectBusinessShopCard);
        return ApiResult.success(businessShopCardList);
    }

    @PostMapping("/updateCard")
    @ResponseBody
    public ApiResult updateCard(@RequestBody BusinessShopCard businessShopCard)
    {
        Long cardId = businessShopCard.getId();

        if (cardId == null){
            return ApiResult.error("系统发生错误,请联系管理员");
        }

        if (StringUtils.isEmpty(businessShopCard.getBankName())){
            return ApiResult.error("请输入银行名称");
        }
        if (StringUtils.isEmpty(businessShopCard.getCardNumber())){
            return ApiResult.error("请输入银行卡号码");
        }
        if (StringUtils.isEmpty(businessShopCard.getUsername())){
            return ApiResult.error("请输入持卡人姓名");
        }

        businessShopCard.setUpdateTime(new Date());
        businessShopCard.setUpdateBy("老板二大爷");
        businessShopCardService.updateBusinessShopCard(businessShopCard);

        return ApiResult.success();
    }


    @GetMapping("/deleteCard")
    @ResponseBody
    public ApiResult deleteCard(@RequestParam("cardId") Long shopId)
    {
        BusinessShopInfo selectBusinessShopInfo = new BusinessShopInfo();
        selectBusinessShopInfo.setShopId(shopId);
        List<BusinessShopInfo> businessShopInfoList = businessShopInfoService.selectBusinessShopInfoList(selectBusinessShopInfo);
        if (businessShopInfoList.size() == 0 ){
            return ApiResult.success(0);
        }
        if (businessShopInfoList.size() > 1){
            return ApiResult.error("系统发生问题,请联系管理员");
        }

        BusinessShopInfo businessShopInfo = businessShopInfoList.get(0);

        return ApiResult.success(businessShopInfo.getBalance());
    }


    @PostMapping("/apply")
    @ResponseBody
    public ApiResult apply(@RequestBody BusinessShopApply businessShopApply)
    {
        if (businessShopApply.getCardId() == null){
            return ApiResult.error("请选择银行卡");
        }
        if (businessShopApply.getShopId() == null){
            return ApiResult.error("请重新登录");
        }

        double balance = businessShopApply.getBalance();
        //提现申请
        BigDecimal money = BigDecimal.valueOf(balance);
        if (money.compareTo(BigDecimal.ZERO) <= 0){
            return  ApiResult.error("输入金额应该大于0");
        }
        //判断金额是否大于 商户的余额
//        BusinessShopInfo businessShopInfo = businessShopInfoService.selectBusinessShopInfoByShopId(businessShopApply.getShopId());
//        if (businessShopInfo == null){
//            return ApiResult.error("暂无余额可以提现");
//        }
//
//        BigDecimal shopBalance =  BigDecimal.valueOf(businessShopInfo.getBalance());
//        if (shopBalance.compareTo(money) < 0){
//            return ApiResult.error("提现金额 超过余额, 请重新输入");
//        }

        businessShopApply.setCreateDate(new Date());
        businessShopApply.setModifyDate(new Date());
        businessShopApply.setStatus(0L);

        businessShopApplyService.insertBusinessShopApply(businessShopApply);

        return  ApiResult.success();
    }
}
