package com.ruoyi.web.controller.business;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TokenUtils;
import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 商户账户管理
 */
@Api(description = "商户账户管理")
@Controller
@RequestMapping("/api/bus/account")
public class ApiBusAccountController {

    @Autowired
    IShopUserService shopUserService;

    @Autowired
    ISysTokenService sysTokenService;

    @Autowired
    SysPasswordService passwordService;

    //登录
    @PostMapping("/login")
    @ResponseBody
    public ApiResult login(@RequestBody JSONObject jsonObject)
    {
        String loginName = jsonObject.getString("loginName");
        String password = jsonObject.getString("password");
        if(StringUtils.isEmpty(loginName) || StringUtils.isEmpty(password)){
            return ApiResult.error("账号 密码不能为空");
        }
        ShopUser shopUser = new ShopUser();
        shopUser.setLoginName(loginName);
        List<ShopUser> shopUserList = shopUserService.selectShopUserList(shopUser);
        if (shopUserList.size() == 0){
            return ApiResult.error("登录失败, 此用户不存在");
        }

        if (shopUserList.size() > 1){
            return ApiResult.error("服务器发生异常,请联系管理员");
        }
        ShopUser actualShopUser = shopUserList.get(0);

        String encryptPassword = passwordService.encryptPassword(loginName, password, actualShopUser.getSalt());

        if (!encryptPassword.equals(actualShopUser.getPassword())){
            return ApiResult.error("密码错误, 请输入正确密码");
        }

        //更新token
        SysToken sysToken = sysTokenService.selectSysTokenByShopUserId(actualShopUser.getId());
        if (sysToken == null){
            //生成一个新的token
            sysToken = new SysToken();
            sysToken.setToken(TokenUtils.generateToken());
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_MONTH, 20); // 今天+20天
            Date expired_date = c.getTime();
            sysToken.setExpiredDate(expired_date);
            sysToken.setShopUserId(actualShopUser.getId());
            sysTokenService.insertSysToken(sysToken);
        }else{
            sysToken.setToken(TokenUtils.generateToken());
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_MONTH, 20); // 今天+20天
            Date expired_date = c.getTime();
            sysToken.setExpiredDate(expired_date);
            sysTokenService.updateSysToken(sysToken);
        }

        String strToken = sysToken.getToken();
        JSONObject resultJson = new JSONObject();
        resultJson.put("token",strToken );
        resultJson.put("shopId",actualShopUser.getShopId());

        return ApiResult.success("登录成功", resultJson);
    }
}
