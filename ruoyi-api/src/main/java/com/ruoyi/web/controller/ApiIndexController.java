package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.util.List;

/**
 * 账号管理Controller
 */
@Api(description = "首页管理")
@Controller
@RequestMapping("/api/index")
public class ApiIndexController {

    @Autowired
    ISysTokenService sysTokenService;

    @Autowired
    IShopGroupService shopGroupService;

    @Autowired
    IShopAdService shopAdService;

    @Autowired
    IGoodsService goodsService;

    @Autowired
    IProductCategoryService productCategoryService;

    @Autowired
    IShopNoticeService shopNoticeService;

    @Autowired
    IShopActivityService shopActivityService;

    @Autowired
    IShopAreaService shopAreaService;

    //获取首页分组信息和其中的列表
    @PostMapping("/group")
    @ResponseBody
    public ApiResult group()
    {
        ShopGroup shopGroup = new ShopGroup();
        shopGroup.setIsUsed(true);
        List<ShopGroup> shopGroupList = shopGroupService.selectShopGroupList(shopGroup);
        return ApiResult.success(shopGroupList);
    }


    //获取首页轮播图
    @PostMapping("/ad")
    @ResponseBody
    public ApiResult ad()
    {
        ShopAd shopAd = new ShopAd();
        shopAd.setType(0L);
        List<ShopAd> shopAdList = shopAdService.selectShopAdList(shopAd);
        return ApiResult.success(shopAdList);
    }

    //获取首页分组商品列表
    @PostMapping("/group/goods")
    @ResponseBody
    public ApiResult groupGoods(
            @RequestBody JSONObject jsonObj)
    {
        Long id = jsonObj.getLong("id");
        if (id == null){
            return ApiResult.error("参数错误");
        }
        ShopGroup shopGroup = shopGroupService.selectShopGroupById(id);
        if(shopGroup == null){
            return ApiResult.error("id 非法");
        }

        List<Goods> goodsList = goodsService.selectGoodsListByGroupId(shopGroup.getId());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("goodsList", goodsList);
        jsonObject.put("id", id);
        return ApiResult.success(jsonObject);
    }


    //获取首页分组商品列表
    @PostMapping("/category")
    @ResponseBody
    public ApiResult category()
    {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setGrade(1L);
        productCategory.setIsMarketable(true);
        productCategory.setIsTop(true);
        List<ProductCategory> productCategoryList = productCategoryService.selectProductCategoryList(productCategory);
        return ApiResult.success(productCategoryList);
    }

    //获取首页公告
    @GetMapping("/notice")
    @ResponseBody
    public ApiResult notice()
    {
        ShopNotice shopNotice = new ShopNotice();
        shopNotice.setNoticeType(new String("1"));
        List<ShopNotice> shopNoticeList = shopNoticeService.selectShopNoticeList(shopNotice);
        return ApiResult.success(shopNoticeList);
    }

    //获取首页活动详情
    @GetMapping("/activity")
    @ResponseBody
    public ApiResult activity()
    {
        ShopActivity shopActivity = new ShopActivity();
        shopActivity.setIsDelete(false);
        shopActivity.setIsTop(true);
        shopActivity.setIsMarketable(true);
        List<ShopActivity> shopActivityList = shopActivityService.selectShopActivityList(shopActivity);
        JSONObject.JSONArray jsonArray = new JSONObject.JSONArray();
        for(ShopActivity tempShopActivity : shopActivityList){
            Long goodsId = tempShopActivity.getRelateId();
            if (goodsId != null){
                Goods goods = goodsService.selectGoodsById(goodsId);
                if (goods != null){
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("activity", tempShopActivity);
                    jsonObject.put("goods", goods);
                    jsonObject.put("type", tempShopActivity.getType());
                    jsonArray.add(jsonObject);
                }
            }
        }
        return ApiResult.success(jsonArray);
    }

    //获取首页活动详情
    @GetMapping("/location/{id}")
    @ResponseBody
    public ApiResult location(@PathVariable("id") Long id)
    {
        if (id == -1){
            //说明没有目的地 获取默认目的地
            ShopArea shopArea = new ShopArea();
            shopArea.setGrade(1L);
            List<ShopArea> shopAreaList = shopAreaService.selectShopAreaList(shopArea);
            if (shopAreaList.size() == 0){
                return ApiResult.error("获取地区失败");
            }
            ShopArea resultShopArea = shopAreaList.get(0);
            return ApiResult.success(resultShopArea);
        }else{
            ShopArea shopArea = shopAreaService.selectShopAreaById(id);
            if (shopArea == null){
                return ApiResult.error("获取地区失败");
            }
            return ApiResult.success(shopArea);
        }
    }
}
