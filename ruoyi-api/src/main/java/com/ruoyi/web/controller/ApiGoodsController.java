package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.domain.ShopNotice;
import com.ruoyi.system.domain.vo.ProductImage;
import com.ruoyi.system.service.IBusinessShopService;
import com.ruoyi.system.service.IGoodsService;
import com.ruoyi.system.service.IShopNoticeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 商品管理
 */
@Api(description = "商品管理")
@Controller
@RequestMapping("/api/goods")
public class ApiGoodsController {

    @Autowired
    IGoodsService goodsService;

    @Autowired
    IBusinessShopService businessShopService;

    //获取商品详情
    @GetMapping("/details/{id}")
    @ResponseBody
    public ApiResult details(@PathVariable("id") Long id)
    {
        if(id == null){
            return ApiResult.error("请检查参数是否有误");
        }
        Goods goods = goodsService.selectGoodsById(id);
        if (goods == null){
            return ApiResult.error("该商品已经被删除");
        }
        List<ProductImage> productImageList = goodsService.getProductImageList(goods);
        BusinessShop businessShop = goods.getBusinessShop();
        JSONObject resultJson = new JSONObject();
        resultJson.put("goods", goods);
        resultJson.put("images", productImageList);
        resultJson.put("shop", businessShop);

        return ApiResult.success(resultJson);
    }
}
