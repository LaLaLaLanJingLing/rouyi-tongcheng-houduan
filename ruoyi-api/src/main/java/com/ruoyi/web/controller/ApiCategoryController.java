package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.domain.ProductCategory;
import com.ruoyi.system.domain.ShopNotice;
import com.ruoyi.system.service.IGoodsService;
import com.ruoyi.system.service.IProductCategoryService;
import com.ruoyi.system.service.IShopNoticeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 分类管理
 */
@Api(description = "分类管理")
@Controller
@RequestMapping("/api/category")
public class ApiCategoryController {

    @Autowired
    IProductCategoryService productCategoryService;

    @Autowired
    IGoodsService goodsService;

    @GetMapping("/goods/{id}")
    @ResponseBody
    public ApiResult detail(@PathVariable("id") Long id)
    {
        if (id == null){
            return ApiResult.error("参数传递有问题");
        }
        Goods goods = new Goods();
        goods.setProductCategoryId(id);
        goods.setIsMarketable(true);
        List<Goods> goodsList = goodsService.selectGoodsList(goods);
        return ApiResult.success(goodsList);
    }

    @GetMapping("/list")
    @ResponseBody
    public ApiResult list()
    {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setIsMarketable(true);
        productCategory.setGrade(1L);
        List<ProductCategory> productCategoryList = productCategoryService.selectProductCategoryList(productCategory);
        return ApiResult.success(productCategoryList);
    }

}
