package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.sun.org.glassfish.gmbal.ParameterNames;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.github.pagehelper.page.PageMethod.startPage;

/**
 * 评论管理Controller
 */
@Api(description = "评论管理")
@Controller
@RequestMapping("/api/comment")
public class ApiCommentController {

    @Autowired
    IShopMemberCommentService shopMemberCommentService;



    //获取首页分组信息和其中的列表
    @GetMapping("/list")
    @ResponseBody
    public ApiResult list(@RequestParam("current") Integer current,  //当前页数
                          @RequestParam("size") Integer size    //页面数量)
    )
    {
        ShopMemberComment shopMemberComment = new ShopMemberComment();
        shopMemberComment.setStatus(String.valueOf('1'));
        startPage(current,size,"update_time");
        List<ShopMemberComment> shopMemberCommentList = shopMemberCommentService.selectShopMemberCommentList(shopMemberComment);
        return ApiResult.success(shopMemberCommentList);
    }
}
