package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.ShopMemberComment;
import com.ruoyi.system.service.IBusinessShopService;
import com.ruoyi.system.service.IShopMemberCommentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.github.pagehelper.page.PageMethod.startPage;

/**
 * 店铺管理Controller
 */
@Api(description = "店铺管理")
@Controller
@RequestMapping("/api/shop")
public class ApiShopController {

    @Autowired
    IBusinessShopService businessShopService;

    //获取首页分组信息和其中的列表
    @GetMapping("/list")
    @ResponseBody
    public ApiResult list(@RequestParam("current") Integer current,  //当前页数
                          @RequestParam("size") Integer size    //页面数量)
    )
    {
        BusinessShop businessShop = new BusinessShop();
        businessShop.setStates(true);
        List<BusinessShop> businessShopList = businessShopService.selectBusinessShopList(businessShop);
        return ApiResult.success(businessShopList);
    }
}
