package com.ruoyi.web.controller;

import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 地区管理Controller
 */
@Api(description = "地区管理")
@Controller
@RequestMapping("/api/location")
public class ApiLocationController {

    @Autowired
    IShopAreaService shopAreaService;

    //获取首页分组信息和其中的列表
    @GetMapping("/list")
    @ResponseBody
    public ApiResult list()
    {
        ShopArea shopArea = new ShopArea();
        List<ShopArea> shopAreaList = shopAreaService.selectShopAreaList(shopArea);
        return ApiResult.success(shopAreaList);
    }


}
