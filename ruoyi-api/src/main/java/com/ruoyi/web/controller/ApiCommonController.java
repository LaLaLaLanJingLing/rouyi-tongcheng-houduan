package com.ruoyi.web.controller;

import com.google.gson.JsonObject;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.json.JSONObject;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.SettingStroage;
import com.ruoyi.system.service.IBusinessShopService;
import com.ruoyi.system.service.ISettingStroageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 店铺管理Controller
 */
@Api(description = "公共接口")
@Controller
@RequestMapping("/api/common")
public class ApiCommonController {

    @Autowired
    IBusinessShopService businessShopService;

    @Autowired
    ISettingStroageService settingStroageService;

    //上传文件 获取网络路劲
    @PostMapping("/upload")
    @ResponseBody
    public ApiResult uploadFile(MultipartFile file)
    {
        try {
            // 上传文件路径
            SettingStroage settingStroage = settingStroageService.selectSettingStroageById(1L);
            String filePath = Global.getUploadPath();
            String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
            FileUploadUtils.setEndpoint(endpoint);
            FileUploadUtils.setAccessId(settingStroage.getAccessId());
            FileUploadUtils.setAccessKey(settingStroage.getAccessKey());
            FileUploadUtils.setBucketName(settingStroage.getBucketName());
            FileUploadUtils.setUrlPrefix(settingStroage.getUrlPri());
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            JSONObject resultObject = new JSONObject();
            resultObject.put("url", fileName);
            return ApiResult.success(resultObject);

        }catch (Exception e){
            return ApiResult.error("上传文件失败");
        }
    }
}
