package com.ruoyi.web.controller.business;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.ApiBaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.TokenUtils;
import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.system.domain.ShopOrder;
import com.ruoyi.system.domain.ShopUser;
import com.ruoyi.system.domain.SysToken;
import com.ruoyi.system.service.IShopOrderService;
import com.ruoyi.system.service.IShopUserService;
import com.ruoyi.system.service.ISysTokenService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 商户首页管理
 */
@Api(description = "商户首页管理")
@Controller
@RequestMapping("/api/bus/index")
public class ApiBusIndexController extends ApiBaseController {

    @Autowired
    IShopUserService shopUserService;

    @Autowired
    IShopOrderService shopOrderService;

    //登录
    @GetMapping("/orderList")
    @ResponseBody
    public ApiResult orderList(@RequestParam("shopId") Long shopId)
    {
        ShopOrder shopOrder = new ShopOrder();
        shopOrder.setBusinessShopId(shopId);
        startPage(0,10,"modify_date");
        List<ShopOrder> shopOrderList = shopOrderService.selectShopOrderList(shopOrder);
        return ApiResult.success(shopOrderList);
    }

}
