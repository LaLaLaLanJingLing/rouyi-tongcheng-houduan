package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysArea;
import java.util.List;

/**
 * 区域Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface SysAreaMapper 
{
    /**
     * 查询区域
     * 
     * @param id 区域ID
     * @return 区域
     */
    public SysArea selectSysAreaById(Long id);

    /**
     * 查询区域列表
     * 
     * @param sysArea 区域
     * @return 区域集合
     */
    public List<SysArea> selectSysAreaList(SysArea sysArea);

    /**
     * 新增区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    public int insertSysArea(SysArea sysArea);

    /**
     * 修改区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    public int updateSysArea(SysArea sysArea);

    /**
     * 删除区域
     * 
     * @param id 区域ID
     * @return 结果
     */
    public int deleteSysAreaById(Long id);

    /**
     * 批量删除区域
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysAreaByIds(String[] ids);
}
