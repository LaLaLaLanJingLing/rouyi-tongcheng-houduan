package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopNotice;
import java.util.List;

/**
 * 商城通告Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
public interface ShopNoticeMapper 
{
    /**
     * 查询商城通告
     * 
     * @param id 商城通告ID
     * @return 商城通告
     */
    public ShopNotice selectShopNoticeById(Long id);

    /**
     * 查询商城通告列表
     * 
     * @param shopNotice 商城通告
     * @return 商城通告集合
     */
    public List<ShopNotice> selectShopNoticeList(ShopNotice shopNotice);

    /**
     * 新增商城通告
     * 
     * @param shopNotice 商城通告
     * @return 结果
     */
    public int insertShopNotice(ShopNotice shopNotice);

    /**
     * 修改商城通告
     * 
     * @param shopNotice 商城通告
     * @return 结果
     */
    public int updateShopNotice(ShopNotice shopNotice);

    /**
     * 删除商城通告
     * 
     * @param id 商城通告ID
     * @return 结果
     */
    public int deleteShopNoticeById(Long id);

    /**
     * 批量删除商城通告
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopNoticeByIds(String[] ids);
}
