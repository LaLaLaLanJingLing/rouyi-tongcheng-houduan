package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopUser;
import java.util.List;

/**
 * 商户用户信息Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-03
 */
public interface ShopUserMapper 
{
    /**
     * 查询商户用户信息
     * 
     * @param id 商户用户信息ID
     * @return 商户用户信息
     */
    public ShopUser selectShopUserById(Long id);

    /**
     * 查询商户用户信息列表
     * 
     * @param shopUser 商户用户信息
     * @return 商户用户信息集合
     */
    public List<ShopUser> selectShopUserList(ShopUser shopUser);

    /**
     * 新增商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    public int insertShopUser(ShopUser shopUser);

    /**
     * 修改商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    public int updateShopUser(ShopUser shopUser);

    /**
     * 删除商户用户信息
     * 
     * @param id 商户用户信息ID
     * @return 结果
     */
    public int deleteShopUserById(Long id);

    /**
     * 批量删除商户用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopUserByIds(String[] ids);


    /**
     * 确认登录名唯一
     *
     * @param loginName 新增的登录名称
     * @return 数量
     */
    public int checkLoginNameUnique(String loginName);


    /**
     * 确认电话唯一
     *
     * @param phone 电话
     * @return 数量
     */
    public int checkPhoneUnique(String phone);
}
