package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysToken;
import java.util.List;

/**
 * 系统tokenMapper接口
 * 
 * @author ruoyi
 * @date 2019-08-27
 */
public interface SysTokenMapper 
{
    /**
     * 查询系统token
     * 
     * @param id 系统tokenID
     * @return 系统token
     */
    public SysToken selectSysTokenById(Long id);

    /**
     * 查询系统token列表
     * 
     * @param sysToken 系统token
     * @return 系统token集合
     */
    public List<SysToken> selectSysTokenList(SysToken sysToken);

    /**
     * 查询系统token列表
     *
     * @param memberId 成员id
     * @return 系统token
     */
    public SysToken selectSysTokenByMemberId(Long memberId);


    /**
     * 查询系统token列表
     *
     * @param strToken Token
     * @return 系统token
     */
    public SysToken selectSysTokenByToken(String strToken);

    /**
     * 查询系统token列表
     *
     * @param strToken Token
     * @return 系统token
     */
    public SysToken selectSysTokenByShopUserId(Long shopUserId);

    /**
     * 新增系统token
     * 
     * @param sysToken 系统token
     * @return 结果
     */
    public int insertSysToken(SysToken sysToken);

    /**
     * 修改系统token
     * 
     * @param sysToken 系统token
     * @return 结果
     */
    public int updateSysToken(SysToken sysToken);

    /**
     * 删除系统token
     * 
     * @param id 系统tokenID
     * @return 结果
     */
    public int deleteSysTokenById(Long id);

    /**
     * 批量删除系统token
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysTokenByIds(String[] ids);
}
