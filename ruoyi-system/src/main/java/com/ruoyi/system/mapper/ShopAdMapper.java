package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopAd;
import java.util.List;

/**
 * 轮播图Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
public interface ShopAdMapper 
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图ID
     * @return 轮播图
     */
    public ShopAd selectShopAdById(Long id);

    /**
     * 查询轮播图列表
     * 
     * @param shopAd 轮播图
     * @return 轮播图集合
     */
    public List<ShopAd> selectShopAdList(ShopAd shopAd);

    /**
     * 新增轮播图
     * 
     * @param shopAd 轮播图
     * @return 结果
     */
    public int insertShopAd(ShopAd shopAd);

    /**
     * 修改轮播图
     * 
     * @param shopAd 轮播图
     * @return 结果
     */
    public int updateShopAd(ShopAd shopAd);

    /**
     * 删除轮播图
     * 
     * @param id 轮播图ID
     * @return 结果
     */
    public int deleteShopAdById(Long id);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopAdByIds(String[] ids);
}
