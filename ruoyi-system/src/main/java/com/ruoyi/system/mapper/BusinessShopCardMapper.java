package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BusinessShopCard;
import java.util.List;

/**
 * 商户银行信息Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface BusinessShopCardMapper 
{
    /**
     * 查询商户银行信息
     * 
     * @param id 商户银行信息ID
     * @return 商户银行信息
     */
    public BusinessShopCard selectBusinessShopCardById(Long id);

    /**
     * 查询商户银行信息列表
     * 
     * @param businessShopCard 商户银行信息
     * @return 商户银行信息集合
     */
    public List<BusinessShopCard> selectBusinessShopCardList(BusinessShopCard businessShopCard);

    /**
     * 新增商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    public int insertBusinessShopCard(BusinessShopCard businessShopCard);

    /**
     * 修改商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    public int updateBusinessShopCard(BusinessShopCard businessShopCard);

    /**
     * 删除商户银行信息
     * 
     * @param id 商户银行信息ID
     * @return 结果
     */
    public int deleteBusinessShopCardById(Long id);

    /**
     * 批量删除商户银行信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopCardByIds(String[] ids);
}
