package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Goods;
import java.util.List;

/**
 * 优惠券列Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface GoodsMapper 
{
    /**
     * 查询优惠券列
     * 
     * @param id 优惠券列ID
     * @return 优惠券列
     */
    public Goods selectGoodsById(Long id);

    /**
     * 查询优惠券列列表
     * 
     * @param goods 优惠券列
     * @return 优惠券列集合
     */
    public List<Goods> selectGoodsList(Goods goods);

    /**
     * 查询优惠券列列表
     *
     * @param Long groupId
     * @return 优惠券列集合
     */
    public List<Goods> selectGoodsListByGroupId(Long groupId);

    /**
     * 新增优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    public int insertGoods(Goods goods);

    /**
     * 修改优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    public int updateGoods(Goods goods);

    /**
     * 删除优惠券列
     * 
     * @param id 优惠券列ID
     * @return 结果
     */
    public int deleteGoodsById(Long id);

    /**
     * 批量删除优惠券列
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsByIds(String[] ids);
}
