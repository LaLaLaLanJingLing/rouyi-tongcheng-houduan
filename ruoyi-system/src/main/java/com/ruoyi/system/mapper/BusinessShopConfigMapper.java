package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BusinessShopConfig;
import java.util.List;

/**
 * 商户分成设置Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
public interface BusinessShopConfigMapper 
{
    /**
     * 查询商户分成设置
     * 
     * @param id 商户分成设置ID
     * @return 商户分成设置
     */
    public BusinessShopConfig selectBusinessShopConfigById(Long id);

    /**
     * 查询商户分成设置列表
     * 
     * @param businessShopConfig 商户分成设置
     * @return 商户分成设置集合
     */
    public List<BusinessShopConfig> selectBusinessShopConfigList(BusinessShopConfig businessShopConfig);

    /**
     * 新增商户分成设置
     * 
     * @param businessShopConfig 商户分成设置
     * @return 结果
     */
    public int insertBusinessShopConfig(BusinessShopConfig businessShopConfig);

    /**
     * 修改商户分成设置
     * 
     * @param businessShopConfig 商户分成设置
     * @return 结果
     */
    public int updateBusinessShopConfig(BusinessShopConfig businessShopConfig);

    /**
     * 删除商户分成设置
     * 
     * @param id 商户分成设置ID
     * @return 结果
     */
    public int deleteBusinessShopConfigById(Long id);

    /**
     * 批量删除商户分成设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopConfigByIds(String[] ids);
}
