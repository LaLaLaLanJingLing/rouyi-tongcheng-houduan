package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopArea;
import java.util.List;

/**
 * 商家地区Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-06
 */
public interface ShopAreaMapper 
{
    /**
     * 查询商家地区
     * 
     * @param id 商家地区ID
     * @return 商家地区
     */
    public ShopArea selectShopAreaById(Long id);

    /**
     * 查询商家地区列表
     * 
     * @param shopArea 商家地区
     * @return 商家地区集合
     */
    public List<ShopArea> selectShopAreaList(ShopArea shopArea);

    /**
     * 新增商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    public int insertShopArea(ShopArea shopArea);

    /**
     * 修改商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    public int updateShopArea(ShopArea shopArea);

    /**
     * 删除商家地区
     * 
     * @param id 商家地区ID
     * @return 结果
     */
    public int deleteShopAreaById(Long id);

    /**
     * 批量删除商家地区
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopAreaByIds(String[] ids);
}
