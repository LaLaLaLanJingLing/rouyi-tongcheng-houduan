package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Member;
import java.util.List;

/**
 * 会员Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public interface MemberMapper 
{
    /**
     * 查询会员
     * 
     * @param id 会员ID
     * @return 会员
     */
    public Member selectMemberById(Long id);

    /**
     * 查询会员
     *
     * @param openId 会员oepnId
     * @return 会员
     */
    public Member selectMemberByOpenId(String openId);

    /**
     * 查询会员列表
     * 
     * @param member 会员
     * @return 会员集合
     */
    public List<Member> selectMemberList(Member member);

    /**
     * 新增会员
     * 
     * @param member 会员
     * @return 结果
     */
    public int insertMember(Member member);

    /**
     * 修改会员
     * 
     * @param member 会员
     * @return 结果
     */
    public int updateMember(Member member);

    /**
     * 删除会员
     * 
     * @param id 会员ID
     * @return 结果
     */
    public int deleteMemberById(Long id);

    /**
     * 批量删除会员
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberByIds(String[] ids);
}
