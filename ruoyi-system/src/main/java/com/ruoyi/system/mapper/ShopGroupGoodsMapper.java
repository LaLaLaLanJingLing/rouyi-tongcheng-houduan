package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopGroupGoods;
import java.util.List;

/**
 * 分组商品关联Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
public interface ShopGroupGoodsMapper 
{
    /**
     * 查询分组商品关联
     * 
     * @param groupId 分组商品关联ID
     * @return 分组商品关联
     */
    public ShopGroupGoods selectShopGroupGoodsById(Long groupId);

    /**
     * 查询分组商品关联列表
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 分组商品关联集合
     */
    public List<ShopGroupGoods> selectShopGroupGoodsList(ShopGroupGoods shopGroupGoods);

    /**
     * 新增分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    public int insertShopGroupGoods(ShopGroupGoods shopGroupGoods);

    /**
     * 修改分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    public int updateShopGroupGoods(ShopGroupGoods shopGroupGoods);

    /**
     * 删除分组商品关联
     * 
     * @param groupId 分组商品关联ID
     * @return 结果
     */
    public int deleteShopGroupGoodsById(Long groupId);

    /**
     * 批量删除分组商品关联
     * 
     * @param groupIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopGroupGoodsByIds(String[] groupIds);

    /**
     * 批量删除分组
     *
     * @param goodsId 需要删除的商品Id
     * @return 结果
     */
    public int deleteShopGroupGoodsByGoodsId(Long goodsId);

}
