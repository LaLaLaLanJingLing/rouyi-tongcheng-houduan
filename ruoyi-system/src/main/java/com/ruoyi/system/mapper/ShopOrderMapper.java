package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface ShopOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public ShopOrder selectShopOrderById(Long id);


    /**
     * 查询订单
     *
     * @param sn 订单编号
     * @return 订单
     */
    public ShopOrder selectShopOrderBySn(String sn);

    /**
     * 查询订单列表
     * 
     * @param shopOrder 订单
     * @return 订单集合
     */
    public List<ShopOrder> selectShopOrderList(ShopOrder shopOrder);

    /**
     * 新增订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    public int insertShopOrder(ShopOrder shopOrder);

    /**
     * 修改订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    public int updateShopOrder(ShopOrder shopOrder);

    /**
     * 删除订单
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteShopOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopOrderByIds(String[] ids);


    public double selectSaleTotalAmountByMonth(@Param("shopId") Long shopId, @Param("month") Long month );

    public int selectSaleTotalNumberByMonth(@Param("shopId") Long shopId, @Param("month") Long month );
}
