package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopGroup;
import java.util.List;

/**
 * 分组Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
public interface ShopGroupMapper 
{
    /**
     * 查询分组
     * 
     * @param id 分组ID
     * @return 分组
     */
    public ShopGroup selectShopGroupById(Long id);

    /**
     * 查询分组列表
     * 
     * @param shopGroup 分组
     * @return 分组集合
     */
    public List<ShopGroup> selectShopGroupList(ShopGroup shopGroup);

    /**
     * 新增分组
     * 
     * @param shopGroup 分组
     * @return 结果
     */
    public int insertShopGroup(ShopGroup shopGroup);

    /**
     * 修改分组
     * 
     * @param shopGroup 分组
     * @return 结果
     */
    public int updateShopGroup(ShopGroup shopGroup);

    /**
     * 删除分组
     * 
     * @param id 分组ID
     * @return 结果
     */
    public int deleteShopGroupById(Long id);

    /**
     * 批量删除分组
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopGroupByIds(String[] ids);

}
