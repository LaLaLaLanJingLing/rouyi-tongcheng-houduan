package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BusinessShopApply;
import java.util.List;

/**
 * 商户提现申请Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface BusinessShopApplyMapper 
{
    /**
     * 查询商户提现申请
     * 
     * @param id 商户提现申请ID
     * @return 商户提现申请
     */
    public BusinessShopApply selectBusinessShopApplyById(Long id);

    /**
     * 查询商户提现申请列表
     * 
     * @param businessShopApply 商户提现申请
     * @return 商户提现申请集合
     */
    public List<BusinessShopApply> selectBusinessShopApplyList(BusinessShopApply businessShopApply);

    /**
     * 新增商户提现申请
     * 
     * @param businessShopApply 商户提现申请
     * @return 结果
     */
    public int insertBusinessShopApply(BusinessShopApply businessShopApply);

    /**
     * 修改商户提现申请
     * 
     * @param businessShopApply 商户提现申请
     * @return 结果
     */
    public int updateBusinessShopApply(BusinessShopApply businessShopApply);

    /**
     * 删除商户提现申请
     * 
     * @param id 商户提现申请ID
     * @return 结果
     */
    public int deleteBusinessShopApplyById(Long id);

    /**
     * 批量删除商户提现申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopApplyByIds(String[] ids);
}
