package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SettingStroage;
import java.util.List;

/**
 * 存储插件配置Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-04
 */
public interface SettingStroageMapper 
{
    /**
     * 查询存储插件配置
     * 
     * @param id 存储插件配置ID
     * @return 存储插件配置
     */
    public SettingStroage selectSettingStroageById(Long id);

    /**
     * 查询存储插件配置列表
     * 
     * @param settingStroage 存储插件配置
     * @return 存储插件配置集合
     */
    public List<SettingStroage> selectSettingStroageList(SettingStroage settingStroage);

    /**
     * 新增存储插件配置
     * 
     * @param settingStroage 存储插件配置
     * @return 结果
     */
    public int insertSettingStroage(SettingStroage settingStroage);

    /**
     * 修改存储插件配置
     * 
     * @param settingStroage 存储插件配置
     * @return 结果
     */
    public int updateSettingStroage(SettingStroage settingStroage);

    /**
     * 删除存储插件配置
     * 
     * @param id 存储插件配置ID
     * @return 结果
     */
    public int deleteSettingStroageById(Long id);

    /**
     * 批量删除存储插件配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSettingStroageByIds(String[] ids);
}
