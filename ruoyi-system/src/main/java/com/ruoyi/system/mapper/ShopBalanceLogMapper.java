package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopBalanceLog;
import java.util.List;

/**
 * 平台余额记录Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
public interface ShopBalanceLogMapper 
{
    /**
     * 查询平台余额记录
     * 
     * @param id 平台余额记录ID
     * @return 平台余额记录
     */
    public ShopBalanceLog selectShopBalanceLogById(Long id);

    /**
     * 查询平台余额记录列表
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 平台余额记录集合
     */
    public List<ShopBalanceLog> selectShopBalanceLogList(ShopBalanceLog shopBalanceLog);

    /**
     * 新增平台余额记录
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 结果
     */
    public int insertShopBalanceLog(ShopBalanceLog shopBalanceLog);

    /**
     * 修改平台余额记录
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 结果
     */
    public int updateShopBalanceLog(ShopBalanceLog shopBalanceLog);

    /**
     * 删除平台余额记录
     * 
     * @param id 平台余额记录ID
     * @return 结果
     */
    public int deleteShopBalanceLogById(Long id);

    /**
     * 批量删除平台余额记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopBalanceLogByIds(String[] ids);
}
