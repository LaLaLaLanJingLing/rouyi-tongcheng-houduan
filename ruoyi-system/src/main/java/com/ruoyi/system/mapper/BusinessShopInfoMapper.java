package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BusinessShopInfo;
import java.util.List;

/**
 * 商户信息Mapper接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface BusinessShopInfoMapper 
{
    /**
     * 查询商户信息
     * 
     * @param id 商户信息ID
     * @return 商户信息
     */
    public BusinessShopInfo selectBusinessShopInfoById(Long id);

    /**
     * 查询商户信息列表
     * 
     * @param businessShopInfo 商户信息
     * @return 商户信息集合
     */
    public List<BusinessShopInfo> selectBusinessShopInfoList(BusinessShopInfo businessShopInfo);

    /**
     * 新增商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    public int insertBusinessShopInfo(BusinessShopInfo businessShopInfo);

    /**
     * 修改商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    public int updateBusinessShopInfo(BusinessShopInfo businessShopInfo);

    /**
     * 删除商户信息
     * 
     * @param id 商户信息ID
     * @return 结果
     */
    public int deleteBusinessShopInfoById(Long id);

    /**
     * 批量删除商户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopInfoByIds(String[] ids);
}
