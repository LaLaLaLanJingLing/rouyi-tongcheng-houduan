package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.BusinessShop;
import java.util.List;

/**
 * 商户Mapper接口
 * 
 * @author ruoyi
 * @date 2019-10-25
 */
public interface BusinessShopMapper 
{
    /**
     * 查询商户
     * 
     * @param id 商户ID
     * @return 商户
     */
    public BusinessShop selectBusinessShopById(Long id);

    /**
     * 查询商户列表
     * 
     * @param businessShop 商户
     * @return 商户集合
     */
    public List<BusinessShop> selectBusinessShopList(BusinessShop businessShop);

    /**
     * 新增商户
     * 
     * @param businessShop 商户
     * @return 结果
     */
    public int insertBusinessShop(BusinessShop businessShop);

    /**
     * 修改商户
     * 
     * @param businessShop 商户
     * @return 结果
     */
    public int updateBusinessShop(BusinessShop businessShop);

    /**
     * 删除商户
     * 
     * @param id 商户ID
     * @return 结果
     */
    public int deleteBusinessShopById(Long id);

    /**
     * 批量删除商户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopByIds(String[] ids);
}
