package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ShopActivity;
import java.util.List;

/**
 * 活动Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
public interface ShopActivityMapper 
{
    /**
     * 查询活动
     * 
     * @param id 活动ID
     * @return 活动
     */
    public ShopActivity selectShopActivityById(Long id);

    /**
     * 查询活动列表
     * 
     * @param shopActivity 活动
     * @return 活动集合
     */
    public List<ShopActivity> selectShopActivityList(ShopActivity shopActivity);

    /**
     * 新增活动
     * 
     * @param shopActivity 活动
     * @return 结果
     */
    public int insertShopActivity(ShopActivity shopActivity);

    /**
     * 修改活动
     * 
     * @param shopActivity 活动
     * @return 结果
     */
    public int updateShopActivity(ShopActivity shopActivity);

    /**
     * 删除活动
     * 
     * @param id 活动ID
     * @return 结果
     */
    public int deleteShopActivityById(Long id);

    /**
     * 批量删除活动
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopActivityByIds(String[] ids);
}
