package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 平台余额记录对象 shop_balance_log
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
public class ShopBalanceLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 平台余额记录表 */
    private Long id;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operator;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 改变金额 */
    @Excel(name = "改变金额")
    private Double changAmount;

    /** 类型关联 0.分成。 1.充值 */
    @Excel(name = "类型关联 0.分成。 1.充值")
    private Long type;

    /** 关联id(订单，充值记录) */
    @Excel(name = "关联id(订单，充值记录)")
    private Long relateId;

    /** 关联操作id(商户,会员) */
    @Excel(name = "关联操作id(商户,会员)")
    private Long relateObjectId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setChangAmount(Double changAmount) 
    {
        this.changAmount = changAmount;
    }

    public Double getChangAmount() 
    {
        return changAmount;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setRelateId(Long relateId) 
    {
        this.relateId = relateId;
    }

    public Long getRelateId() 
    {
        return relateId;
    }
    public void setRelateObjectId(Long relateObjectId) 
    {
        this.relateObjectId = relateObjectId;
    }

    public Long getRelateObjectId() 
    {
        return relateObjectId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("status", getStatus())
            .append("operator", getOperator())
            .append("createDate", getCreateDate())
            .append("remark", getRemark())
            .append("changAmount", getChangAmount())
            .append("type", getType())
            .append("relateId", getRelateId())
            .append("relateObjectId", getRelateObjectId())
            .toString();
    }
}
