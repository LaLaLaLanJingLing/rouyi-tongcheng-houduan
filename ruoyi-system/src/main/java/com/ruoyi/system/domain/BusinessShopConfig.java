package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 商户分成设置对象 business_shop_config
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
public class BusinessShopConfig extends BaseEntity
{
    /**
     * 类型
     */
    public enum type {
        year(0), //按年
        time(1); //按次
        private final int value;

        type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 分成类型
     */
    public enum divideType {
        regular(0), //固定的
        Percentage(1); //百分比
        private final int value;

        divideType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = 1L;

    /** 商户分成设置表 */
    private Long id;

    /** 商户信息id */
    @Excel(name = "商户信息id")
    private Long shopId;

    /** 类型 0.年费模式, 1.按次收费 */
    @Excel(name = "类型 0.年费模式, 1.按次收费")
    private Long type;

    /** 分成类型, 0.百分比, 1.固定金额 */
    @Excel(name = "分成类型, 0.百分比, 1.固定金额")
    private Long divideType;

    /** 数值 */
    @Excel(name = "数值")
    private Double value;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operator;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 修改时间 */
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setDivideType(Long divideType) 
    {
        this.divideType = divideType;
    }

    public Long getDivideType() 
    {
        return divideType;
    }
    public void setValue(Double value) 
    {
        this.value = value;
    }

    public Double getValue() 
    {
        return value;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setModifyDate(Date modifyDate) 
    {
        this.modifyDate = modifyDate;
    }

    public Date getModifyDate() 
    {
        return modifyDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopId", getShopId())
            .append("type", getType())
            .append("divideType", getDivideType())
            .append("value", getValue())
            .append("status", getStatus())
            .append("operator", getOperator())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("createDate", getCreateDate())
            .append("modifyDate", getModifyDate())
            .append("remark", getRemark())
            .toString();
    }
}
