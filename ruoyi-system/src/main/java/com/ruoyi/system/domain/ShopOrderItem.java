package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单单例对象 shop_order_item
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public class ShopOrderItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private Double price;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long quantity;

    /** 退货数量 */
    @Excel(name = "退货数量")
    private Long returnedQuantity;

    /** 已使用数量 */
    @Excel(name = "已使用数量")
    private Long shippedQuantity;

    /** 编号 */
    @Excel(name = "编号")
    private String sn;

    /** 商品缩略图 */
    @Excel(name = "商品缩略图")
    private String image;

    /** 商品类型 */
    @Excel(name = "商品类型")
    private Long type;

    /** 是否评论 */
    @Excel(name = "是否评论")
    private Boolean isReview;

    /** 订单 */
    @Excel(name = "订单")
    private Long orderId;

    /** 商品 */
    @Excel(name = "商品")
    private Long goodsId;

    /** 是否已使用 */
    @Excel(name = "是否已使用")
    private Boolean isUsed;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setReturnedQuantity(Long returnedQuantity) 
    {
        this.returnedQuantity = returnedQuantity;
    }

    public Long getReturnedQuantity() 
    {
        return returnedQuantity;
    }
    public void setShippedQuantity(Long shippedQuantity) 
    {
        this.shippedQuantity = shippedQuantity;
    }

    public Long getShippedQuantity() 
    {
        return shippedQuantity;
    }
    public void setSn(String sn) 
    {
        this.sn = sn;
    }

    public String getSn() 
    {
        return sn;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setIsReview(Boolean isReview)
    {
        this.isReview = isReview;
    }

    public Boolean getIsReview()
    {
        return isReview;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setIsUsed(Boolean isUsed)
    {
        this.isUsed = isUsed;
    }

    public Boolean getIsUsed()
    {
        return isUsed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("price", getPrice())
            .append("quantity", getQuantity())
            .append("returnedQuantity", getReturnedQuantity())
            .append("shippedQuantity", getShippedQuantity())
            .append("sn", getSn())
            .append("image", getImage())
            .append("type", getType())
            .append("isReview", getIsReview())
            .append("orderId", getOrderId())
            .append("goodsId", getGoodsId())
            .append("isUsed", getIsUsed())
            .toString();
    }
}
