package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;
import java.util.List;

/**
 * 订单对象 shop_order
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public class ShopOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**
     * 状态
     */
    public enum Status {

        /**
         * 等待付款 0
         */
        pendingPayment,

        /**
         * 等待审核 1
         */
        pendingReview,

        /**
         * 等待发货 2
         */
        pendingShipment,

        /**
         * 已发货 (已经支付) 3
         */
        shipped,

        /**
         * 已收货 4
         */
        received,

        /**
         * 已完成 （已经核销） 5
         */
        completed,

        /**
         * 已失败 6
         */
        failed,

        /**
         * 已取消 7
         */
        canceled,

        /**
         * 已拒绝 8
         */
        denied,

        /**
         * 未完成 9
         */
        unfinished,

        /**
         * 退款中 10
         */
        refunding,

        /**
         * 退款完成 11
         */
        refunded,

        /**
         * 所有已取消 12
         */
        allCanceled,

        /**
         * 已评价 （评论完成） 13
         */
        reviewed,

    }

    /**
     *  支付方式
     */
    public enum payType {

        /**
         * 微信支付
         */
        weiXinPay,

        /**
         * 支付宝支付
         */
       aliPay,

    }
    /** ID */
    private Long id;

    /** 创建日期 */
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 修改日期 */
    @Excel(name = "修改日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyDate;

    /** 删除标记 */
    @Excel(name = "删除标记")
    private Long deleteFlag;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private Double amount;

    /** 已付金额 */
    @Excel(name = "已付金额")
    private Double amountPaid;

    /** 地区名称 */
    @Excel(name = "地区名称")
    private String areaName;

    /** 完成日期 */
    @Excel(name = "完成日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completeDate;

    /** 收货人 */
    @Excel(name = "收货人")
    private String consignee;

    /** 优惠券折扣 */
    @Excel(name = "优惠券折扣")
    private Double couponDiscount;

    /** 过期时间 */
    @Excel(name = "过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expire;

    /** 手续费 */
    @Excel(name = "手续费")
    private Double fee;

    /** 运费 */
    @Excel(name = "运费")
    private Double freight;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** 调整金额 */
    @Excel(name = "调整金额")
    private Double offsetAmount;

    /** 支付方式名称 */
    @Excel(name = "支付方式名称")
    private String paymentMethodName;

    /** 支付方式类型 */
    @Excel(name = "支付方式类型")
    private Long paymentMethodType;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private Double price;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long quantity;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private Double refundAmount;

    /** 退款商品数量 */
    @Excel(name = "退款商品数量")
    private Long returnedQuantity;

    /** 编号 */
    @Excel(name = "编号")
    private String sn;

    /** 状态0.等待付款1.等待接单2.等待发货3.已发货4.已收货5.已完成6.已失败7.已取消 */
    @Excel(name = "状态0.等待付款1.等待接单2.等待发货3.已发货4.已收货5.已完成6.已失败7.已取消")
    private Long status;

    /** 来源 */
    @Excel(name = "来源")
    private Long source;

    /** 税金 */
    @Excel(name = "税金")
    private Double tax;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 邮编 */
    @Excel(name = "邮编")
    private String zipCode;

    /** 地区 */
    @Excel(name = "地区")
    private Long areaId;

    /** 会员 */
    @Excel(name = "会员")
    private Long memberId;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private Long paymentMethodId;

    /** 推荐人 */
    @Excel(name = "推荐人")
    private String recommender;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;


    /** 兑换码 */
    @Excel(name = "兑换码")
    private String code;

    /** 店铺id */
    @Excel(name = "店铺id")
    private Long businessShopId;

    private BusinessShop businessShop;

    List<ShopOrderItem> shopOrderItems;

    public BusinessShop getBusinessShop() {
        return businessShop;
    }

    public void setBusinessShop(BusinessShop businessShop) {
        this.businessShop = businessShop;
    }

    public List<ShopOrderItem> getShopOrderItems() {
        return shopOrderItems;
    }

    public void setShopOrderItems(List<ShopOrderItem> shopOrderItems) {
        this.shopOrderItems = shopOrderItems;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setModifyDate(Date modifyDate) 
    {
        this.modifyDate = modifyDate;
    }

    public Date getModifyDate() 
    {
        return modifyDate;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setAmount(Double amount) 
    {
        this.amount = amount;
    }

    public Double getAmount() 
    {
        return amount;
    }
    public void setAmountPaid(Double amountPaid) 
    {
        this.amountPaid = amountPaid;
    }

    public Double getAmountPaid() 
    {
        return amountPaid;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setCompleteDate(Date completeDate) 
    {
        this.completeDate = completeDate;
    }

    public Date getCompleteDate() 
    {
        return completeDate;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setCouponDiscount(Double couponDiscount) 
    {
        this.couponDiscount = couponDiscount;
    }

    public Double getCouponDiscount() 
    {
        return couponDiscount;
    }
    public void setExpire(Date expire) 
    {
        this.expire = expire;
    }

    public Date getExpire() 
    {
        return expire;
    }
    public void setFee(Double fee) 
    {
        this.fee = fee;
    }

    public Double getFee() 
    {
        return fee;
    }
    public void setFreight(Double freight) 
    {
        this.freight = freight;
    }

    public Double getFreight() 
    {
        return freight;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setOffsetAmount(Double offsetAmount) 
    {
        this.offsetAmount = offsetAmount;
    }

    public Double getOffsetAmount() 
    {
        return offsetAmount;
    }
    public void setPaymentMethodName(String paymentMethodName) 
    {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentMethodName() 
    {
        return paymentMethodName;
    }
    public void setPaymentMethodType(Long paymentMethodType) 
    {
        this.paymentMethodType = paymentMethodType;
    }

    public Long getPaymentMethodType() 
    {
        return paymentMethodType;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setRefundAmount(Double refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public Double getRefundAmount() 
    {
        return refundAmount;
    }
    public void setReturnedQuantity(Long returnedQuantity) 
    {
        this.returnedQuantity = returnedQuantity;
    }

    public Long getReturnedQuantity() 
    {
        return returnedQuantity;
    }
    public void setSn(String sn) 
    {
        this.sn = sn;
    }

    public String getSn() 
    {
        return sn;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setSource(Long source) 
    {
        this.source = source;
    }

    public Long getSource() 
    {
        return source;
    }
    public void setTax(Double tax) 
    {
        this.tax = tax;
    }

    public Double getTax() 
    {
        return tax;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setZipCode(String zipCode) 
    {
        this.zipCode = zipCode;
    }

    public String getZipCode() 
    {
        return zipCode;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setPaymentMethodId(Long paymentMethodId) 
    {
        this.paymentMethodId = paymentMethodId;
    }

    public Long getPaymentMethodId() 
    {
        return paymentMethodId;
    }
    public void setRecommender(String recommender) 
    {
        this.recommender = recommender;
    }

    public String getRecommender() 
    {
        return recommender;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setBusinessShopId(Long businessShopId) 
    {
        this.businessShopId = businessShopId;
    }

    public Long getBusinessShopId() 
    {
        return businessShopId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createDate", getCreateDate())
            .append("modifyDate", getModifyDate())
            .append("deleteFlag", getDeleteFlag())
            .append("address", getAddress())
            .append("amount", getAmount())
            .append("amountPaid", getAmountPaid())
            .append("areaName", getAreaName())
            .append("completeDate", getCompleteDate())
            .append("consignee", getConsignee())
            .append("couponDiscount", getCouponDiscount())
            .append("expire", getExpire())
            .append("fee", getFee())
            .append("freight", getFreight())
            .append("memo", getMemo())
            .append("offsetAmount", getOffsetAmount())
            .append("paymentMethodName", getPaymentMethodName())
            .append("paymentMethodType", getPaymentMethodType())
            .append("phone", getPhone())
            .append("price", getPrice())
            .append("quantity", getQuantity())
            .append("refundAmount", getRefundAmount())
            .append("returnedQuantity", getReturnedQuantity())
            .append("sn", getSn())
            .append("status", getStatus())
            .append("source", getSource())
            .append("tax", getTax())
            .append("type", getType())
            .append("zipCode", getZipCode())
            .append("areaId", getAreaId())
            .append("memberId", getMemberId())
            .append("paymentMethodId", getPaymentMethodId())
            .append("recommender", getRecommender())
            .append("name", getName())
            .append("businessShopId", getBusinessShopId())
            .toString();
    }
}
