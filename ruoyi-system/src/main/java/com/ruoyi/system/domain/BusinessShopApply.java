package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 商户提现申请对象 business_shop_apply
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public class BusinessShopApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户提现申请表 */
    private Long id;

    /** 商户信息表id */
    @Excel(name = "商户信息表id")
    private Long shopId;

    /** 银行卡 */
    @Excel(name = "银行卡")
    private Long cardId;

    /** 提现类型 */
    @Excel(name = "提现类型")
    private Long applyType;

    /** 提现金额 */
    @Excel(name = "提现金额")
    private Double balance;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operator;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 修改时间 */
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyDate;



    /** 商户 */
    private BusinessShop businessShop;

    public BusinessShop getBusinessShop() {
        return businessShop;
    }

    public void setBusinessShop(BusinessShop businessShop) {
        this.businessShop = businessShop;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setCardId(Long cardId) 
    {
        this.cardId = cardId;
    }

    public Long getCardId() 
    {
        return cardId;
    }
    public void setApplyType(Long applyType) 
    {
        this.applyType = applyType;
    }

    public Long getApplyType() 
    {
        return applyType;
    }
    public void setBalance(Double balance) 
    {
        this.balance = balance;
    }

    public Double getBalance() 
    {
        return balance;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setModifyDate(Date modifyDate) 
    {
        this.modifyDate = modifyDate;
    }

    public Date getModifyDate() 
    {
        return modifyDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopId", getShopId())
            .append("cardId", getCardId())
            .append("applyType", getApplyType())
            .append("balance", getBalance())
            .append("status", getStatus())
            .append("operator", getOperator())
            .append("createDate", getCreateDate())
            .append("modifyDate", getModifyDate())
            .append("remark", getRemark())
            .toString();
    }
}
