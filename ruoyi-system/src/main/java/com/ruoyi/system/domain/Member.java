package com.ruoyi.system.domain;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

/**
 * 会员对象 member
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public class Member extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 消费金额 */
    @Excel(name = "消费金额")
    private Double amount;

    /** 上级 */
    @Excel(name = "上级")
    private Long parentId;

    /** 余额 */
    @Excel(name = "余额")
    private Double balance;

    /** 出生日期 */
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birth;

    /** 性别 */
    @Excel(name = "性别")
    private Long gender;

    /** 是否锁定 */
    @Excel(name = "是否锁定")
    private Boolean isLocked;

    /** 锁定日期 */
    @Excel(name = "锁定日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lockedDate;

    /** 最后登录日期 */
    @Excel(name = "最后登录日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginDate;

    /** 手机 */
    @Excel(name = "手机")
    private String mobile;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;

    /** openID */
    @Excel(name = "openID")
    private String openId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 头像 */
    @Excel(name = "头像")
    private String avatar;

    /** 地区 */
    @Excel(name = "地区")
    private Long areaId;

    /** 会员等级 */
    @Excel(name = "会员等级")
    private Long memberRankId;

    /** 分润等级 */
    @Excel(name = "分润等级")
    private Long memberType;


    public String getActualNickName(){
        String str = StringUtils.EMPTY;
        try {
            if (StringUtils.isNotEmpty(this.nickname)){
                str = URLDecoder.decode(this.nickname, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setAmount(Double amount) 
    {
        this.amount = amount;
    }

    public Double getAmount() 
    {
        return amount;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setBalance(Double balance) 
    {
        this.balance = balance;
    }

    public Double getBalance() 
    {
        return balance;
    }
    public void setBirth(Date birth) 
    {
        this.birth = birth;
    }

    public Date getBirth() 
    {
        return birth;
    }
    public void setGender(Long gender) 
    {
        this.gender = gender;
    }

    public Long getGender() 
    {
        return gender;
    }
    public void setIsLocked(Boolean isLocked)
    {
        this.isLocked = isLocked;
    }

    public Boolean getIsLocked()
    {
        return isLocked;
    }
    public void setLockedDate(Date lockedDate) 
    {
        this.lockedDate = lockedDate;
    }

    public Date getLockedDate() 
    {
        return lockedDate;
    }
    public void setLoginDate(Date loginDate) 
    {
        this.loginDate = loginDate;
    }

    public Date getLoginDate() 
    {
        return loginDate;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return this.nickname;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return this.username;
    }
    public void setAvatar(String avatar) 
    {
        this.avatar = avatar;
    }

    public String getAvatar() 
    {
        return avatar;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setMemberRankId(Long memberRankId) 
    {
        this.memberRankId = memberRankId;
    }

    public Long getMemberRankId() 
    {
        return memberRankId;
    }
    public void setMemberType(Long memberType) 
    {
        this.memberType = memberType;
    }

    public Long getMemberType() 
    {
        return memberType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("address", getAddress())
            .append("amount", getAmount())
            .append("parentId", getParentId())
            .append("balance", getBalance())
            .append("birth", getBirth())
            .append("gender", getGender())
            .append("isLocked", getIsLocked())
            .append("lockedDate", getLockedDate())
            .append("loginDate", getLoginDate())
            .append("mobile", getMobile())
            .append("nickname", getNickname())
            .append("openId", getOpenId())
            .append("username", getUsername())
            .append("avatar", getAvatar())
            .append("areaId", getAreaId())
            .append("memberRankId", getMemberRankId())
            .append("memberType", getMemberType())
            .toString();
    }
}
