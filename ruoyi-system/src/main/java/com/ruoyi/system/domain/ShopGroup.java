package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分组对象 shop_group
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
public class ShopGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 订单排序 */
    @Excel(name = "订单排序")
    private Long orders;

    /** 图标 */
    @Excel(name = "图标")
    private String icon;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 是否使用 */
    @Excel(name = "是否使用")
    private Boolean isUsed;

    /** 内容详情 */
    @Excel(name = "内容详情")
    private String content;

    /** 首页商品排序类型 */
    @Excel(name = "首页商品排序类型")
    private Integer indexType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrders(Long orders) 
    {
        this.orders = orders;
    }

    public Long getOrders() 
    {
        return orders;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setIsUsed(Boolean isUsed)
    {
        this.isUsed = isUsed;
    }

    public Boolean getIsUsed()
    {
        return isUsed;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }


    public Integer getIndexType() {
        return indexType;
    }

    public void setIndexType(Integer indexType) {
        this.indexType = indexType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orders", getOrders())
            .append("icon", getIcon())
            .append("title", getTitle())
            .append("type", getType())
            .append("isUsed", getIsUsed())
            .append("content", getContent())
            .append("indexType", getIndexType())
            .toString();
    }
}
