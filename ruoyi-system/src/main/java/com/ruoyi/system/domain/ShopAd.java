package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 轮播图对象 shop_ad
 * 
 * @author ruoyi
 * @date 2019-11-04
 */
public class ShopAd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 订单排序 */
    @Excel(name = "订单排序")
    private Long orders;

    /** 起始日期 */
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginDate;

    /** 结束日期 */
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 路径 */
    @Excel(name = "路径")
    private String image;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 链接地址 */
    @Excel(name = "链接地址")
    private String url;

    /** $column.columnComment */
    @Excel(name = "链接地址")
    private Long relateType;

    /** 关联对象 */
    @Excel(name = "关联对象")
    private Long relateId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrders(Long orders) 
    {
        this.orders = orders;
    }

    public Long getOrders() 
    {
        return orders;
    }
    public void setBeginDate(Date beginDate) 
    {
        this.beginDate = beginDate;
    }

    public Date getBeginDate() 
    {
        return beginDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setRelateType(Long relateType) 
    {
        this.relateType = relateType;
    }

    public Long getRelateType() 
    {
        return relateType;
    }
    public void setRelateId(Long relateId) 
    {
        this.relateId = relateId;
    }

    public Long getRelateId() 
    {
        return relateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orders", getOrders())
            .append("beginDate", getBeginDate())
            .append("endDate", getEndDate())
            .append("image", getImage())
            .append("title", getTitle())
            .append("type", getType())
            .append("url", getUrl())
            .append("relateType", getRelateType())
            .append("relateId", getRelateId())
            .toString();
    }
}
