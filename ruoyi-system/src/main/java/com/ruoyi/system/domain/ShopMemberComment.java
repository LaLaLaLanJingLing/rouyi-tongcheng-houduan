package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 评论对象 shop_member_comment
 * 
 * @author ruoyi
 * @date 2019-11-26
 */
public class ShopMemberComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private Long id;

    /** 评论状态（0正常 1关闭） */
    @Excel(name = "评论状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    /** 备注 */
    @Excel(name = "备注")
    private String content;

    /** 商品id */
    @Excel(name = "商品id")
    private Long goodsId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long memberId;

    /** 评分 */
    @Excel(name = "评分")
    private Long source;

    /** 评分 */
    @Excel(name = "成员")
    private Member member;

    @Excel(name = "商品")
    private Goods goods;

    @Excel(name = "图片")
    private String images;

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setSource(Long source) 
    {
        this.source = source;
    }

    public Long getSource() 
    {
        return source;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("content", getContent())
            .append("goodsId", getGoodsId())
            .append("memberId", getMemberId())
            .append("source", getSource())
            .toString();
    }
}
