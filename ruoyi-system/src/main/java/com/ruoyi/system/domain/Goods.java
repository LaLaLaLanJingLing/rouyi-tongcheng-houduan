package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 优惠券列对象 goods
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public class Goods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 点击数 */
    @Excel(name = "点击数")
    private Long hits;

    /** 展示图片 */
    @Excel(name = "展示图片")
    private String image;

    /** 介绍 */
    @Excel(name = "介绍")
    private String introduction;

    /** 介绍 */
    @Excel(name = "使用说明")
    private String useIntroduction;

    /** 是否上架 */
    @Excel(name = "是否上架")
    private Boolean isMarketable;

    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private Boolean isTop;

    /** 市场价 */
    @Excel(name = "市场价")
    private Double marketPrice;

    /** 月点击数 */
    @Excel(name = "月点击数")
    private Long monthHits;

    /** 月点击数更新日期 */
    @Excel(name = "月点击数更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date monthHitsDate;

    /** 月销量 */
    @Excel(name = "月销量")
    private Long monthSales;

    /** 月销量更新日期 */
    @Excel(name = "月销量更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date monthSalesDate;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 参数值 */
    @Excel(name = "参数值")
    private String parameterValues;

    /** 价格 */
    @Excel(name = "价格")
    private Double price;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String productImages;

    /** 销量 */
    @Excel(name = "销量")
    private Long sales;

    /** 页面描述 */
    @Excel(name = "页面描述")
    private String useDescription;

    /** 编号 */
    @Excel(name = "编号")
    private String sn;

    /** 规格项 */
    @Excel(name = "规格项")
    private String specificationItems;

    /** 总评分 */
    @Excel(name = "总评分")
    private Long totalScore;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 周点击数 */
    @Excel(name = "周点击数")
    private Long weekHits;

    /** 周点击数更新时间 */
    @Excel(name = "周点击数更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date weekHitsDate;

    /** 周销量 */
    @Excel(name = "周销量")
    private Long weekSales;

    /** 周销量更新时间 */
    @Excel(name = "周销量更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date weekSalesDate;

    /** 商家 */
    @Excel(name = "商家")
    private Long businessshopId;

    /** 商品分类 */
    @Excel(name = "商品分类")
    private Long productCategoryId;

    /** 序号 */
    @Excel(name = "序号")
    private Long orderId;

    /** 限制购买数量 */
    @Excel(name = "限制购买数量")
    private Long limitSale;

    /** 关联商品id */
    @Excel(name = "关联商品id")
    private Long relateId;

    /** 关联商品id */
    @Excel(name = "库存")
    private Long stock;

    public Long getRelateId() {
        return relateId;
    }

    public void setRelateId(Long relateId) {
        this.relateId = relateId;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    /** 商家 */
    private ProductCategory productCategory;

    /** 商家 */
    private BusinessShop businessShop;

    public BusinessShop getBusinessShop() {
        return businessShop;
    }

    public void setBusinessShop(BusinessShop businessShop) {
        this.businessShop = businessShop;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHits(Long hits) 
    {
        this.hits = hits;
    }

    public Long getHits() 
    {
        return hits;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setIntroduction(String introduction) 
    {
        this.introduction = introduction;
    }

    public String getIntroduction() 
    {
        return introduction;
    }
    public void setIsMarketable(Boolean isMarketable)
    {
        this.isMarketable = isMarketable;
    }

    public Boolean getIsMarketable()
    {
        return isMarketable;
    }
    public void setIsTop(Boolean isTop)
    {
        this.isTop = isTop;
    }

    public Boolean getIsTop()
    {
        return isTop;
    }
    public void setMarketPrice(Double marketPrice) 
    {
        this.marketPrice = marketPrice;
    }

    public Double getMarketPrice() 
    {
        return marketPrice;
    }
    public void setMonthHits(Long monthHits) 
    {
        this.monthHits = monthHits;
    }

    public Long getMonthHits() 
    {
        return monthHits;
    }
    public void setMonthHitsDate(Date monthHitsDate) 
    {
        this.monthHitsDate = monthHitsDate;
    }

    public Date getMonthHitsDate() 
    {
        return monthHitsDate;
    }
    public void setMonthSales(Long monthSales) 
    {
        this.monthSales = monthSales;
    }

    public Long getMonthSales() 
    {
        return monthSales;
    }
    public void setMonthSalesDate(Date monthSalesDate) 
    {
        this.monthSalesDate = monthSalesDate;
    }

    public Date getMonthSalesDate() 
    {
        return monthSalesDate;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setParameterValues(String parameterValues) 
    {
        this.parameterValues = parameterValues;
    }

    public String getParameterValues() 
    {
        return parameterValues;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setProductImages(String productImages) 
    {
        this.productImages = productImages;
    }

    public String getProductImages() 
    {
        return productImages;
    }
    public void setSales(Long sales) 
    {
        this.sales = sales;
    }

    public Long getSales() 
    {
        return sales;
    }
    public void setUseDescription(String useDescription) 
    {
        this.useDescription = useDescription;
    }

    public String getUseDescription() 
    {
        return useDescription;
    }
    public void setSn(String sn) 
    {
        this.sn = sn;
    }

    public String getSn() 
    {
        return sn;
    }
    public void setSpecificationItems(String specificationItems) 
    {
        this.specificationItems = specificationItems;
    }

    public String getSpecificationItems() 
    {
        return specificationItems;
    }
    public void setTotalScore(Long totalScore) 
    {
        this.totalScore = totalScore;
    }

    public Long getTotalScore() 
    {
        return totalScore;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setWeekHits(Long weekHits) 
    {
        this.weekHits = weekHits;
    }

    public Long getWeekHits() 
    {
        return weekHits;
    }
    public void setWeekHitsDate(Date weekHitsDate) 
    {
        this.weekHitsDate = weekHitsDate;
    }

    public Date getWeekHitsDate() 
    {
        return weekHitsDate;
    }
    public void setWeekSales(Long weekSales) 
    {
        this.weekSales = weekSales;
    }

    public Long getWeekSales() 
    {
        return weekSales;
    }
    public void setWeekSalesDate(Date weekSalesDate) 
    {
        this.weekSalesDate = weekSalesDate;
    }

    public Date getWeekSalesDate() 
    {
        return weekSalesDate;
    }
    public void setBusinessshopId(Long businessshopId) 
    {
        this.businessshopId = businessshopId;
    }

    public Long getBusinessshopId() 
    {
        return businessshopId;
    }
    public void setProductCategoryId(Long productCategoryId) 
    {
        this.productCategoryId = productCategoryId;
    }

    public Long getProductCategoryId() 
    {
        return productCategoryId;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setLimitSale(Long limitSale) 
    {
        this.limitSale = limitSale;
    }

    public Long getLimitSale() 
    {
        return limitSale;
    }


    public String getUseIntroduction() {
        return useIntroduction;
    }

    public void setUseIntroduction(String useIntroduction) {
        this.useIntroduction = useIntroduction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hits", getHits())
            .append("image", getImage())
            .append("introduction", getIntroduction())
            .append("useIntroduction", getUseIntroduction())
            .append("isMarketable", getIsMarketable())
            .append("isTop", getIsTop())
            .append("marketPrice", getMarketPrice())
            .append("monthHits", getMonthHits())
            .append("monthHitsDate", getMonthHitsDate())
            .append("monthSales", getMonthSales())
            .append("monthSalesDate", getMonthSalesDate())
            .append("name", getName())
            .append("parameterValues", getParameterValues())
            .append("price", getPrice())
            .append("productImages", getProductImages())
            .append("sales", getSales())
            .append("useDescription", getUseDescription())
            .append("sn", getSn())
            .append("specificationItems", getSpecificationItems())
            .append("totalScore", getTotalScore())
            .append("type", getType())
            .append("weekHits", getWeekHits())
            .append("weekHitsDate", getWeekHitsDate())
            .append("weekSales", getWeekSales())
            .append("weekSalesDate", getWeekSalesDate())
            .append("businessshopId", getBusinessshopId())
            .append("productCategoryId", getProductCategoryId())
            .append("orderId", getOrderId())
            .append("limitSale", getLimitSale())
            .toString();
    }
}
