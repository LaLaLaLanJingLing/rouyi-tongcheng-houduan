package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品分类对象 product_category
 * 
 * @author ruoyi
 * @date 2019-10-24
 */
public class ProductCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** ID */
    private Long id;

    /** 排序 */
    @Excel(name = "排序")
    private Long order;

    /** 层级 */
    @Excel(name = "层级")
    private Long grade;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 是否上架 */
    @Excel(name = "是否上架")
    private Boolean isMarketable;

    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private Boolean isTop;

    /** 展示图片 */
    @Excel(name = "展示图片")
    private String image;

    /** 上级分类 */
    @Excel(name = "上级分类")
    private Long parentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrder(Long order) 
    {
        this.order = order;
    }

    public Long getOrder() 
    {
        return order;
    }
    public void setGrade(Long grade) 
    {
        this.grade = grade;
    }

    public Long getGrade() 
    {
        return grade;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIsMarketable(Boolean isMarketable)
    {
        this.isMarketable = isMarketable;
    }

    public Boolean getIsMarketable()
    {
        return isMarketable;
    }
    public void setIsTop(Boolean isTop)
    {
        this.isTop = isTop;
    }

    public Boolean getIsTop()
    {
        return isTop;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("order", getOrder())
            .append("grade", getGrade())
            .append("name", getName())
            .append("isMarketable", getIsMarketable())
            .append("isTop", getIsTop())
            .append("image", getImage())
            .append("parentId", getParentId())
            .toString();
    }
}
