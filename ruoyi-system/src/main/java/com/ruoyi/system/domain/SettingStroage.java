package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 存储插件配置对象 setting_stroage
 * 
 * @author ruoyi
 * @date 2019-11-04
 */
public class SettingStroage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** ACCESS_ID */
    @Excel(name = "ACCESS_ID")
    private String accessId;

    /** ACCESS_KEY */
    @Excel(name = "ACCESS_KEY")
    private String accessKey;

    /** BUCKET_NAME */
    @Excel(name = "BUCKET_NAME")
    private String bucketName;

    /** URL前缀 */
    @Excel(name = "URL前缀")
    private String urlPri;

    /** 排序 */
    @Excel(name = "排序")
    private Long order;

    /** 是否启用 */
    @Excel(name = "是否启用")
    private Boolean isUsed;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setAccessId(String accessId) 
    {
        this.accessId = accessId;
    }

    public String getAccessId() 
    {
        return accessId;
    }
    public void setAccessKey(String accessKey) 
    {
        this.accessKey = accessKey;
    }

    public String getAccessKey() 
    {
        return accessKey;
    }
    public void setBucketName(String bucketName) 
    {
        this.bucketName = bucketName;
    }

    public String getBucketName() 
    {
        return bucketName;
    }
    public void setUrlPri(String urlPri) 
    {
        this.urlPri = urlPri;
    }

    public String getUrlPri() 
    {
        return urlPri;
    }
    public void setOrder(Long order) 
    {
        this.order = order;
    }

    public Long getOrder() 
    {
        return order;
    }
    public void setIsUsed(Boolean isUsed)
    {
        this.isUsed = isUsed;
    }

    public Boolean getIsUsed()
    {
        return isUsed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("accessId", getAccessId())
            .append("accessKey", getAccessKey())
            .append("bucketName", getBucketName())
            .append("urlPri", getUrlPri())
            .append("order", getOrder())
            .append("isUsed", getIsUsed())
            .toString();
    }
}
