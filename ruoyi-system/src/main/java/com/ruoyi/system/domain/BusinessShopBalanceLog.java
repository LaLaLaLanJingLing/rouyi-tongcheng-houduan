package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 商户余额记录对象 business_shop_balance_log
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public class BusinessShopBalanceLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户余额记录表 */
    private Long id;

    /** 商户信息表id */
    @Excel(name = "商户信息表id")
    private Long shopId;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String operator;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    /** 创建时间 */
    @Excel(name = "修改金额")
    private double changAmount;

    @Excel(name = "提现id")
    private Long applyId;

    @Excel(name = "类型")
    private Long type;


    public double getChangAmount() {
        return changAmount;
    }

    public void setChangAmount(double changeAmount) {
        this.changAmount = changeAmount;
    }

    public Long getApplyId() {
        return applyId;
    }

    public void setApplyId(Long applyId) {
        this.applyId = applyId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setOperator(String operator) 
    {
        this.operator = operator;
    }

    public String getOperator() 
    {
        return operator;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopId", getShopId())
            .append("status", getStatus())
            .append("operator", getOperator())
            .append("createDate", getCreateDate())
            .append("remark", getRemark())
            .toString();
    }
}
