package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 活动对象 shop_activity
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
public class ShopActivity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 排序 */
    @Excel(name = "排序")
    private Long orders;

    /** 起始日期 */
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginDate;

    /** 结束日期 */
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 路径 */
    @Excel(name = "路径")
    private String image;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 类型 */
    @Excel(name = "类型")
    private Long type;

    /** 关联对象类型 */
    @Excel(name = "关联对象类型")
    private Long relateType;

    /** 关联对象 */
    @Excel(name = "关联对象")
    private Long relateId;

    /** 限制数量 */
    @Excel(name = "限制数量")
    private Long limitNumber;

    /** 限制数量 */
    @Excel(name = "已售数量")
    private Long saleNumber;

    /** 是否上架 */
    @Excel(name = "是否上架")
    private Boolean isTop;

    /** 是否上架 */
    @Excel(name = "是否上架")
    private Boolean isDelete;

    /** $column.columnComment */
    @Excel(name = "是否上架")
    private Boolean isMarketable;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrders(Long orders) 
    {
        this.orders = orders;
    }

    public Long getOrders() 
    {
        return orders;
    }
    public void setBeginDate(Date beginDate) 
    {
        this.beginDate = beginDate;
    }

    public Date getBeginDate() 
    {
        return beginDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setRelateType(Long relateType) 
    {
        this.relateType = relateType;
    }

    public Long getRelateType() 
    {
        return relateType;
    }
    public void setRelateId(Long relateId) 
    {
        this.relateId = relateId;
    }

    public Long getRelateId() 
    {
        return relateId;
    }
    public void setLimitNumber(Long limitNumber) 
    {
        this.limitNumber = limitNumber;
    }

    public Long getLimitNumber() 
    {
        return limitNumber;
    }
    public void setIsTop(Boolean isTop)
    {
        this.isTop = isTop;
    }

    public Boolean getIsTop()
    {
        return isTop;
    }
    public void setIsDelete(Boolean isDelete)
    {
        this.isDelete = isDelete;
    }

    public Boolean getIsDelete()
    {
        return isDelete;
    }
    public void setIsMarketable(Boolean isMarketable)
    {
        this.isMarketable = isMarketable;
    }

    public Boolean getIsMarketable()
    {
        return isMarketable;
    }

    public Long getSaleNumber() {
        return saleNumber;
    }

    public void setSaleNumber(Long saleNumber) {
        this.saleNumber = saleNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orders", getOrders())
            .append("beginDate", getBeginDate())
            .append("endDate", getEndDate())
            .append("image", getImage())
            .append("title", getTitle())
            .append("type", getType())
            .append("relateType", getRelateType())
            .append("relateId", getRelateId())
            .append("limitNumber", getLimitNumber())
            .append("isTop", getIsTop())
            .append("isDelete", getIsDelete())
            .append("isMarketable", getIsMarketable())
            .append("saleNumber", getSaleNumber())
            .toString();
    }
}
