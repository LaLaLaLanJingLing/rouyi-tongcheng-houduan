package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收货信息对象 receiver
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public class Receiver extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 地区名称 */
    @Excel(name = "地区名称")
    private String areaName;

    /** 收货人 */
    @Excel(name = "收货人")
    private String consignee;

    /** 是否默认 */
    @Excel(name = "是否默认")
    private Long isDefault;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 地区 */
    @Excel(name = "地区")
    private Long areaId;

    /** 会员 */
    @Excel(name = "会员")
    private Long memberId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setConsignee(String consignee) 
    {
        this.consignee = consignee;
    }

    public String getConsignee() 
    {
        return consignee;
    }
    public void setIsDefault(Long isDefault) 
    {
        this.isDefault = isDefault;
    }

    public Long getIsDefault() 
    {
        return isDefault;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("address", getAddress())
            .append("areaName", getAreaName())
            .append("consignee", getConsignee())
            .append("isDefault", getIsDefault())
            .append("phone", getPhone())
            .append("areaId", getAreaId())
            .append("memberId", getMemberId())
            .toString();
    }
}
