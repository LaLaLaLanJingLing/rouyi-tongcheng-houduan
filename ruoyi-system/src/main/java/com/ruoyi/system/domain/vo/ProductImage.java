package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 优惠券展示图片对象
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public class ProductImage
{
    private static final long serialVersionUID = 1L;

    /** 图片id */
    @Excel(name = "图片id")
    private Long imageId;

    /** 展示图片 */
    @Excel(name = "展示图片")
    private String image;

    /** 序号 */
    @Excel(name = "序号")
    private Long orderId;

    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }

    public void setImageId(Long imageId)
    {
        this.imageId = imageId;
    }

    public Long getImageId()
    {
        return imageId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("image", getImage())
            .append("orderId", getOrderId())
            .append("imageId", getImageId()  )
            .toString();
    }
}
