package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 商户银行信息对象 business_shop_card
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public class BusinessShopCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户银行信息表 */
    private Long id;

    /** 商户信息表id */
    @Excel(name = "商户信息表id")
    private Long shopId;

    /** 卡号 */
    @Excel(name = "卡号")
    private String cardNumber;

    /** 开户银行 */
    @Excel(name = "开户银行")
    private String bankName;

    /** 开户人姓名 */
    @Excel(name = "开户人姓名")
    private String username;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    private BusinessShop businessShop;

    public BusinessShop getBusinessShop() {
        return businessShop;
    }

    public void setBusinessShop(BusinessShop businessShop) {
        this.businessShop = businessShop;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setCardNumber(String cardNumber) 
    {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() 
    {
        return cardNumber;
    }
    public void setBankName(String bankName) 
    {
        this.bankName = bankName;
    }

    public String getBankName() 
    {
        return bankName;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopId", getShopId())
            .append("cardNumber", getCardNumber())
            .append("bankName", getBankName())
            .append("username", getUsername())
            .append("status", getStatus())
            .append("createDate", getCreateDate())
            .append("remark", getRemark())
            .toString();
    }
}
