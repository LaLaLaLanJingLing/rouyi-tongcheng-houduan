package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商户信息对象 business_shop_info
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public class BusinessShopInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户信息表id */
    private Long id;

    /** 商户信息表id */
    @Excel(name = "商户信息表id")
    private Long shopId;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 余额 */
    @Excel(name = "余额")
    private Double balance;

    /** 积分 */
    @Excel(name = "积分")
    private Double points;

    @Excel(name = "商户")
    private BusinessShop businessShop;


    public BusinessShop getBusinessShop() {
        return businessShop;
    }

    public void setBusinessShop(BusinessShop businessShop) {
        this.businessShop = businessShop;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setBalance(Double balance) 
    {
        this.balance = balance;
    }

    public Double getBalance() 
    {
        return balance;
    }
    public void setPoints(Double points) 
    {
        this.points = points;
    }

    public Double getPoints() 
    {
        return points;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopId", getShopId())
            .append("status", getStatus())
            .append("balance", getBalance())
            .append("points", getPoints())
            .append("remark", getRemark())
            .toString();
    }
}
