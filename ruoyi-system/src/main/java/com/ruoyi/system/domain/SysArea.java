package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 区域对象 sys_area
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public class SysArea extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 订单 */
    @Excel(name = "订单")
    private Long orders;

    /** 全称 */
    @Excel(name = "全称")
    private String fullName;

    /** 层级 */
    @Excel(name = "层级")
    private Long grade;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 树路径 */
    @Excel(name = "树路径")
    private String treePath;

    /** 上级地区ID */
    @Excel(name = "上级地区ID")
    private Long parentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrders(Long orders) 
    {
        this.orders = orders;
    }

    public Long getOrders() 
    {
        return orders;
    }
    public void setFullName(String fullName) 
    {
        this.fullName = fullName;
    }

    public String getFullName() 
    {
        return fullName;
    }
    public void setGrade(Long grade) 
    {
        this.grade = grade;
    }

    public Long getGrade() 
    {
        return grade;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTreePath(String treePath) 
    {
        this.treePath = treePath;
    }

    public String getTreePath() 
    {
        return treePath;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orders", getOrders())
            .append("fullName", getFullName())
            .append("grade", getGrade())
            .append("name", getName())
            .append("treePath", getTreePath())
            .append("parentId", getParentId())
            .toString();
    }
}
