package com.ruoyi.system.service;

import com.ruoyi.system.domain.BusinessShopBalanceLog;
import java.util.List;

/**
 * 商户余额记录Service接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface IBusinessShopBalanceLogService 
{
    /**
     * 查询商户余额记录
     * 
     * @param id 商户余额记录ID
     * @return 商户余额记录
     */
    public BusinessShopBalanceLog selectBusinessShopBalanceLogById(Long id);

    /**
     * 查询商户余额记录列表
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 商户余额记录集合
     */
    public List<BusinessShopBalanceLog> selectBusinessShopBalanceLogList(BusinessShopBalanceLog businessShopBalanceLog);

    /**
     * 新增商户余额记录
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 结果
     */
    public int insertBusinessShopBalanceLog(BusinessShopBalanceLog businessShopBalanceLog);

    /**
     * 修改商户余额记录
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 结果
     */
    public int updateBusinessShopBalanceLog(BusinessShopBalanceLog businessShopBalanceLog);

    /**
     * 批量删除商户余额记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopBalanceLogByIds(String ids);

    /**
     * 删除商户余额记录信息
     * 
     * @param id 商户余额记录ID
     * @return 结果
     */
    public int deleteBusinessShopBalanceLogById(Long id);
}
