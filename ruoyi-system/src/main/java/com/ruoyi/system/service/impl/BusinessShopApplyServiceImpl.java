package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopApplyMapper;
import com.ruoyi.system.domain.BusinessShopApply;
import com.ruoyi.system.service.IBusinessShopApplyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户提现申请Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Service
public class BusinessShopApplyServiceImpl implements IBusinessShopApplyService 
{
    @Autowired
    private BusinessShopApplyMapper businessShopApplyMapper;

    /**
     * 查询商户提现申请
     * 
     * @param id 商户提现申请ID
     * @return 商户提现申请
     */
    @Override
    public BusinessShopApply selectBusinessShopApplyById(Long id)
    {
        return businessShopApplyMapper.selectBusinessShopApplyById(id);
    }

    /**
     * 查询商户提现申请列表
     * 
     * @param businessShopApply 商户提现申请
     * @return 商户提现申请
     */
    @Override
    public List<BusinessShopApply> selectBusinessShopApplyList(BusinessShopApply businessShopApply)
    {
        return businessShopApplyMapper.selectBusinessShopApplyList(businessShopApply);
    }

    /**
     * 新增商户提现申请
     * 
     * @param businessShopApply 商户提现申请
     * @return 结果
     */
    @Override
    public int insertBusinessShopApply(BusinessShopApply businessShopApply)
    {
        return businessShopApplyMapper.insertBusinessShopApply(businessShopApply);
    }

    /**
     * 修改商户提现申请
     * 
     * @param businessShopApply 商户提现申请
     * @return 结果
     */
    @Override
    public int updateBusinessShopApply(BusinessShopApply businessShopApply)
    {
        return businessShopApplyMapper.updateBusinessShopApply(businessShopApply);
    }

    /**
     * 删除商户提现申请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopApplyByIds(String ids)
    {
        return businessShopApplyMapper.deleteBusinessShopApplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户提现申请信息
     * 
     * @param id 商户提现申请ID
     * @return 结果
     */
    public int deleteBusinessShopApplyById(Long id)
    {
        return businessShopApplyMapper.deleteBusinessShopApplyById(id);
    }
}
