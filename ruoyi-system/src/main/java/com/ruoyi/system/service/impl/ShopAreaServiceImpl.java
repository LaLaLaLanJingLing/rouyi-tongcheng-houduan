package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopAreaMapper;
import com.ruoyi.system.domain.ShopArea;
import com.ruoyi.system.service.IShopAreaService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商家地区Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-06
 */
@Service
public class ShopAreaServiceImpl implements IShopAreaService 
{
    @Autowired
    private ShopAreaMapper shopAreaMapper;

    /**
     * 查询商家地区
     * 
     * @param id 商家地区ID
     * @return 商家地区
     */
    @Override
    public ShopArea selectShopAreaById(Long id)
    {
        return shopAreaMapper.selectShopAreaById(id);
    }

    /**
     * 查询商家地区列表
     * 
     * @param shopArea 商家地区
     * @return 商家地区
     */
    @Override
    public List<ShopArea> selectShopAreaList(ShopArea shopArea)
    {
        return shopAreaMapper.selectShopAreaList(shopArea);
    }

    /**
     * 新增商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    @Override
    public int insertShopArea(ShopArea shopArea)
    {
        return shopAreaMapper.insertShopArea(shopArea);
    }

    /**
     * 修改商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    @Override
    public int updateShopArea(ShopArea shopArea)
    {
        return shopAreaMapper.updateShopArea(shopArea);
    }

    /**
     * 删除商家地区对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopAreaByIds(String ids)
    {
        return shopAreaMapper.deleteShopAreaByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商家地区信息
     * 
     * @param id 商家地区ID
     * @return 结果
     */
    public int deleteShopAreaById(Long id)
    {
        return shopAreaMapper.deleteShopAreaById(id);
    }

    /**
     * 查询商家地区树列表
     * 
     * @return 所有商家地区信息
     */
    @Override
    public List<Ztree> selectShopAreaTree()
    {
        List<ShopArea> shopAreaList = shopAreaMapper.selectShopAreaList(new ShopArea());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (ShopArea shopArea : shopAreaList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(shopArea.getId());
            ztree.setpId(shopArea.getParentId());
            ztree.setName(shopArea.getName());
            ztree.setTitle(shopArea.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
