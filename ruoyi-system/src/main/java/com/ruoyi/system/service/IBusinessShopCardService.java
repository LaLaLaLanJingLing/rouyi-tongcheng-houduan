package com.ruoyi.system.service;

import com.ruoyi.system.domain.BusinessShopCard;
import java.util.List;

/**
 * 商户银行信息Service接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface IBusinessShopCardService 
{
    /**
     * 查询商户银行信息
     * 
     * @param id 商户银行信息ID
     * @return 商户银行信息
     */
    public BusinessShopCard selectBusinessShopCardById(Long id);

    /**
     * 查询商户银行信息列表
     * 
     * @param businessShopCard 商户银行信息
     * @return 商户银行信息集合
     */
    public List<BusinessShopCard> selectBusinessShopCardList(BusinessShopCard businessShopCard);

    /**
     * 新增商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    public int insertBusinessShopCard(BusinessShopCard businessShopCard);

    /**
     * 修改商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    public int updateBusinessShopCard(BusinessShopCard businessShopCard);

    /**
     * 批量删除商户银行信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopCardByIds(String ids);

    /**
     * 删除商户银行信息信息
     * 
     * @param id 商户银行信息ID
     * @return 结果
     */
    public int deleteBusinessShopCardById(Long id);
}
