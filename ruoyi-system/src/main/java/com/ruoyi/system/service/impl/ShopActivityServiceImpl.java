package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopActivityMapper;
import com.ruoyi.system.domain.ShopActivity;
import com.ruoyi.system.service.IShopActivityService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
@Service
public class ShopActivityServiceImpl implements IShopActivityService 
{
    @Autowired
    private ShopActivityMapper shopActivityMapper;

    /**
     * 查询活动
     * 
     * @param id 活动ID
     * @return 活动
     */
    @Override
    public ShopActivity selectShopActivityById(Long id)
    {
        return shopActivityMapper.selectShopActivityById(id);
    }

    /**
     * 查询活动列表
     * 
     * @param shopActivity 活动
     * @return 活动
     */
    @Override
    public List<ShopActivity> selectShopActivityList(ShopActivity shopActivity)
    {
        return shopActivityMapper.selectShopActivityList(shopActivity);
    }

    /**
     * 新增活动
     * 
     * @param shopActivity 活动
     * @return 结果
     */
    @Override
    public int insertShopActivity(ShopActivity shopActivity)
    {
        return shopActivityMapper.insertShopActivity(shopActivity);
    }

    /**
     * 修改活动
     * 
     * @param shopActivity 活动
     * @return 结果
     */
    @Override
    public int updateShopActivity(ShopActivity shopActivity)
    {
        return shopActivityMapper.updateShopActivity(shopActivity);
    }

    /**
     * 删除活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopActivityByIds(String ids)
    {
        return shopActivityMapper.deleteShopActivityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动信息
     * 
     * @param id 活动ID
     * @return 结果
     */
    public int deleteShopActivityById(Long id)
    {
        return shopActivityMapper.deleteShopActivityById(id);
    }
}
