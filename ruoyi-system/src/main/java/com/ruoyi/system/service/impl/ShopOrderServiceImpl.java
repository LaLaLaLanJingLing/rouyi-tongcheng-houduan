package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.utils.IdGenerator;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.ShopOrderItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopOrderMapper;
import com.ruoyi.system.service.IShopOrderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Service
public class ShopOrderServiceImpl implements IShopOrderService 
{
    @Autowired
    private ShopOrderMapper shopOrderMapper;

    @Autowired
    private ShopOrderItemMapper shopOrderItemMapper;


    public static final IdGenerator ID_GENERATOR = IdGenerator.INSTANCE;

    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public ShopOrder selectShopOrderById(Long id)
    {
        return shopOrderMapper.selectShopOrderById(id);
    }

    @Override
    public ShopOrder selectShopOrderBySn(String sn) {
        return shopOrderMapper.selectShopOrderBySn(sn);
    }

    /**
     * 查询订单列表
     * 
     * @param shopOrder 订单
     * @return 订单
     */
    @Override
    public List<ShopOrder> selectShopOrderList(ShopOrder shopOrder)
    {
        return shopOrderMapper.selectShopOrderList(shopOrder);
    }

    /**
     * 新增订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    @Override
    public int insertShopOrder(ShopOrder shopOrder)
    {
        return shopOrderMapper.insertShopOrder(shopOrder);
    }

    /**
     * 修改订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    @Override
    public int updateShopOrder(ShopOrder shopOrder)
    {
        return shopOrderMapper.updateShopOrder(shopOrder);
    }

    /**
     * 删除订单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopOrderByIds(String ids)
    {
        return shopOrderMapper.deleteShopOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteShopOrderById(Long id)
    {
        return shopOrderMapper.deleteShopOrderById(id);
    }


    /**
     * 创建订单
     *
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public ShopOrder createOrder(Long type, Goods goods,
                                 BusinessShop businessShop,
                                 String phone,
                                 String consignee,
                                 Member member) {
        if(goods == null ||  businessShop == null){
            return null;
        }

        ShopOrder order = new ShopOrder();
        order.setStatus(Long.valueOf(ShopOrder.Status.pendingPayment.ordinal()));
        order.setType(type);
        order.setAddress(null);
        order.setPhone(phone);
        order.setConsignee(consignee);
        order.setSn(ID_GENERATOR.nextId());
        order.setPrice(goods.getPrice());
        order.setFee(BigDecimal.ZERO.doubleValue());
        order.setFreight(BigDecimal.ZERO.doubleValue());
        order.setOffsetAmount(BigDecimal.ZERO.doubleValue());
        order.setAmountPaid(BigDecimal.ZERO.doubleValue());
        order.setRefundAmount(BigDecimal.ZERO.doubleValue());
        order.setQuantity(1L);
        order.setReturnedQuantity(0L);
        order.setAreaName(StringUtils.EMPTY);
        order.setAddress(StringUtils.EMPTY);
        order.setZipCode(StringUtils.EMPTY);
        order.setAreaId(0L);
        order.setMemo("");
        order.setMemberId(member.getId());
        order.setDeleteFlag(0L);
        order.setBusinessShopId(businessShop.getId());
        order.setCreateDate(new Date());
        order.setModifyDate(new Date());
        order.setAmount(goods.getPrice().doubleValue());
        order.setCouponDiscount(BigDecimal.ZERO.doubleValue());
        order.setSource(0L);
        order.setTax(BigDecimal.ZERO.doubleValue());


        BigDecimal oprice = new BigDecimal(0);
        List<ShopOrder> orderItems = new ArrayList<>();

        //暂时只会产生一个OrderItem 引入商品买卖后 会产生购物车的概念出来
        ShopOrderItem orderItem = new ShopOrderItem();
        orderItem.setGoodsId(goods.getId());
        orderItem.setQuantity(1L);
        orderItem.setIsUsed(false);
        orderItem.setReturnedQuantity(0L);
        orderItem.setShippedQuantity(0L);
        orderItem.setName(goods.getName());
        orderItem.setSn(goods.getSn());
        orderItem.setImage(goods.getImage());
        orderItem.setPrice(goods.getPrice());
        insertShopOrder(order);
        orderItem.setOrderId(order.getId());
        orderItem.setType(0L);

        shopOrderItemMapper.insertShopOrderItem(orderItem);
        List<ShopOrderItem> shopOrderItemList = new ArrayList<>();
        shopOrderItemList.add(orderItem);
        order.setShopOrderItems(shopOrderItemList);
        return order;
    }


    @Override
    public double selectShopSaleTotalAmountByMonth(Long shopId, Long month) {
        return shopOrderMapper.selectSaleTotalAmountByMonth(shopId, month);
    }

    @Override
    public int selectShopSaleTotalNumberByMonth(Long shopId, Long month) {
        return shopOrderMapper.selectSaleTotalNumberByMonth(shopId, month);
    }
}
