package com.ruoyi.system.service;

import com.ruoyi.system.domain.BusinessShopInfo;
import java.util.List;

/**
 * 商户信息Service接口
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
public interface IBusinessShopInfoService 
{
    /**
     * 查询商户信息
     * 
     * @param id 商户信息ID
     * @return 商户信息
     */
    public BusinessShopInfo selectBusinessShopInfoById(Long id);

    /**
     * 查询商户信息列表
     * 
     * @param businessShopInfo 商户信息
     * @return 商户信息集合
     */
    public List<BusinessShopInfo> selectBusinessShopInfoList(BusinessShopInfo businessShopInfo);

    /**
     * 新增商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    public int insertBusinessShopInfo(BusinessShopInfo businessShopInfo);

    /**
     * 修改商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    public int updateBusinessShopInfo(BusinessShopInfo businessShopInfo);

    /**
     * 批量删除商户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBusinessShopInfoByIds(String ids);

    /**
     * 删除商户信息信息
     * 
     * @param id 商户信息ID
     * @return 结果
     */
    public int deleteBusinessShopInfoById(Long id);


    /**
     * 删除商户信息信息
     *
     * @param shopId 商户ID
     * @return 结果
     */
    public BusinessShopInfo selectBusinessShopInfoByShopId(Long shopId);
}
