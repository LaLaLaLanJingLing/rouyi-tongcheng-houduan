package com.ruoyi.system.service;

import com.ruoyi.system.domain.ShopMemberComment;
import java.util.List;

/**
 * 评论Service接口
 * 
 * @author ruoyi
 * @date 2019-11-26
 */
public interface IShopMemberCommentService 
{
    /**
     * 查询评论
     * 
     * @param id 评论ID
     * @return 评论
     */
    public ShopMemberComment selectShopMemberCommentById(Long id);

    /**
     * 查询评论列表
     * 
     * @param shopMemberComment 评论
     * @return 评论集合
     */
    public List<ShopMemberComment> selectShopMemberCommentList(ShopMemberComment shopMemberComment);

    /**
     * 新增评论
     * 
     * @param shopMemberComment 评论
     * @return 结果
     */
    public int insertShopMemberComment(ShopMemberComment shopMemberComment);

    /**
     * 修改评论
     * 
     * @param shopMemberComment 评论
     * @return 结果
     */
    public int updateShopMemberComment(ShopMemberComment shopMemberComment);

    /**
     * 批量删除评论
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopMemberCommentByIds(String ids);

    /**
     * 删除评论信息
     * 
     * @param id 评论ID
     * @return 结果
     */
    public int deleteShopMemberCommentById(Long id);
}
