package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopUserMapper;
import com.ruoyi.system.domain.ShopUser;
import com.ruoyi.system.service.IShopUserService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户用户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-03
 */
@Service
public class ShopUserServiceImpl implements IShopUserService 
{
    @Autowired
    private ShopUserMapper shopUserMapper;

    /**
     * 查询商户用户信息
     * 
     * @param id 商户用户信息ID
     * @return 商户用户信息
     */
    @Override
    public ShopUser selectShopUserById(Long id)
    {
        return shopUserMapper.selectShopUserById(id);
    }

    /**
     * 查询商户用户信息列表
     * 
     * @param shopUser 商户用户信息
     * @return 商户用户信息
     */
    @Override
    public List<ShopUser> selectShopUserList(ShopUser shopUser)
    {
        return shopUserMapper.selectShopUserList(shopUser);
    }

    /**
     * 新增商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    @Override
    public int insertShopUser(ShopUser shopUser)
    {
        shopUser.setCreateTime(DateUtils.getNowDate());
        return shopUserMapper.insertShopUser(shopUser);
    }

    /**
     * 修改商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    @Override
    public int updateShopUser(ShopUser shopUser)
    {
        shopUser.setUpdateTime(DateUtils.getNowDate());
        return shopUserMapper.updateShopUser(shopUser);
    }

    /**
     * 删除商户用户信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopUserByIds(String ids)
    {
        return shopUserMapper.deleteShopUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户用户信息信息
     * 
     * @param id 商户用户信息ID
     * @return 结果
     */
    public int deleteShopUserById(Long id)
    {
        return shopUserMapper.deleteShopUserById(id);
    }


    @Override
    public boolean checkLoginNameUnique(String loginName) {
        int count = shopUserMapper.checkLoginNameUnique(loginName);
        if (count > 0){
            return false;
        }else{
            return true;
        }
    }


    @Override
    public boolean checkPhoneUnique(String phone) {
        int count = shopUserMapper.checkPhoneUnique(phone);
        if (count > 0){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public boolean resetUserPwd(ShopUser shopUser) {
        return updateShopUser(shopUser)> 0? true : false;
    }
}
