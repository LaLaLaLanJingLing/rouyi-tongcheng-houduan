package com.ruoyi.system.service;

import com.ruoyi.system.domain.ShopOrderItem;
import java.util.List;

/**
 * 订单单例Service接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface IShopOrderItemService 
{
    /**
     * 查询订单单例
     * 
     * @param id 订单单例ID
     * @return 订单单例
     */
    public ShopOrderItem selectShopOrderItemById(Long id);

    /**
     * 查询订单单例列表
     * 
     * @param shopOrderItem 订单单例
     * @return 订单单例集合
     */
    public List<ShopOrderItem> selectShopOrderItemList(ShopOrderItem shopOrderItem);

    /**
     * 新增订单单例
     * 
     * @param shopOrderItem 订单单例
     * @return 结果
     */
    public int insertShopOrderItem(ShopOrderItem shopOrderItem);

    /**
     * 修改订单单例
     * 
     * @param shopOrderItem 订单单例
     * @return 结果
     */
    public int updateShopOrderItem(ShopOrderItem shopOrderItem);

    /**
     * 批量删除订单单例
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopOrderItemByIds(String ids);

    /**
     * 删除订单单例信息
     * 
     * @param id 订单单例ID
     * @return 结果
     */
    public int deleteShopOrderItemById(Long id);
}
