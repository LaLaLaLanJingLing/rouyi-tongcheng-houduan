package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopCardMapper;
import com.ruoyi.system.domain.BusinessShopCard;
import com.ruoyi.system.service.IBusinessShopCardService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户银行信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Service
public class BusinessShopCardServiceImpl implements IBusinessShopCardService 
{
    @Autowired
    private BusinessShopCardMapper businessShopCardMapper;

    /**
     * 查询商户银行信息
     * 
     * @param id 商户银行信息ID
     * @return 商户银行信息
     */
    @Override
    public BusinessShopCard selectBusinessShopCardById(Long id)
    {
        return businessShopCardMapper.selectBusinessShopCardById(id);
    }

    /**
     * 查询商户银行信息列表
     * 
     * @param businessShopCard 商户银行信息
     * @return 商户银行信息
     */
    @Override
    public List<BusinessShopCard> selectBusinessShopCardList(BusinessShopCard businessShopCard)
    {
        return businessShopCardMapper.selectBusinessShopCardList(businessShopCard);
    }

    /**
     * 新增商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    @Override
    public int insertBusinessShopCard(BusinessShopCard businessShopCard)
    {
        return businessShopCardMapper.insertBusinessShopCard(businessShopCard);
    }

    /**
     * 修改商户银行信息
     * 
     * @param businessShopCard 商户银行信息
     * @return 结果
     */
    @Override
    public int updateBusinessShopCard(BusinessShopCard businessShopCard)
    {
        return businessShopCardMapper.updateBusinessShopCard(businessShopCard);
    }

    /**
     * 删除商户银行信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopCardByIds(String ids)
    {
        return businessShopCardMapper.deleteBusinessShopCardByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户银行信息信息
     * 
     * @param id 商户银行信息ID
     * @return 结果
     */
    public int deleteBusinessShopCardById(Long id)
    {
        return businessShopCardMapper.deleteBusinessShopCardById(id);
    }
}
