package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.ApiResult;
import org.apache.shiro.authc.LockedAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopInfoMapper;
import com.ruoyi.system.domain.BusinessShopInfo;
import com.ruoyi.system.service.IBusinessShopInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Service
public class BusinessShopInfoServiceImpl implements IBusinessShopInfoService 
{

    private static Logger logger = LoggerFactory.getLogger(BusinessShopInfoServiceImpl.class);

    @Autowired
    private BusinessShopInfoMapper businessShopInfoMapper;

    /**
     * 查询商户信息
     * 
     * @param id 商户信息ID
     * @return 商户信息
     */
    @Override
    public BusinessShopInfo selectBusinessShopInfoById(Long id)
    {
        return businessShopInfoMapper.selectBusinessShopInfoById(id);
    }

    /**
     * 查询商户信息列表
     * 
     * @param businessShopInfo 商户信息
     * @return 商户信息
     */
    @Override
    public List<BusinessShopInfo> selectBusinessShopInfoList(BusinessShopInfo businessShopInfo)
    {
        return businessShopInfoMapper.selectBusinessShopInfoList(businessShopInfo);
    }

    /**
     * 新增商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    @Override
    public int insertBusinessShopInfo(BusinessShopInfo businessShopInfo)
    {
        return businessShopInfoMapper.insertBusinessShopInfo(businessShopInfo);
    }

    /**
     * 修改商户信息
     * 
     * @param businessShopInfo 商户信息
     * @return 结果
     */
    @Override
    public int updateBusinessShopInfo(BusinessShopInfo businessShopInfo)
    {
        return businessShopInfoMapper.updateBusinessShopInfo(businessShopInfo);
    }

    /**
     * 删除商户信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopInfoByIds(String ids)
    {
        return businessShopInfoMapper.deleteBusinessShopInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户信息信息
     * 
     * @param id 商户信息ID
     * @return 结果
     */
    public int deleteBusinessShopInfoById(Long id)
    {
        return businessShopInfoMapper.deleteBusinessShopInfoById(id);
    }


    @Override
    public BusinessShopInfo selectBusinessShopInfoByShopId(Long shopId)  {

        BusinessShopInfo selectBusinessShopInfo = new BusinessShopInfo();
        selectBusinessShopInfo.setShopId(shopId);
        List<BusinessShopInfo> businessShopInfoList = selectBusinessShopInfoList(selectBusinessShopInfo);
        if (businessShopInfoList.size() == 0 ){
            return null;
        }
        if (businessShopInfoList.size() > 1){
            logger.error("系统出现问题,发现重复 shopInfo shopId=" + shopId.toString());
            return null;
        }
        BusinessShopInfo businessShopInfo = businessShopInfoList.get(0);

        return businessShopInfo;
    }
}
