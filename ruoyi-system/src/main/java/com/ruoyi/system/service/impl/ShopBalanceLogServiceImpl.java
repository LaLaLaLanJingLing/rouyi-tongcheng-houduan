package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopBalanceLogMapper;
import com.ruoyi.system.domain.ShopBalanceLog;
import com.ruoyi.system.service.IShopBalanceLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 平台余额记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
@Service
public class ShopBalanceLogServiceImpl implements IShopBalanceLogService 
{
    @Autowired
    private ShopBalanceLogMapper shopBalanceLogMapper;

    /**
     * 查询平台余额记录
     * 
     * @param id 平台余额记录ID
     * @return 平台余额记录
     */
    @Override
    public ShopBalanceLog selectShopBalanceLogById(Long id)
    {
        return shopBalanceLogMapper.selectShopBalanceLogById(id);
    }

    /**
     * 查询平台余额记录列表
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 平台余额记录
     */
    @Override
    public List<ShopBalanceLog> selectShopBalanceLogList(ShopBalanceLog shopBalanceLog)
    {
        return shopBalanceLogMapper.selectShopBalanceLogList(shopBalanceLog);
    }

    /**
     * 新增平台余额记录
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 结果
     */
    @Override
    public int insertShopBalanceLog(ShopBalanceLog shopBalanceLog)
    {
        return shopBalanceLogMapper.insertShopBalanceLog(shopBalanceLog);
    }

    /**
     * 修改平台余额记录
     * 
     * @param shopBalanceLog 平台余额记录
     * @return 结果
     */
    @Override
    public int updateShopBalanceLog(ShopBalanceLog shopBalanceLog)
    {
        return shopBalanceLogMapper.updateShopBalanceLog(shopBalanceLog);
    }

    /**
     * 删除平台余额记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopBalanceLogByIds(String ids)
    {
        return shopBalanceLogMapper.deleteShopBalanceLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除平台余额记录信息
     * 
     * @param id 平台余额记录ID
     * @return 结果
     */
    public int deleteShopBalanceLogById(Long id)
    {
        return shopBalanceLogMapper.deleteShopBalanceLogById(id);
    }
}
