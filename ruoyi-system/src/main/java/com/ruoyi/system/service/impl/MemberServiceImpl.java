package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.SysToken;
import com.ruoyi.system.mapper.SysTokenMapper;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MemberMapper;
import com.ruoyi.system.domain.Member;
import com.ruoyi.system.service.IMemberService;
import com.ruoyi.common.core.text.Convert;

import javax.servlet.http.HttpServletRequest;

/**
 * 会员Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
@Service
public class MemberServiceImpl implements IMemberService 
{
    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private SysTokenMapper sysTokenMapper;

    /**
     * 查询会员
     * 
     * @param id 会员ID
     * @return 会员
     */
    @Override
    public Member selectMemberById(Long id)
    {
        return memberMapper.selectMemberById(id);
    }

    @Override
    public Member selectMemberByOpenId(String openId) {
        return memberMapper.selectMemberByOpenId(openId);
    }


    /**
     * 查询会员列表
     * 
     * @param member 会员
     * @return 会员
     */
    @Override
    public List<Member> selectMemberList(Member member)
    {
        return memberMapper.selectMemberList(member);
    }

    /**
     * 新增会员
     * 
     * @param member 会员
     * @return 结果
     */
    @Override
    public int insertMember(Member member)
    {
        return memberMapper.insertMember(member);
    }

    /**
     * 修改会员
     * 
     * @param member 会员
     * @return 结果
     */
    @Override
    public int updateMember(Member member)
    {
        return memberMapper.updateMember(member);
    }

    /**
     * 删除会员对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberByIds(String ids)
    {
        return memberMapper.deleteMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员信息
     * 
     * @param id 会员ID
     * @return 结果
     */
    public int deleteMemberById(Long id)
    {
        return memberMapper.deleteMemberById(id);
    }


    public Member getCurrentMember(HttpServletRequest request) {
        String strToken = request.getHeader("token");
        SysToken sysToken = sysTokenMapper.selectSysTokenByToken(strToken);
        if (sysToken == null){
            return null;
        }
        Member member = selectMemberById(sysToken.getMemberId());
        return member;
    }

}
