package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopGroupMapper;
import com.ruoyi.system.domain.ShopGroup;
import com.ruoyi.system.service.IShopGroupService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分组Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
@Service
public class ShopGroupServiceImpl implements IShopGroupService 
{
    @Autowired
    private ShopGroupMapper shopGroupMapper;

    /**
     * 查询分组
     * 
     * @param id 分组ID
     * @return 分组
     */
    @Override
    public ShopGroup selectShopGroupById(Long id)
    {
        return shopGroupMapper.selectShopGroupById(id);
    }

    /**
     * 查询分组列表
     * 
     * @param shopGroup 分组
     * @return 分组
     */
    @Override
    public List<ShopGroup> selectShopGroupList(ShopGroup shopGroup)
    {
        return shopGroupMapper.selectShopGroupList(shopGroup);
    }

    /**
     * 新增分组
     * 
     * @param shopGroup 分组
     * @return 结果
     */
    @Override
    public int insertShopGroup(ShopGroup shopGroup)
    {
        return shopGroupMapper.insertShopGroup(shopGroup);
    }

    /**
     * 修改分组
     * 
     * @param shopGroup 分组
     * @return 结果
     */
    @Override
    public int updateShopGroup(ShopGroup shopGroup)
    {
        return shopGroupMapper.updateShopGroup(shopGroup);
    }

    /**
     * 删除分组对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopGroupByIds(String ids)
    {
        return shopGroupMapper.deleteShopGroupByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分组信息
     * 
     * @param id 分组ID
     * @return 结果
     */
    public int deleteShopGroupById(Long id)
    {
        return shopGroupMapper.deleteShopGroupById(id);
    }
}
