package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopOrderItemMapper;
import com.ruoyi.system.domain.ShopOrderItem;
import com.ruoyi.system.service.IShopOrderItemService;
import com.ruoyi.common.core.text.Convert;

/**
 * 订单单例Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Service
public class ShopOrderItemServiceImpl implements IShopOrderItemService 
{
    @Autowired
    private ShopOrderItemMapper shopOrderItemMapper;

    /**
     * 查询订单单例
     * 
     * @param id 订单单例ID
     * @return 订单单例
     */
    @Override
    public ShopOrderItem selectShopOrderItemById(Long id)
    {
        return shopOrderItemMapper.selectShopOrderItemById(id);
    }

    /**
     * 查询订单单例列表
     * 
     * @param shopOrderItem 订单单例
     * @return 订单单例
     */
    @Override
    public List<ShopOrderItem> selectShopOrderItemList(ShopOrderItem shopOrderItem)
    {
        return shopOrderItemMapper.selectShopOrderItemList(shopOrderItem);
    }

    /**
     * 新增订单单例
     * 
     * @param shopOrderItem 订单单例
     * @return 结果
     */
    @Override
    public int insertShopOrderItem(ShopOrderItem shopOrderItem)
    {
        return shopOrderItemMapper.insertShopOrderItem(shopOrderItem);
    }

    /**
     * 修改订单单例
     * 
     * @param shopOrderItem 订单单例
     * @return 结果
     */
    @Override
    public int updateShopOrderItem(ShopOrderItem shopOrderItem)
    {
        return shopOrderItemMapper.updateShopOrderItem(shopOrderItem);
    }

    /**
     * 删除订单单例对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopOrderItemByIds(String ids)
    {
        return shopOrderItemMapper.deleteShopOrderItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单单例信息
     * 
     * @param id 订单单例ID
     * @return 结果
     */
    public int deleteShopOrderItemById(Long id)
    {
        return shopOrderItemMapper.deleteShopOrderItemById(id);
    }
}
