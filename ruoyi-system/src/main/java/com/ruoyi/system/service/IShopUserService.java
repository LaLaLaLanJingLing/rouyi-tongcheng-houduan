package com.ruoyi.system.service;

import com.ruoyi.system.domain.ShopUser;
import java.util.List;

/**
 * 商户用户信息Service接口
 * 
 * @author ruoyi
 * @date 2019-12-03
 */
public interface IShopUserService 
{
    /**
     * 查询商户用户信息
     * 
     * @param id 商户用户信息ID
     * @return 商户用户信息
     */
    public ShopUser selectShopUserById(Long id);

    /**
     * 查询商户用户信息列表
     * 
     * @param shopUser 商户用户信息
     * @return 商户用户信息集合
     */
    public List<ShopUser> selectShopUserList(ShopUser shopUser);

    /**
     * 新增商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    public int insertShopUser(ShopUser shopUser);

    /**
     * 修改商户用户信息
     * 
     * @param shopUser 商户用户信息
     * @return 结果
     */
    public int updateShopUser(ShopUser shopUser);


    /**
     * 更改密码
     *
     * @param shopUser 商户用户信息
     * @return 结果
     */
    public boolean resetUserPwd(ShopUser shopUser);

    /**
     * 批量删除商户用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopUserByIds(String ids);

    /**
     * 删除商户用户信息信息
     * 
     * @param id 商户用户信息ID
     * @return 结果
     */
    public int deleteShopUserById(Long id);


    /**
     * 删除商户用户信息信息
     *
     * @param loginName 登录用户名
     * @return 结果
     */
    public boolean checkLoginNameUnique(String loginName);


    /**
     * 删除商户用户信息信息
     *
     * @param phonenumber 登录电话
     * @return 结果
     */
    public boolean checkPhoneUnique(String phonenumber);
}
