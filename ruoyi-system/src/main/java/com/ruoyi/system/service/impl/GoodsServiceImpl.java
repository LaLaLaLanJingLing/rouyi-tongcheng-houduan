package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.ShopGroup;
import com.ruoyi.system.domain.ShopGroupGoods;
import com.ruoyi.system.domain.vo.ProductImage;
import com.ruoyi.system.mapper.ShopGroupGoodsMapper;
import com.ruoyi.system.mapper.ShopGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.GoodsMapper;
import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.service.IGoodsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 优惠券列Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
@Service
public class GoodsServiceImpl implements IGoodsService 
{
    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private ShopGroupGoodsMapper shopGroupGoodsMapper;

    @Autowired
    private ShopGroupMapper shopGroupMapper;

    /**
     * 查询优惠券列
     * 
     * @param id 优惠券列ID
     * @return 优惠券列
     */
    @Override
    public Goods selectGoodsById(Long id)
    {
        return goodsMapper.selectGoodsById(id);
    }

    /**
     * 获取优惠券图片列表
     *
     * @param Goods goods对象
     * @return 优惠券列
     */
    @Override
    public List<ProductImage> getProductImageList(Goods goods) {
        List<ProductImage> productImageList = new ArrayList<>();
        if (goods == null){
            return productImageList;
        }
        String productImageString = goods.getProductImages();
        JSONArray imageArray = JSONArray.parseArray(productImageString);
        if (imageArray.size() == 0){
            return productImageList;
        }
        else{
            for(int i = 0; i < imageArray.size(); i++){
                JSONObject imageObject = imageArray.getJSONObject(i);
                if (imageObject == null ||  imageObject.size() != 3){
                    //error
                    return null;
                }
                ProductImage productImage = new ProductImage();
                productImage.setImage(imageObject.getString("image"));
                productImage.setImageId(imageObject.getLong("imageId"));
                productImage.setOrderId(imageObject.getLong("orderId"));
                productImageList.add(productImage);
            }
        }
        return productImageList;
    }

    /**
     * 查询优惠券列列表
     * 
     * @param goods 优惠券列
     * @return 优惠券列
     */
    @Override
    public List<Goods> selectGoodsList(Goods goods)
    {
        return goodsMapper.selectGoodsList(goods);
    }

    /**
     * 查询优惠券列列表
     *
     * @param Long 分组Id
     * @return 优惠券列
     */
    @Override
    public List<Goods> selectGoodsListByGroupId(Long groupId)
    {
        return goodsMapper.selectGoodsListByGroupId(groupId);
    }


    /**
     * 新增优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    @Override
    public int insertGoods(Goods goods)
    {
        return goodsMapper.insertGoods(goods);
    }

    /**
     * 新增优惠券列
     *
     * @param goodsMap 优惠券列
     * @return 优惠券列集合
     */
    @Override
    public int insertGoods(Map<String, String> goodsMap)
    {
        //根据前端过来的数据 存储goods 和 product //优惠券只有一个 先只有goods 不搞多规格
        Goods goods = new Goods();
        try{
            Integer type = Integer.parseInt(goodsMap.get("type"));
            goods.setType( type.longValue());

            String value = goodsMap.get("businessshopId");
            Integer businessShopId = Integer.parseInt(value);
            goods.setBusinessshopId( businessShopId.longValue());

            Integer productCategoryId = Integer.parseInt(goodsMap.get("productCategoryId"));
            goods.setProductCategoryId( productCategoryId.longValue());

            goods.setSn( goodsMap.get("sn"));
            goods.setName( goodsMap.get("name"));

            BigDecimal price = new BigDecimal(goodsMap.get("price"));
            goods.setPrice(price.doubleValue());

            goods.setImage(goodsMap.get("image"));
            goods.setHits(Long.valueOf(0));

            goods.setIntroduction(goodsMap.get("introduction"));

            BigDecimal marketPrice = new BigDecimal(goodsMap.get("marketPrice"));
            goods.setMarketPrice(marketPrice.doubleValue());

            goods.setMonthHits(Long.valueOf(0));
            goods.setSales(Long.valueOf(goodsMap.get("sales")));
            goods.setStock(Long.valueOf(goodsMap.get("stock")));
            goods.setRelateId(Long.valueOf(goodsMap.get("relateId")));


            Integer isTop = Integer.parseInt(goodsMap.get("isTop"));
            Integer isMarketable = Integer.parseInt(goodsMap.get("isMarketable"));
            goods.setIsMarketable(isMarketable == 1 ?true : false);
            goods.setIsTop(isTop == 1 ?true : false);

            //商品处理要拼接成Json
            goods.setProductImages(goodsMap.get("productImageList"));
            goods.setOrderId(Long.valueOf(0));
            goods.setTotalScore(0L);
            goods.setMonthSales(0L);
            goods.setWeekSales(0L);
            goods.setWeekHits(0L);
            goods.setMonthHits(0L);
            goods.setMonthHitsDate(new Date());
            goods.setWeekSalesDate(new Date());
            goods.setWeekHitsDate(new Date());
            goods.setMonthSalesDate(new Date());
            goodsMapper.insertGoods(goods);

            //生成分组关系
            String strGroupIds = goodsMap.get("groupIds");
            if(!StringUtils.isEmpty(strGroupIds)){
                String[] groupIds = strGroupIds.split(",");
                Long[] ids = StringUtils.convertToLong(groupIds);
                for (Long id : ids){
                    ShopGroup shopGroup = shopGroupMapper.selectShopGroupById(id);
                    if (shopGroup != null){
                        ShopGroupGoods shopGroupGoods = new ShopGroupGoods();
                        shopGroupGoods.setGoodsId(goods.getId());
                        shopGroupGoods.setGroupId(shopGroup.getId());
                        shopGroupGoodsMapper.insertShopGroupGoods(shopGroupGoods);
                    }
                }
            }

        }catch (Exception e)
        {
            return -1;
        }
        return 0;
    }

    /**
     * 修改优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    @Override
    public int updateGoods(Goods goods)
    {
        return goodsMapper.updateGoods(goods);
    }

    /**
     * 删除优惠券列对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsByIds(String ids)
    {
        return goodsMapper.deleteGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除优惠券列信息
     * 
     * @param id 优惠券列ID
     * @return 结果
     */
    public int deleteGoodsById(Long id)
    {
        return goodsMapper.deleteGoodsById(id);
    }
}
