package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopNoticeMapper;
import com.ruoyi.system.domain.ShopNotice;
import com.ruoyi.system.service.IShopNoticeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商城通告Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
@Service
public class ShopNoticeServiceImpl implements IShopNoticeService 
{
    @Autowired
    private ShopNoticeMapper shopNoticeMapper;

    /**
     * 查询商城通告
     * 
     * @param id 商城通告ID
     * @return 商城通告
     */
    @Override
    public ShopNotice selectShopNoticeById(Long id)
    {
        return shopNoticeMapper.selectShopNoticeById(id);
    }

    /**
     * 查询商城通告列表
     * 
     * @param shopNotice 商城通告
     * @return 商城通告
     */
    @Override
    public List<ShopNotice> selectShopNoticeList(ShopNotice shopNotice)
    {
        return shopNoticeMapper.selectShopNoticeList(shopNotice);
    }

    /**
     * 新增商城通告
     * 
     * @param shopNotice 商城通告
     * @return 结果
     */
    @Override
    public int insertShopNotice(ShopNotice shopNotice)
    {
        shopNotice.setCreateTime(DateUtils.getNowDate());
        return shopNoticeMapper.insertShopNotice(shopNotice);
    }

    /**
     * 修改商城通告
     * 
     * @param shopNotice 商城通告
     * @return 结果
     */
    @Override
    public int updateShopNotice(ShopNotice shopNotice)
    {
        shopNotice.setUpdateTime(DateUtils.getNowDate());
        return shopNoticeMapper.updateShopNotice(shopNotice);
    }

    /**
     * 删除商城通告对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopNoticeByIds(String ids)
    {
        return shopNoticeMapper.deleteShopNoticeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商城通告信息
     * 
     * @param id 商城通告ID
     * @return 结果
     */
    public int deleteShopNoticeById(Long id)
    {
        return shopNoticeMapper.deleteShopNoticeById(id);
    }
}
