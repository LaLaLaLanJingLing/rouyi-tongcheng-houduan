package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopMemberCommentMapper;
import com.ruoyi.system.domain.ShopMemberComment;
import com.ruoyi.system.service.IShopMemberCommentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-26
 */
@Service
public class ShopMemberCommentServiceImpl implements IShopMemberCommentService 
{
    @Autowired
    private ShopMemberCommentMapper shopMemberCommentMapper;

    /**
     * 查询评论
     * 
     * @param id 评论ID
     * @return 评论
     */
    @Override
    public ShopMemberComment selectShopMemberCommentById(Long id)
    {
        return shopMemberCommentMapper.selectShopMemberCommentById(id);
    }

    /**
     * 查询评论列表
     * 
     * @param shopMemberComment 评论
     * @return 评论
     */
    @Override
    public List<ShopMemberComment> selectShopMemberCommentList(ShopMemberComment shopMemberComment)
    {
        return shopMemberCommentMapper.selectShopMemberCommentList(shopMemberComment);
    }

    /**
     * 新增评论
     * 
     * @param shopMemberComment 评论
     * @return 结果
     */
    @Override
    public int insertShopMemberComment(ShopMemberComment shopMemberComment)
    {
        shopMemberComment.setCreateTime(DateUtils.getNowDate());
        return shopMemberCommentMapper.insertShopMemberComment(shopMemberComment);
    }

    /**
     * 修改评论
     * 
     * @param shopMemberComment 评论
     * @return 结果
     */
    @Override
    public int updateShopMemberComment(ShopMemberComment shopMemberComment)
    {
        shopMemberComment.setUpdateTime(DateUtils.getNowDate());
        return shopMemberCommentMapper.updateShopMemberComment(shopMemberComment);
    }

    /**
     * 删除评论对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopMemberCommentByIds(String ids)
    {
        return shopMemberCommentMapper.deleteShopMemberCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除评论信息
     * 
     * @param id 评论ID
     * @return 结果
     */
    public int deleteShopMemberCommentById(Long id)
    {
        return shopMemberCommentMapper.deleteShopMemberCommentById(id);
    }
}
