package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopMapper;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.service.IBusinessShopService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-25
 */
@Service
public class BusinessShopServiceImpl implements IBusinessShopService 
{
    @Autowired
    private BusinessShopMapper businessShopMapper;

    /**
     * 查询商户
     * 
     * @param id 商户ID
     * @return 商户
     */
    @Override
    public BusinessShop selectBusinessShopById(Long id)
    {
        return businessShopMapper.selectBusinessShopById(id);
    }

    /**
     * 查询商户列表
     * 
     * @param businessShop 商户
     * @return 商户
     */
    @Override
    public List<BusinessShop> selectBusinessShopList(BusinessShop businessShop)
    {
        return businessShopMapper.selectBusinessShopList(businessShop);
    }

    /**
     * 新增商户
     * 
     * @param businessShop 商户
     * @return 结果
     */
    @Override
    public int insertBusinessShop(BusinessShop businessShop)
    {
        return businessShopMapper.insertBusinessShop(businessShop);
    }

    /**
     * 修改商户
     * 
     * @param businessShop 商户
     * @return 结果
     */
    @Override
    public int updateBusinessShop(BusinessShop businessShop)
    {
        return businessShopMapper.updateBusinessShop(businessShop);
    }

    /**
     * 删除商户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopByIds(String ids)
    {
        return businessShopMapper.deleteBusinessShopByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户信息
     * 
     * @param id 商户ID
     * @return 结果
     */
    public int deleteBusinessShopById(Long id)
    {
        return businessShopMapper.deleteBusinessShopById(id);
    }
}
