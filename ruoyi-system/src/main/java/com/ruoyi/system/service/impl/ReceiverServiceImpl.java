package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ReceiverMapper;
import com.ruoyi.system.domain.Receiver;
import com.ruoyi.system.service.IReceiverService;
import com.ruoyi.common.core.text.Convert;

/**
 * 收货信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Service
public class ReceiverServiceImpl implements IReceiverService 
{
    @Autowired
    private ReceiverMapper receiverMapper;

    /**
     * 查询收货信息
     * 
     * @param id 收货信息ID
     * @return 收货信息
     */
    @Override
    public Receiver selectReceiverById(Long id)
    {
        return receiverMapper.selectReceiverById(id);
    }

    /**
     * 查询收货信息列表
     * 
     * @param receiver 收货信息
     * @return 收货信息
     */
    @Override
    public List<Receiver> selectReceiverList(Receiver receiver)
    {
        return receiverMapper.selectReceiverList(receiver);
    }

    /**
     * 新增收货信息
     * 
     * @param receiver 收货信息
     * @return 结果
     */
    @Override
    public int insertReceiver(Receiver receiver)
    {
        return receiverMapper.insertReceiver(receiver);
    }

    /**
     * 修改收货信息
     * 
     * @param receiver 收货信息
     * @return 结果
     */
    @Override
    public int updateReceiver(Receiver receiver)
    {
        return receiverMapper.updateReceiver(receiver);
    }

    /**
     * 删除收货信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteReceiverByIds(String ids)
    {
        return receiverMapper.deleteReceiverByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除收货信息信息
     * 
     * @param id 收货信息ID
     * @return 结果
     */
    public int deleteReceiverById(Long id)
    {
        return receiverMapper.deleteReceiverById(id);
    }
}
