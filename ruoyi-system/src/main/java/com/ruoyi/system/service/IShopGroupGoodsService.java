package com.ruoyi.system.service;

import com.ruoyi.system.domain.ShopGroupGoods;
import java.util.List;

/**
 * 分组商品关联Service接口
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
public interface IShopGroupGoodsService 
{
    /**
     * 查询分组商品关联
     * 
     * @param groupId 分组商品关联ID
     * @return 分组商品关联
     */
    public ShopGroupGoods selectShopGroupGoodsById(Long groupId);

    /**
     * 查询分组商品关联列表
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 分组商品关联集合
     */
    public List<ShopGroupGoods> selectShopGroupGoodsList(ShopGroupGoods shopGroupGoods);

    /**
     * 新增分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    public int insertShopGroupGoods(ShopGroupGoods shopGroupGoods);

    /**
     * 修改分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    public int updateShopGroupGoods(ShopGroupGoods shopGroupGoods);

    /**
     * 批量删除分组商品关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopGroupGoodsByIds(String ids);

    /**
     * 删除分组商品关联信息
     * 
     * @param groupId 分组商品关联ID
     * @return 结果
     */
    public int deleteShopGroupGoodsById(Long groupId);

    /**
     * 删除分组商品关联信息
     *
     * @param goodsId 分组商品关联商品id
     * @return 结果
     */
    public int deleteShopGroupGoodsByGoodsId(Long goodsId);
}
