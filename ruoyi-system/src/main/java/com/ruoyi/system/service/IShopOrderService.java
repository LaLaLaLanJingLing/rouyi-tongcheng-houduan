package com.ruoyi.system.service;

import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.domain.Member;
import com.ruoyi.system.domain.ShopOrder;
import java.util.List;

/**
 * 订单Service接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface IShopOrderService 
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public ShopOrder selectShopOrderById(Long id);

    /**
     * 查询订单
     *
     * @param sn 订单编号
     * @return 订单
     */
    public ShopOrder selectShopOrderBySn(String sn);

    /**
     * 查询订单列表
     * 
     * @param shopOrder 订单
     * @return 订单集合
     */
    public List<ShopOrder> selectShopOrderList(ShopOrder shopOrder);

    /**
     * 新增订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    public int insertShopOrder(ShopOrder shopOrder);

    /**
     * 修改订单
     * 
     * @param shopOrder 订单
     * @return 结果
     */
    public int updateShopOrder(ShopOrder shopOrder);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopOrderByIds(String ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteShopOrderById(Long id);


    /**
     * 创建订单 （优惠券的订单）
     *
     * @param id 订单ID
     * @return 结果
     */
    public ShopOrder createOrder(Long Type , Goods goods, BusinessShop businessShop, String phone, String consignee, Member member);


    /**
     *  获取已经成交的订单总额
     *
     * @param  shopId 商户订单
     * @param  month 月份 0代表全部
     * @return 总金额
     */
    public double selectShopSaleTotalAmountByMonth(Long shopId, Long month);


    /**
     *  获取已经成交的订单数量
     *
     * @param  shopId 商户订单
     * @param  month 月份 0代表全部
     * @return 总金额
     */
    public int selectShopSaleTotalNumberByMonth(Long shopId, Long month);

}
