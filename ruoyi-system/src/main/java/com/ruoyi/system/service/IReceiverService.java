package com.ruoyi.system.service;

import com.ruoyi.system.domain.Receiver;
import java.util.List;

/**
 * 收货信息Service接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface IReceiverService 
{
    /**
     * 查询收货信息
     * 
     * @param id 收货信息ID
     * @return 收货信息
     */
    public Receiver selectReceiverById(Long id);

    /**
     * 查询收货信息列表
     * 
     * @param receiver 收货信息
     * @return 收货信息集合
     */
    public List<Receiver> selectReceiverList(Receiver receiver);

    /**
     * 新增收货信息
     * 
     * @param receiver 收货信息
     * @return 结果
     */
    public int insertReceiver(Receiver receiver);

    /**
     * 修改收货信息
     * 
     * @param receiver 收货信息
     * @return 结果
     */
    public int updateReceiver(Receiver receiver);

    /**
     * 批量删除收货信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteReceiverByIds(String ids);

    /**
     * 删除收货信息信息
     * 
     * @param id 收货信息ID
     * @return 结果
     */
    public int deleteReceiverById(Long id);
}
