package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopConfigMapper;
import com.ruoyi.system.domain.BusinessShopConfig;
import com.ruoyi.system.service.IBusinessShopConfigService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户分成设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
@Service
public class BusinessShopConfigServiceImpl implements IBusinessShopConfigService 
{
    @Autowired
    private BusinessShopConfigMapper businessShopConfigMapper;

    /**
     * 查询商户分成设置
     * 
     * @param id 商户分成设置ID
     * @return 商户分成设置
     */
    @Override
    public BusinessShopConfig selectBusinessShopConfigById(Long id)
    {
        return businessShopConfigMapper.selectBusinessShopConfigById(id);
    }

    /**
     * 查询商户分成设置列表
     * 
     * @param businessShopConfig 商户分成设置
     * @return 商户分成设置
     */
    @Override
    public List<BusinessShopConfig> selectBusinessShopConfigList(BusinessShopConfig businessShopConfig)
    {
        return businessShopConfigMapper.selectBusinessShopConfigList(businessShopConfig);
    }

    /**
     * 新增商户分成设置
     * 
     * @param businessShopConfig 商户分成设置
     * @return 结果
     */
    @Override
    public int insertBusinessShopConfig(BusinessShopConfig businessShopConfig)
    {
        return businessShopConfigMapper.insertBusinessShopConfig(businessShopConfig);
    }

    /**
     * 修改商户分成设置
     * 
     * @param businessShopConfig 商户分成设置
     * @return 结果
     */
    @Override
    public int updateBusinessShopConfig(BusinessShopConfig businessShopConfig)
    {
        return businessShopConfigMapper.updateBusinessShopConfig(businessShopConfig);
    }

    /**
     * 删除商户分成设置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopConfigByIds(String ids)
    {
        return businessShopConfigMapper.deleteBusinessShopConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户分成设置信息
     * 
     * @param id 商户分成设置ID
     * @return 结果
     */
    public int deleteBusinessShopConfigById(Long id)
    {
        return businessShopConfigMapper.deleteBusinessShopConfigById(id);
    }


    @Override
    public BusinessShopConfig selectBusinessShopConfigByShopId(Long shopId)  {
        BusinessShopConfig selectBusinessShopConfig = new BusinessShopConfig();
        selectBusinessShopConfig.setShopId(shopId);
        List<BusinessShopConfig> businessShopConfigList = selectBusinessShopConfigList(selectBusinessShopConfig);
        if (businessShopConfigList.size() == 0 ){
            return null;
        }
        if (businessShopConfigList.size() > 1){
            //logger.error("系统出现问题,发现重复 shopInfo shopId=" + shopId.toString());
            return null;
        }
        BusinessShopConfig businessShopConfig = businessShopConfigList.get(0);

        return businessShopConfig;
    }
}
