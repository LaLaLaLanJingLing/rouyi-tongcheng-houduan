package com.ruoyi.system.service;

import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.domain.vo.ProductImage;

import java.util.List;
import java.util.Map;

/**
 * 优惠券列Service接口
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface IGoodsService 
{
    /**
     * 查询优惠券列
     * 
     * @param id 优惠券列ID
     * @return 优惠券列
     */
    public Goods selectGoodsById(Long id);


    /**
     * 转换成图片列表
     *
     * @param Goods goods对象
     * @return 返回产品列表
     */
    public List<ProductImage> getProductImageList(Goods goods);

    /**
     * 查询优惠券列列表
     * 
     * @param goods 优惠券列
     * @return 优惠券列集合
     */
    public List<Goods> selectGoodsList(Goods goods);


    /**
     * 查询优惠券列列表
     *
     * @param Long 分组id
     * @return 优惠券列集合
     */
    public List<Goods> selectGoodsListByGroupId(Long groupId);


    /**
     * 新增优惠券列
     *
     * @param goodsMap 优惠券列
     * @return 优惠券列集合
     */
    public int insertGoods(Map<String, String> goodsMap);

    /**
     * 新增优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    public int insertGoods(Goods goods);

    /**
     * 修改优惠券列
     * 
     * @param goods 优惠券列
     * @return 结果
     */
    public int updateGoods(Goods goods);

    /**
     * 批量删除优惠券列
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsByIds(String ids);

    /**
     * 删除优惠券列信息
     * 
     * @param id 优惠券列ID
     * @return 结果
     */
    public int deleteGoodsById(Long id);
}
