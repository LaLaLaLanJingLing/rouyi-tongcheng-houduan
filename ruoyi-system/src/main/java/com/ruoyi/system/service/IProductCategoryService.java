package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.ProductCategory;
import com.ruoyi.system.domain.SysDept;

import java.util.List;

/**
 * 商品分类Service接口
 * 
 * @author ruoyi
 * @date 2019-10-24
 */
public interface IProductCategoryService 
{
    /**
     * 查询商品分类
     * 
     * @param id 商品分类ID
     * @return 商品分类
     */
    public ProductCategory selectProductCategoryById(Long id);

    /**
     * 查询商品分类列表
     * 
     * @param productCategory 商品分类
     * @return 商品分类集合
     */
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory);

    /**
     * 新增商品分类
     * 
     * @param productCategory 商品分类
     * @return 结果
     */
    public int insertProductCategory(ProductCategory productCategory);



    /**
     * 修改商品分类
     * 
     * @param productCategory 商品分类
     * @return 结果
     */
    public int updateProductCategory(ProductCategory productCategory);

    /**
     * 批量删除商品分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProductCategoryByIds(String ids);

    /**
     * 删除商品分类信息
     * 
     * @param id 商品分类ID
     * @return 结果
     */
    public int deleteProductCategoryById(Long id);


    /**
     * 商品分类树
     *
     * @param ProductCategory 商品分类
     * @return 结果
     */
    public List<Ztree> selectProductCategoryTree(ProductCategory productCategory);

}
