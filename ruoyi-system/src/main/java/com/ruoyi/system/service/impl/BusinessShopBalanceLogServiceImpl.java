package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BusinessShopBalanceLogMapper;
import com.ruoyi.system.domain.BusinessShopBalanceLog;
import com.ruoyi.system.service.IBusinessShopBalanceLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商户余额记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Service
public class BusinessShopBalanceLogServiceImpl implements IBusinessShopBalanceLogService 
{
    @Autowired
    private BusinessShopBalanceLogMapper businessShopBalanceLogMapper;

    /**
     * 查询商户余额记录
     * 
     * @param id 商户余额记录ID
     * @return 商户余额记录
     */
    @Override
    public BusinessShopBalanceLog selectBusinessShopBalanceLogById(Long id)
    {
        return businessShopBalanceLogMapper.selectBusinessShopBalanceLogById(id);
    }

    /**
     * 查询商户余额记录列表
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 商户余额记录
     */
    @Override
    public List<BusinessShopBalanceLog> selectBusinessShopBalanceLogList(BusinessShopBalanceLog businessShopBalanceLog)
    {
        return businessShopBalanceLogMapper.selectBusinessShopBalanceLogList(businessShopBalanceLog);
    }

    /**
     * 新增商户余额记录
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 结果
     */
    @Override
    public int insertBusinessShopBalanceLog(BusinessShopBalanceLog businessShopBalanceLog)
    {
        return businessShopBalanceLogMapper.insertBusinessShopBalanceLog(businessShopBalanceLog);
    }

    /**
     * 修改商户余额记录
     * 
     * @param businessShopBalanceLog 商户余额记录
     * @return 结果
     */
    @Override
    public int updateBusinessShopBalanceLog(BusinessShopBalanceLog businessShopBalanceLog)
    {
        return businessShopBalanceLogMapper.updateBusinessShopBalanceLog(businessShopBalanceLog);
    }

    /**
     * 删除商户余额记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBusinessShopBalanceLogByIds(String ids)
    {
        return businessShopBalanceLogMapper.deleteBusinessShopBalanceLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户余额记录信息
     * 
     * @param id 商户余额记录ID
     * @return 结果
     */
    public int deleteBusinessShopBalanceLogById(Long id)
    {
        return businessShopBalanceLogMapper.deleteBusinessShopBalanceLogById(id);
    }
}
