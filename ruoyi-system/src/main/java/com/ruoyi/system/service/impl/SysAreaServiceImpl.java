package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAreaMapper;
import com.ruoyi.system.domain.SysArea;
import com.ruoyi.system.service.ISysAreaService;
import com.ruoyi.common.core.text.Convert;

/**
 * 区域Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Service
public class SysAreaServiceImpl implements ISysAreaService 
{
    @Autowired
    private SysAreaMapper sysAreaMapper;

    /**
     * 查询区域
     * 
     * @param id 区域ID
     * @return 区域
     */
    @Override
    public SysArea selectSysAreaById(Long id)
    {
        return sysAreaMapper.selectSysAreaById(id);
    }

    /**
     * 查询区域列表
     * 
     * @param sysArea 区域
     * @return 区域
     */
    @Override
    public List<SysArea> selectSysAreaList(SysArea sysArea)
    {
        return sysAreaMapper.selectSysAreaList(sysArea);
    }

    /**
     * 新增区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    @Override
    public int insertSysArea(SysArea sysArea)
    {
        return sysAreaMapper.insertSysArea(sysArea);
    }

    /**
     * 修改区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    @Override
    public int updateSysArea(SysArea sysArea)
    {
        return sysAreaMapper.updateSysArea(sysArea);
    }

    /**
     * 删除区域对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysAreaByIds(String ids)
    {
        return sysAreaMapper.deleteSysAreaByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除区域信息
     * 
     * @param id 区域ID
     * @return 结果
     */
    public int deleteSysAreaById(Long id)
    {
        return sysAreaMapper.deleteSysAreaById(id);
    }

    /**
     * 查询区域树列表
     * 
     * @return 所有区域信息
     */
    @Override
    public List<Ztree> selectSysAreaTree()
    {
        List<SysArea> sysAreaList = sysAreaMapper.selectSysAreaList(new SysArea());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (SysArea sysArea : sysAreaList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(sysArea.getId());
            ztree.setpId(sysArea.getParentId());
            ztree.setName(sysArea.getName());
            ztree.setTitle(sysArea.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
