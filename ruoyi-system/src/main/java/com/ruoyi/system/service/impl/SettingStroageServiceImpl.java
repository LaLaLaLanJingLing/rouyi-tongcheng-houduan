package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SettingStroageMapper;
import com.ruoyi.system.domain.SettingStroage;
import com.ruoyi.system.service.ISettingStroageService;
import com.ruoyi.common.core.text.Convert;

/**
 * 存储插件配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-04
 */
@Service
public class SettingStroageServiceImpl implements ISettingStroageService 
{
    @Autowired
    private SettingStroageMapper settingStroageMapper;

    /**
     * 查询存储插件配置
     * 
     * @param id 存储插件配置ID
     * @return 存储插件配置
     */
    @Override
    public SettingStroage selectSettingStroageById(Long id)
    {
        return settingStroageMapper.selectSettingStroageById(id);
    }

    /**
     * 查询存储插件配置列表
     * 
     * @param settingStroage 存储插件配置
     * @return 存储插件配置
     */
    @Override
    public List<SettingStroage> selectSettingStroageList(SettingStroage settingStroage)
    {
        return settingStroageMapper.selectSettingStroageList(settingStroage);
    }

    /**
     * 新增存储插件配置
     * 
     * @param settingStroage 存储插件配置
     * @return 结果
     */
    @Override
    public int insertSettingStroage(SettingStroage settingStroage)
    {
        return settingStroageMapper.insertSettingStroage(settingStroage);
    }

    /**
     * 修改存储插件配置
     * 
     * @param settingStroage 存储插件配置
     * @return 结果
     */
    @Override
    public int updateSettingStroage(SettingStroage settingStroage)
    {
        return settingStroageMapper.updateSettingStroage(settingStroage);
    }

    /**
     * 删除存储插件配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSettingStroageByIds(String ids)
    {
        return settingStroageMapper.deleteSettingStroageByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除存储插件配置信息
     * 
     * @param id 存储插件配置ID
     * @return 结果
     */
    public int deleteSettingStroageById(Long id)
    {
        return settingStroageMapper.deleteSettingStroageById(id);
    }
}
