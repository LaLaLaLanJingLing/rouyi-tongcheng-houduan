package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopAdMapper;
import com.ruoyi.system.domain.ShopAd;
import com.ruoyi.system.service.IShopAdService;
import com.ruoyi.common.core.text.Convert;

/**
 * 轮播图Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
@Service
public class ShopAdServiceImpl implements IShopAdService 
{
    @Autowired
    private ShopAdMapper shopAdMapper;

    /**
     * 查询轮播图
     * 
     * @param id 轮播图ID
     * @return 轮播图
     */
    @Override
    public ShopAd selectShopAdById(Long id)
    {
        return shopAdMapper.selectShopAdById(id);
    }

    /**
     * 查询轮播图列表
     * 
     * @param shopAd 轮播图
     * @return 轮播图
     */
    @Override
    public List<ShopAd> selectShopAdList(ShopAd shopAd)
    {
        return shopAdMapper.selectShopAdList(shopAd);
    }

    /**
     * 新增轮播图
     * 
     * @param shopAd 轮播图
     * @return 结果
     */
    @Override
    public int insertShopAd(ShopAd shopAd)
    {
        return shopAdMapper.insertShopAd(shopAd);
    }

    /**
     * 修改轮播图
     * 
     * @param shopAd 轮播图
     * @return 结果
     */
    @Override
    public int updateShopAd(ShopAd shopAd)
    {
        return shopAdMapper.updateShopAd(shopAd);
    }

    /**
     * 删除轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopAdByIds(String ids)
    {
        return shopAdMapper.deleteShopAdByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图ID
     * @return 结果
     */
    public int deleteShopAdById(Long id)
    {
        return shopAdMapper.deleteShopAdById(id);
    }
}
