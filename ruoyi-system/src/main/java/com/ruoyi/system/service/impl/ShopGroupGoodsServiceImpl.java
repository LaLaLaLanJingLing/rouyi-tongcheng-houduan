package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShopGroupGoodsMapper;
import com.ruoyi.system.domain.ShopGroupGoods;
import com.ruoyi.system.service.IShopGroupGoodsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分组商品关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
@Service
public class ShopGroupGoodsServiceImpl implements IShopGroupGoodsService 
{
    @Autowired
    private ShopGroupGoodsMapper shopGroupGoodsMapper;

    /**
     * 查询分组商品关联
     * 
     * @param groupId 分组商品关联ID
     * @return 分组商品关联
     */
    @Override
    public ShopGroupGoods selectShopGroupGoodsById(Long groupId)
    {
        return shopGroupGoodsMapper.selectShopGroupGoodsById(groupId);
    }

    /**
     * 查询分组商品关联列表
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 分组商品关联
     */
    @Override
    public List<ShopGroupGoods> selectShopGroupGoodsList(ShopGroupGoods shopGroupGoods)
    {
        return shopGroupGoodsMapper.selectShopGroupGoodsList(shopGroupGoods);
    }

    /**
     * 新增分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    @Override
    public int insertShopGroupGoods(ShopGroupGoods shopGroupGoods)
    {
        return shopGroupGoodsMapper.insertShopGroupGoods(shopGroupGoods);
    }

    /**
     * 修改分组商品关联
     * 
     * @param shopGroupGoods 分组商品关联
     * @return 结果
     */
    @Override
    public int updateShopGroupGoods(ShopGroupGoods shopGroupGoods)
    {
        return shopGroupGoodsMapper.updateShopGroupGoods(shopGroupGoods);
    }

    /**
     * 删除分组商品关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopGroupGoodsByIds(String ids)
    {
        return shopGroupGoodsMapper.deleteShopGroupGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分组商品关联信息
     * 
     * @param groupId 分组商品关联ID
     * @return 结果
     */
    public int deleteShopGroupGoodsById(Long groupId)
    {
        return shopGroupGoodsMapper.deleteShopGroupGoodsById(groupId);
    }

    public int deleteShopGroupGoodsByGoodsId(Long goodsId){
        return shopGroupGoodsMapper.deleteShopGroupGoodsByGoodsId(goodsId);
    }
}
