package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysTokenMapper;
import com.ruoyi.system.domain.SysToken;
import com.ruoyi.system.service.ISysTokenService;
import com.ruoyi.common.core.text.Convert;

/**
 * 系统tokenService业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-27
 */
@Service
public class SysTokenServiceImpl implements ISysTokenService 
{
    @Autowired
    private SysTokenMapper sysTokenMapper;

    /**
     * 查询系统token
     * 
     * @param id 系统tokenID
     * @return 系统token
     */
    @Override
    public SysToken selectSysTokenById(Long id)
    {
        return sysTokenMapper.selectSysTokenById(id);
    }

    @Override
    public SysToken selectSysTokenByMemberId(Long memberId) {
        return sysTokenMapper.selectSysTokenByMemberId(memberId);
    }

    /**
     * 查询系统token列表
     * 
     * @param sysToken 系统token
     * @return 系统token
     */
    @Override
    public List<SysToken> selectSysTokenList(SysToken sysToken)
    {
        return sysTokenMapper.selectSysTokenList(sysToken);
    }

    /**
     * 新增系统token
     * 
     * @param sysToken 系统token
     * @return 结果
     */
    @Override
    public int insertSysToken(SysToken sysToken)
    {
        return sysTokenMapper.insertSysToken(sysToken);
    }

    /**
     * 修改系统token
     * 
     * @param sysToken 系统token
     * @return 结果
     */
    @Override
    public int updateSysToken(SysToken sysToken)
    {
        return sysTokenMapper.updateSysToken(sysToken);
    }

    /**
     * 删除系统token对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysTokenByIds(String ids)
    {
        return sysTokenMapper.deleteSysTokenByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除系统token信息
     * 
     * @param id 系统tokenID
     * @return 结果
     */
    public int deleteSysTokenById(Long id)
    {
        return sysTokenMapper.deleteSysTokenById(id);
    }


    /**
     *  根据token 查找sysToken
     *
     * @param strToken token字符串
     * @return 结果
     */
    public SysToken selectSysTokenByToken(String strToken)
    {
        return sysTokenMapper.selectSysTokenByToken(strToken);
    }


    @Override
    public SysToken selectSysTokenByShopUserId(Long shopUserId) {
        return sysTokenMapper.selectSysTokenByShopUserId(shopUserId);
    }
}
