package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysDept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductCategoryMapper;
import com.ruoyi.system.domain.ProductCategory;
import com.ruoyi.system.service.IProductCategoryService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商品分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-10-24
 */
@Service
public class ProductCategoryServiceImpl implements IProductCategoryService 
{
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    /**
     * 查询商品分类
     * 
     * @param id 商品分类ID
     * @return 商品分类
     */
    @Override
    public ProductCategory selectProductCategoryById(Long id)
    {
        return productCategoryMapper.selectProductCategoryById(id);
    }

    /**
     * 查询商品分类列表
     * 
     * @param productCategory 商品分类
     * @return 商品分类
     */
    @Override
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory)
    {
        return productCategoryMapper.selectProductCategoryList(productCategory);
    }

    /**
     * 新增商品分类
     * 
     * @param productCategory 商品分类
     * @return 结果
     */
    @Override
    public int insertProductCategory(ProductCategory productCategory)
    {
        SetProductCategoryGrade(productCategory);
        return productCategoryMapper.insertProductCategory(productCategory);
    }

    /**
     * 修改商品分类
     * 
     * @param productCategory 商品分类
     * @return 结果
     */
    @Override
    public int updateProductCategory(ProductCategory productCategory)
    {
        SetProductCategoryGrade(productCategory);
        return productCategoryMapper.updateProductCategory(productCategory);
    }

    private void SetProductCategoryGrade(ProductCategory productCategory) {
        if (productCategory.getParentId() == 0L ){
            //一级分类
            productCategory.setGrade(1L);
        }else{
            ProductCategory parent = productCategoryMapper.selectProductCategoryById(productCategory.getId());
            if (parent == null){
                productCategory.setGrade(1L);
            }else{
                productCategory.setGrade(parent.getGrade() + 1);
            }
        }
    }

    /**
     * 删除商品分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductCategoryByIds(String ids)
    {
        return productCategoryMapper.deleteProductCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品分类信息
     * 
     * @param id 商品分类ID
     * @return 结果
     */
    public int deleteProductCategoryById(Long id)
    {
        return productCategoryMapper.deleteProductCategoryById(id);
    }

    /**
     * 查询商品列表树
     *
     * @param ProductCategory 商品分类
     * @return 结果
     */
    @Override
    public List<Ztree> selectProductCategoryTree(ProductCategory productCategory) {
        List<ProductCategory> deptList = productCategoryMapper.selectProductCategoryList(productCategory);
        List<Ztree> ztrees = initZtree(deptList);
        return ztrees;
    }

    /**
     * 对象转商品分类树
     *
     * @param productCategoryList 商品分类列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<ProductCategory> productCategoryList)
    {
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (ProductCategory productCategory : productCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(productCategory.getId());
            ztree.setpId(productCategory.getParentId());
            ztree.setName(productCategory.getName());
            ztree.setTitle(productCategory.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
