package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysArea;
import java.util.List;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 区域Service接口
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
public interface ISysAreaService 
{
    /**
     * 查询区域
     * 
     * @param id 区域ID
     * @return 区域
     */
    public SysArea selectSysAreaById(Long id);

    /**
     * 查询区域列表
     * 
     * @param sysArea 区域
     * @return 区域集合
     */
    public List<SysArea> selectSysAreaList(SysArea sysArea);

    /**
     * 新增区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    public int insertSysArea(SysArea sysArea);

    /**
     * 修改区域
     * 
     * @param sysArea 区域
     * @return 结果
     */
    public int updateSysArea(SysArea sysArea);

    /**
     * 批量删除区域
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysAreaByIds(String ids);

    /**
     * 删除区域信息
     * 
     * @param id 区域ID
     * @return 结果
     */
    public int deleteSysAreaById(Long id);

    /**
     * 查询区域树列表
     * 
     * @return 所有区域信息
     */
    public List<Ztree> selectSysAreaTree();
}
