package com.ruoyi.system.service;

import com.ruoyi.system.domain.ShopArea;
import java.util.List;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 商家地区Service接口
 * 
 * @author ruoyi
 * @date 2019-11-06
 */
public interface IShopAreaService 
{
    /**
     * 查询商家地区
     * 
     * @param id 商家地区ID
     * @return 商家地区
     */
    public ShopArea selectShopAreaById(Long id);

    /**
     * 查询商家地区列表
     * 
     * @param shopArea 商家地区
     * @return 商家地区集合
     */
    public List<ShopArea> selectShopAreaList(ShopArea shopArea);

    /**
     * 新增商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    public int insertShopArea(ShopArea shopArea);

    /**
     * 修改商家地区
     * 
     * @param shopArea 商家地区
     * @return 结果
     */
    public int updateShopArea(ShopArea shopArea);

    /**
     * 批量删除商家地区
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopAreaByIds(String ids);

    /**
     * 删除商家地区信息
     * 
     * @param id 商家地区ID
     * @return 结果
     */
    public int deleteShopAreaById(Long id);

    /**
     * 查询商家地区树列表
     * 
     * @return 所有商家地区信息
     */
    public List<Ztree> selectShopAreaTree();
}
