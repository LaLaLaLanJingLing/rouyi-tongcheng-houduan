package com.ruoyi.common.utils;


public class TokenUtils {

    /**
     * 生成token号码
     *
     * @return token号码
     */
    public static String generateToken() {
        return RandomUtils.randomCustomUUID().concat(RandomUtils.randomString(6));
    }
}
