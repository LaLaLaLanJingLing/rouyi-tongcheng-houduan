package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysToken;
import com.ruoyi.system.service.ISysTokenService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统tokenController
 * 
 * @author ruoyi
 * @date 2019-08-27
 */
@Controller
@RequestMapping("/system/token")
public class SysTokenController extends BaseController
{
    private String prefix = "system/token";

    @Autowired
    private ISysTokenService sysTokenService;

    @RequiresPermissions("system:token:view")
    @GetMapping()
    public String token()
    {
        return prefix + "/token";
    }

    /**
     * 查询系统token列表
     */
    @RequiresPermissions("system:token:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysToken sysToken)
    {
        startPage();
        List<SysToken> list = sysTokenService.selectSysTokenList(sysToken);
        return getDataTable(list);
    }

    /**
     * 导出系统token列表
     */
    @RequiresPermissions("system:token:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysToken sysToken)
    {
        List<SysToken> list = sysTokenService.selectSysTokenList(sysToken);
        ExcelUtil<SysToken> util = new ExcelUtil<SysToken>(SysToken.class);
        return util.exportExcel(list, "token");
    }

    /**
     * 新增系统token
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存系统token
     */
    @RequiresPermissions("system:token:add")
    @Log(title = "系统token", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysToken sysToken)
    {
        return toAjax(sysTokenService.insertSysToken(sysToken));
    }

    /**
     * 修改系统token
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysToken sysToken = sysTokenService.selectSysTokenById(id);
        mmap.put("sysToken", sysToken);
        return prefix + "/edit";
    }

    /**
     * 修改保存系统token
     */
    @RequiresPermissions("system:token:edit")
    @Log(title = "系统token", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysToken sysToken)
    {
        return toAjax(sysTokenService.updateSysToken(sysToken));
    }

    /**
     * 删除系统token
     */
    @RequiresPermissions("system:token:remove")
    @Log(title = "系统token", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysTokenService.deleteSysTokenByIds(ids));
    }
}
