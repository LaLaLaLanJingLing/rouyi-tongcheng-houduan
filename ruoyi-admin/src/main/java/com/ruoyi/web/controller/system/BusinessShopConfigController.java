package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.service.IBusinessShopService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShopConfig;
import com.ruoyi.system.service.IBusinessShopConfigService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户分成设置Controller
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
@Controller
@RequestMapping("/system/business_config")
public class BusinessShopConfigController extends BaseController
{
    private String prefix = "system/business_config";

    @Autowired
    private IBusinessShopConfigService businessShopConfigService;

    @Autowired
    private IBusinessShopService businessShopService;

    @RequiresPermissions("system:config:view")
    @GetMapping()
    public String config()
    {
        return prefix + "/business_config";
    }

    /**
     * 查询商户分成设置列表
     */
    @RequiresPermissions("system:config:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShopConfig businessShopConfig)
    {
        startPage();
        List<BusinessShopConfig> list = businessShopConfigService.selectBusinessShopConfigList(businessShopConfig);
        return getDataTable(list);
    }

    /**
     * 导出商户分成设置列表
     */
    @RequiresPermissions("system:config:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShopConfig businessShopConfig)
    {
        List<BusinessShopConfig> list = businessShopConfigService.selectBusinessShopConfigList(businessShopConfig);
        ExcelUtil<BusinessShopConfig> util = new ExcelUtil<BusinessShopConfig>(BusinessShopConfig.class);
        return util.exportExcel(list, "config");
    }

    /**
     * 新增商户分成设置
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户分成设置
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "商户分成设置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShopConfig businessShopConfig)
    {
        return toAjax(businessShopConfigService.insertBusinessShopConfig(businessShopConfig));
    }

    /**
     * 修改商户分成设置
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShopConfig businessShopConfig = businessShopConfigService.selectBusinessShopConfigById(id);
        mmap.put("businessShopConfig", businessShopConfig);
        return prefix + "/edit";
    }

    /**
     * 修改商户分成设置
     */
    @GetMapping("/editShop/{id}")
    public String editShop(@PathVariable("id") Long id, ModelMap mmap)
    {

        BusinessShop businessShop = businessShopService.selectBusinessShopById(id);
        if (businessShop == null){
            //商户不存在
            return "/500";
        }
        BusinessShopConfig businessShopConfig = businessShopConfigService.selectBusinessShopConfigByShopId(id);
        if (businessShopConfig == null){
            //创建一个 新的出来
            businessShopConfig = new BusinessShopConfig();
            businessShopConfig.setCreateDate(new Date());
            businessShopConfig.setType(0L);
            businessShopConfig.setModifyDate(new Date());
            businessShopConfig.setShopId(id);
            businessShopConfig.setValue(0.0);
            businessShopConfig.setOperator(ShiroUtils.getLoginName());
            businessShopConfig.setDivideType(0L);
            businessShopConfigService.insertBusinessShopConfig(businessShopConfig);
        }

        mmap.put("shop", businessShop);
        mmap.put("businessShopConfig", businessShopConfig);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户分成设置
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "商户分成设置", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShopConfig businessShopConfig)
    {
        Long id = businessShopConfig.getId();
        BusinessShopConfig actualBusinessShopConfig = businessShopConfigService.selectBusinessShopConfigById(id);
        if (actualBusinessShopConfig == null){
            return AjaxResult.error("配置不存在,请联系管理员");
        }

        actualBusinessShopConfig.setOperator(ShiroUtils.getLoginName());
        actualBusinessShopConfig.setModifyDate(new Date());
        actualBusinessShopConfig.setShopId(businessShopConfig.getId());
        actualBusinessShopConfig.setType(businessShopConfig.getType());
        actualBusinessShopConfig.setDivideType(businessShopConfig.getDivideType());
        actualBusinessShopConfig.setValue(businessShopConfig.getValue());
        actualBusinessShopConfig.setStartDate(businessShopConfig.getStartDate());
        actualBusinessShopConfig.setEndDate(businessShopConfig.getEndDate());
        if (actualBusinessShopConfig.getCreateDate() == null){
            actualBusinessShopConfig.setCreateDate(new Date());
        }

        return toAjax(businessShopConfigService.updateBusinessShopConfig(actualBusinessShopConfig));
    }

    /**
     * 删除商户分成设置
     */
    @RequiresPermissions("system:config:remove")
    @Log(title = "商户分成设置", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopConfigService.deleteBusinessShopConfigByIds(ids));
    }
}
