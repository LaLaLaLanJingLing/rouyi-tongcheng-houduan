package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SettingStroage;
import com.ruoyi.system.service.ISettingStroageService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 存储插件配置Controller
 * 
 * @author ruoyi
 * @date 2019-11-04
 */
@Controller
@RequestMapping("/system/stroage")
public class SettingStroageController extends BaseController
{
    private String prefix = "system/stroage";

    @Autowired
    private ISettingStroageService settingStroageService;

    @RequiresPermissions("system:stroage:view")
    @GetMapping()
    public String stroage()
    {
        return prefix + "/stroage";
    }

    /**
     * 查询存储插件配置列表
     */
    @RequiresPermissions("system:stroage:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SettingStroage settingStroage)
    {
        startPage();
        List<SettingStroage> list = settingStroageService.selectSettingStroageList(settingStroage);
        return getDataTable(list);
    }

    /**
     * 导出存储插件配置列表
     */
    @RequiresPermissions("system:stroage:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SettingStroage settingStroage)
    {
        List<SettingStroage> list = settingStroageService.selectSettingStroageList(settingStroage);
        ExcelUtil<SettingStroage> util = new ExcelUtil<SettingStroage>(SettingStroage.class);
        return util.exportExcel(list, "stroage");
    }

    /**
     * 新增存储插件配置
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存存储插件配置
     */
    @RequiresPermissions("system:stroage:add")
    @Log(title = "存储插件配置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SettingStroage settingStroage)
    {
        return toAjax(settingStroageService.insertSettingStroage(settingStroage));
    }

    /**
     * 修改存储插件配置
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SettingStroage settingStroage = settingStroageService.selectSettingStroageById(id);
        mmap.put("settingStroage", settingStroage);
        return prefix + "/edit";
    }

    /**
     * 修改保存存储插件配置
     */
    @RequiresPermissions("system:stroage:edit")
    @Log(title = "存储插件配置", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SettingStroage settingStroage)
    {
        return toAjax(settingStroageService.updateSettingStroage(settingStroage));
    }

    /**
     * 删除存储插件配置
     */
    @RequiresPermissions("system:stroage:remove")
    @Log(title = "存储插件配置", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(settingStroageService.deleteSettingStroageByIds(ids));
    }
}
