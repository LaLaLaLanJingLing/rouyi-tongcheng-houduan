package com.ruoyi.web.controller.normal;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.BusinessShopBalanceLog;
import com.ruoyi.system.domain.BusinessShopInfo;
import com.ruoyi.system.service.IBusinessShopBalanceLogService;
import com.ruoyi.system.service.IBusinessShopInfoService;
import com.ruoyi.system.service.IBusinessShopService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShopApply;
import com.ruoyi.system.service.IBusinessShopApplyService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户提现申请Controller
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Controller
@RequestMapping("/normal/apply")
public class BusinessShopApplyController extends BaseController
{
    private String prefix = "normal/apply";

    @Autowired
    private IBusinessShopApplyService businessShopApplyService;

    @Autowired
    private IBusinessShopInfoService businessShopInfoService;

    @Autowired
    private IBusinessShopService businessShopService;

    @Autowired
    private IBusinessShopBalanceLogService businessShopBalanceLogService;

    @RequiresPermissions("system:apply:view")
    @GetMapping()
    public String apply()
    {
        return prefix + "/apply";
    }

    /**
     * 查询商户提现申请列表
     */
    @RequiresPermissions("system:apply:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShopApply businessShopApply)
    {
        startPage();
        List<BusinessShopApply> list = businessShopApplyService.selectBusinessShopApplyList(businessShopApply);
        return getDataTable(list);
    }

    /**
     * 导出商户提现申请列表
     */
    @RequiresPermissions("system:apply:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShopApply businessShopApply)
    {
        List<BusinessShopApply> list = businessShopApplyService.selectBusinessShopApplyList(businessShopApply);
        ExcelUtil<BusinessShopApply> util = new ExcelUtil<BusinessShopApply>(BusinessShopApply.class);
        return util.exportExcel(list, "apply");
    }

    /**
     * 新增商户提现申请
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户提现申请
     */
    @RequiresPermissions("system:apply:add")
    @Log(title = "商户提现申请", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShopApply businessShopApply)
    {
        return toAjax(businessShopApplyService.insertBusinessShopApply(businessShopApply));
    }


    /**
     * 新增保存商户提现申请
     */
    @RequiresPermissions("system:apply:edit")
    @Log(title = "商户提现通过", businessType = BusinessType.INSERT)
    @PostMapping("/pass")
    @ResponseBody
    public AjaxResult pass(BusinessShopApply businessShopApply)
    {
        Long id = businessShopApply.getId();
        BusinessShopApply actualBusinessShopApply = businessShopApplyService.selectBusinessShopApplyById(id);
        if (actualBusinessShopApply == null){
            return AjaxResult.error("商户不存在");
        }

        actualBusinessShopApply.setStatus(2L);

        BusinessShopInfo businessShopInfo = businessShopInfoService.selectBusinessShopInfoByShopId(actualBusinessShopApply.getShopId());
        if (businessShopInfo == null){
            businessShopInfo = new BusinessShopInfo();
            businessShopInfo.setShopId(actualBusinessShopApply.getShopId());
            businessShopInfo.setBalance(0.00);
            businessShopInfo.setCreateTime(new Date());
            businessShopInfo.setUpdateTime(new Date());
            businessShopInfo.setPoints(0.00);
            businessShopInfo.setStatus(0L);
            businessShopInfoService.insertBusinessShopInfo(businessShopInfo);
        }

        BigDecimal balance = BigDecimal.valueOf(businessShopInfo.getBalance());
        balance = balance.add(BigDecimal.valueOf(actualBusinessShopApply.getBalance()));
        businessShopInfo.setBalance(balance.doubleValue());

        actualBusinessShopApply.setOperator(ShiroUtils.getLoginName());
        BusinessShopBalanceLog businessShopBalanceLog = new BusinessShopBalanceLog();
        businessShopBalanceLog.setCreateDate(new Date());
        businessShopBalanceLog.setOperator(ShiroUtils.getLoginName());
        businessShopBalanceLog.setShopId(actualBusinessShopApply.getShopId());
        businessShopBalanceLog.setStatus(0L);
        businessShopBalanceLog.setType(0L);
        businessShopBalanceLog.setChangAmount(actualBusinessShopApply.getBalance());
        businessShopBalanceLog.setApplyId(actualBusinessShopApply.getId());

        businessShopBalanceLogService.insertBusinessShopBalanceLog(businessShopBalanceLog);
        businessShopInfoService.updateBusinessShopInfo(businessShopInfo);
        businessShopApplyService.updateBusinessShopApply(actualBusinessShopApply);

        return AjaxResult.success("操作成功");
    }



    /**
     * 修改商户提现申请
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShopApply businessShopApply = businessShopApplyService.selectBusinessShopApplyById(id);
        Long shopId = businessShopApply.getShopId();
        BusinessShopInfo businessShopInfo = businessShopInfoService.selectBusinessShopInfoByShopId(shopId);
        if (businessShopInfo == null){
            businessShopInfo = new BusinessShopInfo();
        }
        BusinessShop businessShop = businessShopService.selectBusinessShopById(shopId);
        if (businessShop == null){
            return "/500";
        }
        mmap.put("shop", businessShop);
        mmap.put("shopInfo", businessShopInfo);
        mmap.put("shopApply", businessShopApply);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户提现申请
     */
    @RequiresPermissions("system:apply:edit")
    @Log(title = "商户提现申请", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShopApply businessShopApply)
    {
        return toAjax(businessShopApplyService.updateBusinessShopApply(businessShopApply));
    }

    /**
     * 删除商户提现申请
     */
    @RequiresPermissions("system:apply:remove")
    @Log(title = "商户提现申请", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopApplyService.deleteBusinessShopApplyByIds(ids));
    }
}
