package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.domain.ShopMemberComment;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.IBusinessShopService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopUser;
import com.ruoyi.system.service.IShopUserService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户用户信息Controller
 * 
 * @author ruoyi
 * @date 2019-12-03
 */
@Controller
@RequestMapping("/normal/shopUser")
public class ShopUserController extends BaseController
{
    private String prefix = "normal/shopUser";

    @Autowired
    private IShopUserService shopUserService;

    @Autowired
    private IBusinessShopService businessShopService;

    @Autowired
    private SysPasswordService passwordService;

    @RequiresPermissions("normal:shopUser:view")
    @GetMapping()
    public String shopUser()
    {
        return prefix + "/shopUser";
    }

    /**
     * 查询商户用户信息列表
     */
    @RequiresPermissions("normal:shopUser:list")
    @PostMapping("/list/{id}")
    @ResponseBody
    public TableDataInfo list(ShopUser shopUser, @PathVariable("id") Long id)
    {
        startPage();
        shopUser.setShopId(id);
        List<ShopUser> list = shopUserService.selectShopUserList(shopUser);
        return getDataTable(list);
    }

    /**
     * 导出商户用户信息列表
     */
    @RequiresPermissions("normal:shopUser:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopUser shopUser)
    {
        List<ShopUser> list = shopUserService.selectShopUserList(shopUser);
        ExcelUtil<ShopUser> util = new ExcelUtil<ShopUser>(ShopUser.class);
        return util.exportExcel(list, "shopUser");
    }

    /**
     * 新增商户用户信息
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShop businessShop = businessShopService.selectBusinessShopById(id);
        if (businessShop == null){
            return "/404";
        }
        mmap.put("shop", businessShop);
        return prefix + "/add";
    }

    /**
     * 分配商户用户信息
     */
    @GetMapping("/authUser/{id}")
    public String authUser(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShop businessShop = businessShopService.selectBusinessShopById(id);
        if (businessShop == null){
            return "/404";
        }
        mmap.put("shop", businessShop);
        return prefix + "/shopUser";
    }


    /**
     * 新增保存商户用户信息
     */
    @RequiresPermissions("normal:shopUser:add")
    @Log(title = "商户用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopUser shopUser)
    {
        if(!shopUserService.checkLoginNameUnique(shopUser.getLoginName())){
            return error("新增用户'" + shopUser.getLoginName() + "'失败，登录账号已存在");
        }

        if(!shopUserService.checkPhoneUnique(shopUser.getPhonenumber())){
            return error("新增用户'" + shopUser.getPhonenumber() + "'失败,手机号码已存在");
        }

        shopUser.setSalt(ShiroUtils.randomSalt());
        shopUser.setPassword(passwordService.encryptPassword(shopUser.getLoginName(), shopUser.getPassword(), shopUser.getSalt()));
        shopUser.setCreateBy(ShiroUtils.getLoginName());

        return toAjax(shopUserService.insertShopUser(shopUser));
    }

    /**
     * 修改商户用户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopUser shopUser = shopUserService.selectShopUserById(id);
        BusinessShop businessShop = businessShopService.selectBusinessShopById(shopUser.getShopId());
        if (businessShop == null){
            return "/404";
        }
        mmap.put("shopUser", shopUser);
        mmap.put("shop", businessShop);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户用户信息
     */
    @RequiresPermissions("normal:shopUser:edit")
    @Log(title = "商户用户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopUser shopUser)
    {
        ShopUser actualShopUser = shopUserService.selectShopUserById(shopUser.getId());
        if (actualShopUser == null){
            return error("用户不存在,请联系管理员");
        }
        if (!actualShopUser.getPhonenumber().equals(shopUser.getPhonenumber())){
            if(!shopUserService.checkPhoneUnique(shopUser.getPhonenumber())){
                return error("修改用户'" + shopUser.getPhonenumber() + "'失败,手机号码已存在");
            }
        }

        shopUser.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(shopUserService.updateShopUser(shopUser));
    }

    @RequiresPermissions("normal:shopUser:resetPwd")
    @Log(title = "重置密码", businessType = BusinessType.UPDATE)
    @GetMapping("/resetPwd/{id}")
    public String resetPwd(@PathVariable("id") Long userId, ModelMap mmap)
    {
        mmap.put("shopUser", shopUserService.selectShopUserById(userId));
        return prefix + "/resetPwd";
    }

    @RequiresPermissions("normal:shopUser:resetPwd")
    @Log(title = "重置密码", businessType = BusinessType.UPDATE)
    @PostMapping("/resetPwd")
    @ResponseBody
    public AjaxResult resetPwdSave(ShopUser shopUser)
    {
        shopUser.setSalt(ShiroUtils.randomSalt());
        shopUser.setPassword(passwordService.encryptPassword(shopUser.getLoginName(), shopUser.getPassword(), shopUser.getSalt()));
        if (shopUserService.resetUserPwd(shopUser))
        {
            return success();
        }
        return error();
    }
    /**
     * 删除商户用户信息
     */
    @RequiresPermissions("normal:shopUser:remove")
    @Log(title = "商户用户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopUserService.deleteShopUserByIds(ids));
    }
}
