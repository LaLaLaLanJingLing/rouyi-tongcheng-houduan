package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopBalanceLog;
import com.ruoyi.system.service.IShopBalanceLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 平台余额记录Controller
 * 
 * @author ruoyi
 * @date 2019-12-17
 */
@Controller
@RequestMapping("/system/log")
public class ShopBalanceLogController extends BaseController
{
    private String prefix = "system/log";

    @Autowired
    private IShopBalanceLogService shopBalanceLogService;

    @RequiresPermissions("system:log:view")
    @GetMapping()
    public String log()
    {
        return prefix + "/log";
    }

    /**
     * 查询平台余额记录列表
     */
    @RequiresPermissions("system:log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopBalanceLog shopBalanceLog)
    {
        startPage();
        List<ShopBalanceLog> list = shopBalanceLogService.selectShopBalanceLogList(shopBalanceLog);
        return getDataTable(list);
    }

    /**
     * 导出平台余额记录列表
     */
    @RequiresPermissions("system:log:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopBalanceLog shopBalanceLog)
    {
        List<ShopBalanceLog> list = shopBalanceLogService.selectShopBalanceLogList(shopBalanceLog);
        ExcelUtil<ShopBalanceLog> util = new ExcelUtil<ShopBalanceLog>(ShopBalanceLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 新增平台余额记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存平台余额记录
     */
    @RequiresPermissions("system:log:add")
    @Log(title = "平台余额记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopBalanceLog shopBalanceLog)
    {
        return toAjax(shopBalanceLogService.insertShopBalanceLog(shopBalanceLog));
    }

    /**
     * 修改平台余额记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopBalanceLog shopBalanceLog = shopBalanceLogService.selectShopBalanceLogById(id);
        mmap.put("shopBalanceLog", shopBalanceLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存平台余额记录
     */
    @RequiresPermissions("system:log:edit")
    @Log(title = "平台余额记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopBalanceLog shopBalanceLog)
    {
        return toAjax(shopBalanceLogService.updateShopBalanceLog(shopBalanceLog));
    }

    /**
     * 删除平台余额记录
     */
    @RequiresPermissions("system:log:remove")
    @Log(title = "平台余额记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopBalanceLogService.deleteShopBalanceLogByIds(ids));
    }
}
