package com.ruoyi.web.controller.normal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.ProductImage;
import com.ruoyi.system.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import oshi.jna.platform.mac.SystemB;

/**
 * 优惠券列Controller
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
@Controller
@RequestMapping("/normal/goods")
public class GoodsController extends BaseController
{
    private String prefix = "normal/goods";

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private IBusinessShopService businessShopService;

    @Autowired
    private IProductCategoryService productCategoryService;

    @Autowired
    private IShopGroupService shopGroupService;

    @Autowired
    private IShopGroupGoodsService shopGroupGoodsService;

    @RequiresPermissions("system:goods:view")
    @GetMapping()
    public String goods()
    {
        return prefix + "/goods";
    }

    /**
     * 查询优惠券列列表
     */
    @RequiresPermissions("system:goods:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Goods goods)
    {
        startPage();
        List<Goods> list = goodsService.selectGoodsList(goods);
        return getDataTable(list);
    }

    /**
     * 导出优惠券列列表
     */
    @RequiresPermissions("system:goods:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Goods goods)
    {
        List<Goods> list = goodsService.selectGoodsList(goods);
        ExcelUtil<Goods> util = new ExcelUtil<Goods>(Goods.class);
        return util.exportExcel(list, "goods");
    }

    /**
     * 新增优惠券列
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        List<BusinessShop> businessShopList = businessShopService.selectBusinessShopList(new BusinessShop());
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(0L);
        productCategory.setName("顶级分类");

        List<ShopGroup> shopGroupList = shopGroupService.selectShopGroupList(new ShopGroup());

        mmap.put("groups", shopGroupList);
        mmap.put("shops", businessShopList);
        mmap.put("category",productCategory);
        return prefix + "/add";
    }

    /**
     * 新增保存优惠券列
     */
    @RequiresPermissions("system:goods:add")
    @Log(title = "优惠券列", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, String> goodsInfo)
    {

       goodsService.insertGoods(goodsInfo);
       return toAjax(true);
    }

    /**
     * 修改优惠券列
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Goods goods = goodsService.selectGoodsById(id);
        List<BusinessShop> businessShopList = businessShopService.selectBusinessShopList(new BusinessShop());

        if (StringUtils.isEmpty(goods.getIntroduction())){
            goods.setIntroduction("<h2>请111填写优惠券的介绍</h2>");
        }
        Long productCategoryId = goods.getProductCategoryId();
        ProductCategory productCategory = null;
        if (productCategoryId == 0L){
            productCategory = new ProductCategory();
            productCategory.setId(0L);
            productCategory.setName("顶级分类");
        }
        else{
            productCategory = productCategoryService.selectProductCategoryById(productCategoryId);
            if (productCategory == null){
                //报错
            }
        }

        //获取分组信息
        List<ShopGroup> shopGroupAllList = shopGroupService.selectShopGroupList(new ShopGroup());
        ShopGroupGoods shopGroupGoods = new ShopGroupGoods();
        shopGroupGoods.setGoodsId(id);
        List<ShopGroupGoods> shopGroupGoodsList = shopGroupGoodsService.selectShopGroupGoodsList(shopGroupGoods);
        List<Long> selectGroupIds = new ArrayList<Long>();
        for(ShopGroupGoods tempShopGroupGoods : shopGroupGoodsList){
            selectGroupIds.add(tempShopGroupGoods.getGroupId());
        }

        //获取关联商品信息
        Long relateId = goods.getRelateId();
        String relateName = "暂未关联商品";
        if (relateId != null){
            Goods relateGoods = goodsService.selectGoodsById(relateId);
            if (relateGoods != null){
                relateName = relateGoods.getName();
            }
        }
        mmap.put("relateName", relateName);
        mmap.put("selectGroupIds",selectGroupIds);
        mmap.put("groups",shopGroupAllList);
        mmap.put("shops", businessShopList);
        mmap.put("category",productCategory);
        mmap.put("goods", goods);

        return prefix + "/edit";
    }

    /**
     * 增加优惠券的展示图片
     */
    @GetMapping("/addProductImage")
    public String addProductImage()
    {
        return prefix + "/productImageFormAdd";
    }


    /**
     * 修改优惠券的展示图片
     */
    @GetMapping("/editProductImage")
    public String editProductImage( ProductImage productImage, ModelMap mmap)
    {
        mmap.put("productImage", productImage);
        return prefix + "/productImageFormEdit";
    }

    /**
     * 优惠券的图片展示列表
     */
    @PostMapping("/productImageList/{id}")
    @ResponseBody
    public TableDataInfo productImageList(@PathVariable("id") Long id)
    {
        Goods goods = goodsService.selectGoodsById(id);
        if (goods == null){
            //error
        }
        List<ProductImage> productImageList = goodsService.getProductImageList(goods);
        return getDataTable(productImageList);
    }

    /**
     * 修改保存优惠券列
     */
    @RequiresPermissions("system:goods:edit")
    @Log(title = "优惠券列", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Goods goods, @RequestParam Map<String, String> goodsInfo)
    {
        //TODO: 直接拿 会多一个逗号 不知道为什么
        goods.setProductImages(goodsInfo.get("productImages"));
        //生成分组关系
        String strGroupIds = goodsInfo.get("groupIds");
        //先删除以前的
        shopGroupGoodsService.deleteShopGroupGoodsByGoodsId(goods.getId());
        if(!StringUtils.isEmpty(strGroupIds)){
            String[] groupIds = strGroupIds.split(",");
            Long[] ids = StringUtils.convertToLong(groupIds);
            for (Long id : ids){
                ShopGroup shopGroup = shopGroupService.selectShopGroupById(id);
                if (shopGroup != null){
                    ShopGroupGoods shopGroupGoods = new ShopGroupGoods();
                    shopGroupGoods.setGoodsId(goods.getId());
                    shopGroupGoods.setGroupId(shopGroup.getId());
                    shopGroupGoodsService.insertShopGroupGoods(shopGroupGoods);
                }
            }
        }
        return toAjax(goodsService.updateGoods(goods));
    }

    /**
     * 删除优惠券列
     */
    @RequiresPermissions("system:goods:remove")
    @Log(title = "优惠券列", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(goodsService.deleteGoodsByIds(ids));
    }

    /**
     * 增加优惠券的展示图片
     */
    @GetMapping("/selectSingleGoods")
    public String selectSingleGoods()
    {
        return prefix + "/selectSingleGoods";
    }
    /**
     * 查询单选优惠券列表
     */
    @RequiresPermissions("system:goods:list")
    @PostMapping("/singleList")
    @ResponseBody
    public TableDataInfo singleList(Goods goods)
    {
        startPage();
        goods.setIsMarketable(true);
        List<Goods> list = goodsService.selectGoodsList(goods);
        return getDataTable(list);
    }

}
