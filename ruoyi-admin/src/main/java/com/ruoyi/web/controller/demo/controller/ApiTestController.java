package com.ruoyi.web.controller.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 报表
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/api123")
public class ApiTestController
{
    private String prefix = "demo/report";

    /**
     * 百度ECharts
     */
    @GetMapping("/index")
    public String index()
    {
        return prefix + "/echarts";
    }

}
