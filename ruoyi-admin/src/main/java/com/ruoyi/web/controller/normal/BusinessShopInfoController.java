package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShopInfo;
import com.ruoyi.system.service.IBusinessShopInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户信息Controller
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Controller
@RequestMapping("/normal/info")
public class BusinessShopInfoController extends BaseController
{
    private String prefix = "normal/info";

    @Autowired
    private IBusinessShopInfoService businessShopInfoService;

    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }

    /**
     * 查询商户信息列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShopInfo businessShopInfo)
    {
        startPage();
        List<BusinessShopInfo> list = businessShopInfoService.selectBusinessShopInfoList(businessShopInfo);
        return getDataTable(list);
    }

    /**
     * 导出商户信息列表
     */
    @RequiresPermissions("system:info:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShopInfo businessShopInfo)
    {
        List<BusinessShopInfo> list = businessShopInfoService.selectBusinessShopInfoList(businessShopInfo);
        ExcelUtil<BusinessShopInfo> util = new ExcelUtil<BusinessShopInfo>(BusinessShopInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 新增商户信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户信息
     */
    @RequiresPermissions("system:info:add")
    @Log(title = "商户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShopInfo businessShopInfo)
    {
        return toAjax(businessShopInfoService.insertBusinessShopInfo(businessShopInfo));
    }

    /**
     * 修改商户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShopInfo businessShopInfo = businessShopInfoService.selectBusinessShopInfoById(id);
        mmap.put("businessShopInfo", businessShopInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户信息
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "商户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShopInfo businessShopInfo)
    {
        return toAjax(businessShopInfoService.updateBusinessShopInfo(businessShopInfo));
    }

    /**
     * 删除商户信息
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "商户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopInfoService.deleteBusinessShopInfoByIds(ids));
    }
}
