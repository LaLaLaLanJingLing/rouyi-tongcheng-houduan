package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.system.domain.ShopArea;
import com.ruoyi.system.service.IShopAreaService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShop;
import com.ruoyi.system.service.IBusinessShopService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户Controller
 * 
 * @author ruoyi
 * @date 2019-10-25
 */
@Controller
@RequestMapping("/normal/shop")
public class BusinessShopController extends BaseController
{
    private String prefix = "normal/shop";

    @Autowired
    private IBusinessShopService businessShopService;

    @Autowired
    private IShopAreaService shopAreaService;

    @RequiresPermissions("system:shop:view")
    @GetMapping()
    public String shop()
    {
        return prefix + "/shop";
    }

    /**
     * 查询商户列表
     */
    @RequiresPermissions("system:shop:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShop businessShop)
    {
        startPage();
        List<BusinessShop> list = businessShopService.selectBusinessShopList(businessShop);
        return getDataTable(list);
    }




    /**
     * 导出商户列表
     */
    @RequiresPermissions("system:shop:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShop businessShop)
    {
        List<BusinessShop> list = businessShopService.selectBusinessShopList(businessShop);
        ExcelUtil<BusinessShop> util = new ExcelUtil<BusinessShop>(BusinessShop.class);
        return util.exportExcel(list, "shop");
    }

    /**
     * 新增商户
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        ShopArea shopArea = shopAreaService.selectShopAreaById(1L);
        mmap.put("areaId", shopArea.getId());
        mmap.put("areaName",shopArea.getName());
        return prefix + "/add";
    }

    /**
     * 新增保存商户
     */
    @RequiresPermissions("system:shop:add")
    @Log(title = "商户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShop businessShop)
    {
        return toAjax(businessShopService.insertBusinessShop(businessShop));
    }

    /**
     * 修改商户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShop businessShop = businessShopService.selectBusinessShopById(id);
        ShopArea shopArea = shopAreaService.selectShopAreaById(businessShop.getAreaId());
        Long areaId = 0L;
        String areaName = "";
        if (shopArea != null){
            areaId = shopArea.getId();
            areaName = shopArea.getName();
        }
        mmap.put("areaId", areaId);
        mmap.put("areaName", areaName);
        mmap.put("businessShop", businessShop);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户
     */
    @RequiresPermissions("system:shop:edit")
    @Log(title = "商户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShop businessShop)
    {
        return toAjax(businessShopService.updateBusinessShop(businessShop));
    }

    /**
     * 删除商户
     */
    @RequiresPermissions("system:shop:remove")
    @Log(title = "商户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopService.deleteBusinessShopByIds(ids));
    }
}
