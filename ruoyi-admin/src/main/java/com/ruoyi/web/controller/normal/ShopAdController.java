package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.system.domain.Goods;
import com.ruoyi.system.domain.ProductCategory;
import com.ruoyi.system.domain.ShopGroup;
import com.ruoyi.system.service.IGoodsService;
import com.ruoyi.system.service.IProductCategoryService;
import com.ruoyi.system.service.IShopGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopAd;
import com.ruoyi.system.service.IShopAdService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 轮播图Controller
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
@Controller
@RequestMapping("/normal/ad")
public class ShopAdController extends BaseController
{
    private String prefix = "normal/ad";

    @Autowired
    private IShopAdService shopAdService;

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private IShopGroupService shopGroupService;

    @Autowired
    private IProductCategoryService productCategoryService;

    @RequiresPermissions("system:ad:view")
    @GetMapping()
    public String ad()
    {
        return prefix + "/ad";
    }

    /**
     * 查询轮播图列表
     */
    @RequiresPermissions("system:ad:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopAd shopAd)
    {
        startPage();
        List<ShopAd> list = shopAdService.selectShopAdList(shopAd);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @RequiresPermissions("system:ad:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopAd shopAd)
    {
        List<ShopAd> list = shopAdService.selectShopAdList(shopAd);
        ExcelUtil<ShopAd> util = new ExcelUtil<ShopAd>(ShopAd.class);
        return util.exportExcel(list, "ad");
    }

    /**
     * 新增轮播图
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存轮播图
     */
    @RequiresPermissions("system:ad:add")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopAd shopAd)
    {
        return toAjax(shopAdService.insertShopAd(shopAd));
    }

    /**
     * 修改轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopAd shopAd = shopAdService.selectShopAdById(id);
        Long relateType = shopAd.getRelateType();
        Long relateId = shopAd.getRelateId();
        shopAd.setType(2L);
        String relateName = "";
        if (relateType == 0 ){
            relateName = "";
        }else if (relateType == 1){
            Goods goods = goodsService.selectGoodsById(relateId);
            if (goods != null){
                relateName = goods.getName();
            }
        }else if(relateType == 2){
            ProductCategory productCategory = productCategoryService.selectProductCategoryById(relateId);
            if (productCategory != null){
                relateName = productCategory.getName();
            }
        }else if(relateType == 3){
            ShopGroup shopGroup = shopGroupService.selectShopGroupById(relateId);
            if (shopGroup != null){
                relateName = shopGroup.getTitle();
            }
        }
        mmap.put("relateName", relateName);
        mmap.put("shopAd", shopAd);
        return prefix + "/edit";
    }

    /**
     * 修改保存轮播图
     */
    @RequiresPermissions("system:ad:edit")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopAd shopAd)
    {
        return toAjax(shopAdService.updateShopAd(shopAd));
    }

    /**
     * 删除轮播图
     */
    @RequiresPermissions("system:ad:remove")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopAdService.deleteShopAdByIds(ids));
    }
}
