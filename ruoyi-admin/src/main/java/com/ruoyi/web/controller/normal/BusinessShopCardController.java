package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShopCard;
import com.ruoyi.system.service.IBusinessShopCardService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户银行信息Controller
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Controller
@RequestMapping("/normal/card")
public class BusinessShopCardController extends BaseController
{
    private String prefix = "normal/card";

    @Autowired
    private IBusinessShopCardService businessShopCardService;

    @RequiresPermissions("system:card:view")
    @GetMapping()
    public String card()
    {
        return prefix + "/card";
    }

    /**
     * 查询商户银行信息列表
     */
    @RequiresPermissions("system:card:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShopCard businessShopCard)
    {
        startPage();
        List<BusinessShopCard> list = businessShopCardService.selectBusinessShopCardList(businessShopCard);
        return getDataTable(list);
    }

    /**
     * 导出商户银行信息列表
     */
    @RequiresPermissions("system:card:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShopCard businessShopCard)
    {
        List<BusinessShopCard> list = businessShopCardService.selectBusinessShopCardList(businessShopCard);
        ExcelUtil<BusinessShopCard> util = new ExcelUtil<BusinessShopCard>(BusinessShopCard.class);
        return util.exportExcel(list, "card");
    }

    /**
     * 新增商户银行信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户银行信息
     */
    @RequiresPermissions("system:card:add")
    @Log(title = "商户银行信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShopCard businessShopCard)
    {
        return toAjax(businessShopCardService.insertBusinessShopCard(businessShopCard));
    }

    /**
     * 修改商户银行信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShopCard businessShopCard = businessShopCardService.selectBusinessShopCardById(id);
        mmap.put("businessShopCard", businessShopCard);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户银行信息
     */
    @RequiresPermissions("system:card:edit")
    @Log(title = "商户银行信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShopCard businessShopCard)
    {
        return toAjax(businessShopCardService.updateBusinessShopCard(businessShopCard));
    }

    /**
     * 删除商户银行信息
     */
    @RequiresPermissions("system:card:remove")
    @Log(title = "商户银行信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopCardService.deleteBusinessShopCardByIds(ids));
    }
}
