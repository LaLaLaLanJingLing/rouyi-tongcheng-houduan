package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BusinessShopBalanceLog;
import com.ruoyi.system.service.IBusinessShopBalanceLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商户余额记录Controller
 * 
 * @author ruoyi
 * @date 2019-12-11
 */
@Controller
@RequestMapping("/normal/log")
public class BusinessShopBalanceLogController extends BaseController
{
    private String prefix = "normal/log";

    @Autowired
    private IBusinessShopBalanceLogService businessShopBalanceLogService;

    @RequiresPermissions("system:log:view")
    @GetMapping()
    public String log()
    {
        return prefix + "/log";
    }

    /**
     * 查询商户余额记录列表
     */
    @RequiresPermissions("system:log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BusinessShopBalanceLog businessShopBalanceLog)
    {
        startPage();
        List<BusinessShopBalanceLog> list = businessShopBalanceLogService.selectBusinessShopBalanceLogList(businessShopBalanceLog);
        return getDataTable(list);
    }

    /**
     * 导出商户余额记录列表
     */
    @RequiresPermissions("system:log:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BusinessShopBalanceLog businessShopBalanceLog)
    {
        List<BusinessShopBalanceLog> list = businessShopBalanceLogService.selectBusinessShopBalanceLogList(businessShopBalanceLog);
        ExcelUtil<BusinessShopBalanceLog> util = new ExcelUtil<BusinessShopBalanceLog>(BusinessShopBalanceLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 新增商户余额记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户余额记录
     */
    @RequiresPermissions("system:log:add")
    @Log(title = "商户余额记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BusinessShopBalanceLog businessShopBalanceLog)
    {
        return toAjax(businessShopBalanceLogService.insertBusinessShopBalanceLog(businessShopBalanceLog));
    }

    /**
     * 修改商户余额记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BusinessShopBalanceLog businessShopBalanceLog = businessShopBalanceLogService.selectBusinessShopBalanceLogById(id);
        mmap.put("businessShopBalanceLog", businessShopBalanceLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户余额记录
     */
    @RequiresPermissions("system:log:edit")
    @Log(title = "商户余额记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusinessShopBalanceLog businessShopBalanceLog)
    {
        return toAjax(businessShopBalanceLogService.updateBusinessShopBalanceLog(businessShopBalanceLog));
    }

    /**
     * 删除商户余额记录
     */
    @RequiresPermissions("system:log:remove")
    @Log(title = "商户余额记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(businessShopBalanceLogService.deleteBusinessShopBalanceLogByIds(ids));
    }
}
