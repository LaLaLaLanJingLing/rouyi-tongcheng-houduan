package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopArea;
import com.ruoyi.system.service.IShopAreaService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 商家地区Controller
 * 
 * @author ruoyi
 * @date 2019-11-06
 */
@Controller
@RequestMapping("/normal/area")
public class ShopAreaController extends BaseController
{
    private String prefix = "normal/area";

    @Autowired
    private IShopAreaService shopAreaService;

    @RequiresPermissions("normal:area:view")
    @GetMapping()
    public String area()
    {
        return prefix + "/area";
    }

    /**
     * 查询商家地区树列表
     */
    @RequiresPermissions("normal:area:list")
    @PostMapping("/list")
    @ResponseBody
    public List<ShopArea> list(ShopArea shopArea)
    {
        List<ShopArea> list = shopAreaService.selectShopAreaList(shopArea);
        return list;
    }

    /**
     * 导出商家地区列表
     */
    @RequiresPermissions("normal:area:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopArea shopArea)
    {
        List<ShopArea> list = shopAreaService.selectShopAreaList(shopArea);
        ExcelUtil<ShopArea> util = new ExcelUtil<ShopArea>(ShopArea.class);
        return util.exportExcel(list, "area");
    }

    /**
     * 新增商家地区
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("shopArea", shopAreaService.selectShopAreaById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存商家地区
     */
    @RequiresPermissions("normal:area:add")
    @Log(title = "商家地区", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopArea shopArea)
    {
        if (shopArea.getParentId() == null || shopArea.getParentId() == 0L){
            shopArea.setParentId(0L);
            shopArea.setGrade(1L);
        }
        else{
            ShopArea parentShopArea = shopAreaService.selectShopAreaById(shopArea.getParentId());
            if (parentShopArea != null){
                shopArea.setGrade(parentShopArea.getGrade() + 1);
            }else{
                //TODO 仍错
            }
        }
        shopArea.setFullName(shopArea.getName());
        return toAjax(shopAreaService.insertShopArea(shopArea));
    }

    /**
     * 修改商家地区
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopArea shopArea = shopAreaService.selectShopAreaById(id);
        String parentName = "顶级区域";
        if (shopArea.getParentId() != null && shopArea.getParentId() != 0L){
            parentName = shopAreaService.selectShopAreaById(shopArea.getParentId()).getName();
        }
        shopArea.setParentName(parentName);
        mmap.put("shopArea", shopArea);
        return prefix + "/edit";
    }

    /**
     * 修改保存商家地区
     */
    @RequiresPermissions("normal:area:edit")
    @Log(title = "商家地区", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopArea shopArea)
    {
        if (shopArea.getParentId() == null || shopArea.getParentId() == 0L){
            shopArea.setParentId(0L);
            shopArea.setGrade(1L);
        }
        else{
            ShopArea parentShopArea = shopAreaService.selectShopAreaById(shopArea.getParentId());
            if (parentShopArea != null){
                shopArea.setGrade(parentShopArea.getGrade() + 1);
            }else{
                //TODO 仍错
            }
        }
        shopArea.setFullName(shopArea.getName());
        return toAjax(shopAreaService.updateShopArea(shopArea));
    }

    /**
     * 删除
     */
    @RequiresPermissions("normal:area:remove")
    @Log(title = "商家地区", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(shopAreaService.deleteShopAreaById(id));
    }

    /**
     * 选择商家地区树
     */
    @GetMapping(value = { "/selectAreaTree/{id}", "/selectAreaTree/" })
    public String selectAreaTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("shopArea", shopAreaService.selectShopAreaById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载商家地区树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = shopAreaService.selectShopAreaTree();
        return ztrees;
    }
}
