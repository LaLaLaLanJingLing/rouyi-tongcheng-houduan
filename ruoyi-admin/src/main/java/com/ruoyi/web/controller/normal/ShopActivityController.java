package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopActivity;
import com.ruoyi.system.service.IShopActivityService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动Controller
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
@Controller
@RequestMapping("/normal/activity")
public class ShopActivityController extends BaseController
{
    private String prefix = "normal/activity";

    @Autowired
    private IShopActivityService shopActivityService;

    @RequiresPermissions("system:activity:view")
    @GetMapping()
    public String activity()
    {
        return prefix + "/activity";
    }

    /**
     * 查询活动列表
     */
    @RequiresPermissions("system:activity:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopActivity shopActivity)
    {
        startPage();
        List<ShopActivity> list = shopActivityService.selectShopActivityList(shopActivity);
        return getDataTable(list);
    }

    /**
     * 导出活动列表
     */
    @RequiresPermissions("system:activity:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopActivity shopActivity)
    {
        List<ShopActivity> list = shopActivityService.selectShopActivityList(shopActivity);
        ExcelUtil<ShopActivity> util = new ExcelUtil<ShopActivity>(ShopActivity.class);
        return util.exportExcel(list, "activity");
    }

    /**
     * 新增活动
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动
     */
    @RequiresPermissions("system:activity:add")
    @Log(title = "活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopActivity shopActivity)
    {
        shopActivity.setIsDelete(false);
        return toAjax(shopActivityService.insertShopActivity(shopActivity));
    }

    /**
     * 修改活动
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopActivity shopActivity = shopActivityService.selectShopActivityById(id);
        mmap.put("activity", shopActivity);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动
     */
    @RequiresPermissions("system:activity:edit")
    @Log(title = "活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopActivity shopActivity)
    {
        shopActivity.setIsDelete(false);
        return toAjax(shopActivityService.updateShopActivity(shopActivity));
    }

    /**
     * 删除活动
     */
    @RequiresPermissions("system:activity:remove")
    @Log(title = "活动", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopActivityService.deleteShopActivityByIds(ids));
    }
}
