package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.system.domain.Member;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopMemberComment;
import com.ruoyi.system.service.IShopMemberCommentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 评论Controller
 * 
 * @author ruoyi
 * @date 2019-11-26
 */
@Controller
@RequestMapping("/normal/comment")
public class ShopMemberCommentController extends BaseController
{
    private String prefix = "normal/comment";

    @Autowired
    private IShopMemberCommentService shopMemberCommentService;

    @RequiresPermissions("system:comment:view")
    @GetMapping()
    public String comment()
    {
        return prefix + "/comment";
    }

    /**
     * 查询评论列表
     */
    @RequiresPermissions("system:comment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopMemberComment shopMemberComment)
    {
        startPage();
        List<ShopMemberComment> list = shopMemberCommentService.selectShopMemberCommentList(shopMemberComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @RequiresPermissions("system:comment:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopMemberComment shopMemberComment)
    {
        List<ShopMemberComment> list = shopMemberCommentService.selectShopMemberCommentList(shopMemberComment);
        ExcelUtil<ShopMemberComment> util = new ExcelUtil<ShopMemberComment>(ShopMemberComment.class);
        return util.exportExcel(list, "comment");
    }

    /**
     * 新增评论
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存评论
     */
    @RequiresPermissions("system:comment:add")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopMemberComment shopMemberComment)
    {
        return toAjax(shopMemberCommentService.insertShopMemberComment(shopMemberComment));
    }

    /**
     * 修改评论
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopMemberComment shopMemberComment = shopMemberCommentService.selectShopMemberCommentById(id);
        mmap.put("shopMemberComment", shopMemberComment);
        Member member = shopMemberComment.getMember();
        mmap.put("nickName", member.getActualNickName());
        return prefix + "/edit";
    }

    /**
     * 修改保存评论
     */
    @RequiresPermissions("system:comment:edit")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopMemberComment shopMemberComment)
    {
        ShopMemberComment actualShopMemberComment = shopMemberCommentService.selectShopMemberCommentById(shopMemberComment.getId());
        if (actualShopMemberComment == null){
            return AjaxResult.error("参数有误,请联系管理员");
        }
        String strContent = shopMemberComment.getContent();
        String status = shopMemberComment.getStatus();

        actualShopMemberComment.setContent(strContent);
        actualShopMemberComment.setStatus(status);

        return toAjax(shopMemberCommentService.updateShopMemberComment(actualShopMemberComment));
    }

    /**
     * 删除评论
     */
    @RequiresPermissions("system:comment:remove")
    @Log(title = "评论", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopMemberCommentService.deleteShopMemberCommentByIds(ids));
    }
}
