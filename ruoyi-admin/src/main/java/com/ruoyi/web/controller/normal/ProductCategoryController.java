package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.SysDept;
import com.ruoyi.system.domain.SysMenu;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductCategory;
import com.ruoyi.system.service.IProductCategoryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品分类Controller
 * 
 * @author ruoyi
 * @date 2019-10-24
 */
@Controller
@RequestMapping("/normal/category")
public class ProductCategoryController extends BaseController
{
    private String prefix = "normal/category";

    @Autowired
    private IProductCategoryService productCategoryService;

    @RequiresPermissions("system:category:view")
    @GetMapping()
    public String category()
    {
        return prefix + "/category";
    }

    /**
     * 查询商品分类列表
     */
    @RequiresPermissions("system:category:list")
    @PostMapping("/list")
    @ResponseBody
    public List<ProductCategory> list(ProductCategory productCategory)
    {
        startPage();
        List<ProductCategory> list = productCategoryService.selectProductCategoryList(productCategory);
        return list;
    }

    /**
     * 导出商品分类列表
     */
    @RequiresPermissions("system:category:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductCategory productCategory)
    {
        List<ProductCategory> list = productCategoryService.selectProductCategoryList(productCategory);
        ExcelUtil<ProductCategory> util = new ExcelUtil<ProductCategory>(ProductCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 新增商品分类
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap)
    {
        ProductCategory productCategory = null;
        if (parentId != 0L){
            productCategory = productCategoryService.selectProductCategoryById(parentId);
        }
        else {
            productCategory = new ProductCategory();
            productCategory.setId(0L);
            productCategory.setName("顶级分类");
        }
        mmap.put("dept", productCategory);
        return prefix + "/add";
    }

    @GetMapping("/selectProductCategoryTree/{productCategoryId}")
    public String selectProductCategoryTree(@PathVariable("productCategoryId") Long productCategoryId, ModelMap mmap)
    {
        ProductCategory productCategory = null;
        if (productCategoryId != 0L){
            productCategory = productCategoryService.selectProductCategoryById(productCategoryId);
        }
        else {
            productCategory = new ProductCategory();
            productCategory.setId(0L);
            productCategory.setName("顶级分类");
        }
        mmap.put("dept", productCategory);
        return prefix + "/tree";
    }

    /**
     * 加载分类列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = productCategoryService.selectProductCategoryTree(new ProductCategory());
        return ztrees;
    }

    /**
     * 新增保存商品分类
     */
    @RequiresPermissions("system:category:add")
    @Log(title = "商品分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductCategory productCategory)
    {

        return toAjax(productCategoryService.insertProductCategory(productCategory));
    }

    /**
     * 修改商品分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductCategory productCategory = productCategoryService.selectProductCategoryById(id);
        Long parentId = productCategory.getParentId();
        ProductCategory parentProductCategory = null;
        if (parentId == 0L){
            parentProductCategory = new ProductCategory();
            parentProductCategory.setId(0L);
            parentProductCategory.setName("顶级分类");
        }else{
            parentProductCategory = productCategoryService.selectProductCategoryById(parentId);
        }

        mmap.put("productCategory", productCategory);
        mmap.put("parent", parentProductCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品分类
     */
    @RequiresPermissions("system:category:edit")
    @Log(title = "商品分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductCategory productCategory)
    {
        return toAjax(productCategoryService.updateProductCategory(productCategory));
    }

    /**
     * 删除商品分类
     */
    @RequiresPermissions("system:category:remove")
    @Log(title = "商品分类", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productCategoryService.deleteProductCategoryByIds(ids));
    }
}
