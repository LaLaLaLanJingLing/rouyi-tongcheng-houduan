package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysArea;
import com.ruoyi.system.service.ISysAreaService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 区域Controller
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Controller
@RequestMapping("/system/area")
public class SysAreaController extends BaseController
{
    private String prefix = "system/area";

    @Autowired
    private ISysAreaService sysAreaService;

    @RequiresPermissions("system:area:view")
    @GetMapping()
    public String area()
    {
        return prefix + "/area";
    }

    /**
     * 查询区域树列表
     */
    @RequiresPermissions("system:area:list")
    @PostMapping("/list")
    @ResponseBody
    public List<SysArea> list(SysArea sysArea)
    {
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        return list;
    }

    /**
     * 导出区域列表
     */
    @RequiresPermissions("system:area:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysArea sysArea)
    {
        List<SysArea> list = sysAreaService.selectSysAreaList(sysArea);
        ExcelUtil<SysArea> util = new ExcelUtil<SysArea>(SysArea.class);
        return util.exportExcel(list, "area");
    }

    /**
     * 新增区域
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("sysArea", sysAreaService.selectSysAreaById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存区域
     */
    @RequiresPermissions("system:area:add")
    @Log(title = "区域", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysArea sysArea)
    {
        return toAjax(sysAreaService.insertSysArea(sysArea));
    }

    /**
     * 修改区域
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysArea sysArea = sysAreaService.selectSysAreaById(id);
        mmap.put("sysArea", sysArea);
        return prefix + "/edit";
    }

    /**
     * 修改保存区域
     */
    @RequiresPermissions("system:area:edit")
    @Log(title = "区域", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysArea sysArea)
    {
        return toAjax(sysAreaService.updateSysArea(sysArea));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:area:remove")
    @Log(title = "区域", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(sysAreaService.deleteSysAreaById(id));
    }

    /**
     * 选择区域树
     */
    @GetMapping(value = { "/selectAreaTree/{id}", "/selectAreaTree/" })
    public String selectAreaTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("sysArea", sysAreaService.selectSysAreaById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载区域树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = sysAreaService.selectSysAreaTree();
        return ztrees;
    }
}
