package com.ruoyi.web.controller.normal;

import java.util.List;

import com.ruoyi.system.domain.Goods;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopGroup;
import com.ruoyi.system.service.IShopGroupService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分组Controller
 * 
 * @author ruoyi
 * @date 2019-10-31
 */
@Controller
@RequestMapping("/normal/group")
public class ShopGroupController extends BaseController
{
    private String prefix = "normal/group";

    @Autowired
    private IShopGroupService shopGroupService;

    @RequiresPermissions("system:group:view")
    @GetMapping()
    public String group()
    {
        return prefix + "/group";
    }

    /**
     * 查询分组列表
     */
    @RequiresPermissions("system:group:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopGroup shopGroup)
    {
        startPage();
        List<ShopGroup> list = shopGroupService.selectShopGroupList(shopGroup);
        return getDataTable(list);
    }

    /**
     * 导出分组列表
     */
    @RequiresPermissions("system:group:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopGroup shopGroup)
    {
        List<ShopGroup> list = shopGroupService.selectShopGroupList(shopGroup);
        ExcelUtil<ShopGroup> util = new ExcelUtil<ShopGroup>(ShopGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 新增分组
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存分组
     */
    @RequiresPermissions("system:group:add")
    @Log(title = "分组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopGroup shopGroup)
    {
        return toAjax(shopGroupService.insertShopGroup(shopGroup));
    }

    /**
     * 修改分组
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopGroup shopGroup = shopGroupService.selectShopGroupById(id);
        mmap.put("shopGroup", shopGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存分组
     */
    @RequiresPermissions("system:group:edit")
    @Log(title = "分组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopGroup shopGroup)
    {
        return toAjax(shopGroupService.updateShopGroup(shopGroup));
    }

    /**
     * 删除分组
     */
    @RequiresPermissions("system:group:remove")
    @Log(title = "分组", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopGroupService.deleteShopGroupByIds(ids));
    }

    /**
     * 单选分组
     */
    @GetMapping("/selectSingleGroup")
    public String selectSingleGroup()
    {
        return prefix + "/selectSingleGroup";
    }
    /**
     * 查询单选优惠券列表
     */
    @RequiresPermissions("system:group:list")
    @PostMapping("/singleList")
    @ResponseBody
    public TableDataInfo singleList(ShopGroup shopGroup)
    {
        startPage();
        shopGroup.setIsUsed(true);
        List<ShopGroup> list = shopGroupService.selectShopGroupList(shopGroup);
        return getDataTable(list);
    }

}
