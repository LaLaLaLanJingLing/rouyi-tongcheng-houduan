package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopNotice;
import com.ruoyi.system.service.IShopNoticeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商城通告Controller
 * 
 * @author ruoyi
 * @date 2019-11-12
 */
@Controller
@RequestMapping("/normal/notice")
public class ShopNoticeController extends BaseController
{
    private String prefix = "normal/notice";

    @Autowired
    private IShopNoticeService shopNoticeService;

    @RequiresPermissions("system:notice:view")
    @GetMapping()
    public String notice()
    {
        return prefix + "/notice";
    }

    /**
     * 查询商城通告列表
     */
    @RequiresPermissions("system:notice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopNotice shopNotice)
    {
        startPage();
        List<ShopNotice> list = shopNoticeService.selectShopNoticeList(shopNotice);
        return getDataTable(list);
    }

    /**
     * 导出商城通告列表
     */
    @RequiresPermissions("system:notice:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopNotice shopNotice)
    {
        List<ShopNotice> list = shopNoticeService.selectShopNoticeList(shopNotice);
        ExcelUtil<ShopNotice> util = new ExcelUtil<ShopNotice>(ShopNotice.class);
        return util.exportExcel(list, "notice");
    }

    /**
     * 新增商城通告
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商城通告
     */
    @RequiresPermissions("system:notice:add")
    @Log(title = "商城通告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopNotice shopNotice)
    {
        return toAjax(shopNoticeService.insertShopNotice(shopNotice));
    }

    /**
     * 修改商城通告
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopNotice shopNotice = shopNoticeService.selectShopNoticeById(id);
        mmap.put("shopNotice", shopNotice);
        return prefix + "/edit";
    }

    /**
     * 修改保存商城通告
     */
    @RequiresPermissions("system:notice:edit")
    @Log(title = "商城通告", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopNotice shopNotice)
    {
        return toAjax(shopNoticeService.updateShopNotice(shopNotice));
    }

    /**
     * 删除商城通告
     */
    @RequiresPermissions("system:notice:remove")
    @Log(title = "商城通告", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopNoticeService.deleteShopNoticeByIds(ids));
    }
}
