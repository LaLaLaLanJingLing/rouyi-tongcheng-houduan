package com.ruoyi.web.controller.normal;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Receiver;
import com.ruoyi.system.service.IReceiverService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收货信息Controller
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Controller
@RequestMapping("/normal/receiver")
public class ReceiverController extends BaseController
{
    private String prefix = "normal/receiver";

    @Autowired
    private IReceiverService receiverService;

    @RequiresPermissions("normal:receiver:view")
    @GetMapping()
    public String receiver()
    {
        return prefix + "/receiver";
    }

    /**
     * 查询收货信息列表
     */
    @RequiresPermissions("normal:receiver:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Receiver receiver)
    {
        startPage();
        List<Receiver> list = receiverService.selectReceiverList(receiver);
        return getDataTable(list);
    }

    /**
     * 导出收货信息列表
     */
    @RequiresPermissions("normal:receiver:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Receiver receiver)
    {
        List<Receiver> list = receiverService.selectReceiverList(receiver);
        ExcelUtil<Receiver> util = new ExcelUtil<Receiver>(Receiver.class);
        return util.exportExcel(list, "receiver");
    }

    /**
     * 新增收货信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存收货信息
     */
    @RequiresPermissions("normal:receiver:add")
    @Log(title = "收货信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Receiver receiver)
    {
        return toAjax(receiverService.insertReceiver(receiver));
    }

    /**
     * 修改收货信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Receiver receiver = receiverService.selectReceiverById(id);
        mmap.put("receiver", receiver);
        return prefix + "/edit";
    }

    /**
     * 修改保存收货信息
     */
    @RequiresPermissions("normal:receiver:edit")
    @Log(title = "收货信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Receiver receiver)
    {
        return toAjax(receiverService.updateReceiver(receiver));
    }

    /**
     * 删除收货信息
     */
    @RequiresPermissions("normal:receiver:remove")
    @Log(title = "收货信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(receiverService.deleteReceiverByIds(ids));
    }
}
