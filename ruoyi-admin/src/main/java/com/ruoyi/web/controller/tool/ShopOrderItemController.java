package com.ruoyi.web.controller.tool;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ShopOrderItem;
import com.ruoyi.system.service.IShopOrderItemService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单单例Controller
 * 
 * @author ruoyi
 * @date 2019-10-29
 */
@Controller
@RequestMapping("/system/item")
public class ShopOrderItemController extends BaseController
{
    private String prefix = "system/item";

    @Autowired
    private IShopOrderItemService shopOrderItemService;

    @RequiresPermissions("system:item:view")
    @GetMapping()
    public String item()
    {
        return prefix + "/item";
    }

    /**
     * 查询订单单例列表
     */
    @RequiresPermissions("system:item:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopOrderItem shopOrderItem)
    {
        startPage();
        List<ShopOrderItem> list = shopOrderItemService.selectShopOrderItemList(shopOrderItem);
        return getDataTable(list);
    }

    /**
     * 导出订单单例列表
     */
    @RequiresPermissions("system:item:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopOrderItem shopOrderItem)
    {
        List<ShopOrderItem> list = shopOrderItemService.selectShopOrderItemList(shopOrderItem);
        ExcelUtil<ShopOrderItem> util = new ExcelUtil<ShopOrderItem>(ShopOrderItem.class);
        return util.exportExcel(list, "item");
    }

    /**
     * 新增订单单例
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单单例
     */
    @RequiresPermissions("system:item:add")
    @Log(title = "订单单例", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopOrderItem shopOrderItem)
    {
        return toAjax(shopOrderItemService.insertShopOrderItem(shopOrderItem));
    }

    /**
     * 修改订单单例
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ShopOrderItem shopOrderItem = shopOrderItemService.selectShopOrderItemById(id);
        mmap.put("shopOrderItem", shopOrderItem);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单单例
     */
    @RequiresPermissions("system:item:edit")
    @Log(title = "订单单例", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopOrderItem shopOrderItem)
    {
        return toAjax(shopOrderItemService.updateShopOrderItem(shopOrderItem));
    }

    /**
     * 删除订单单例
     */
    @RequiresPermissions("system:item:remove")
    @Log(title = "订单单例", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopOrderItemService.deleteShopOrderItemByIds(ids));
    }
}
